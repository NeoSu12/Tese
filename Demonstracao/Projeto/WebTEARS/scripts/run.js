// ----- IMPORTS

const path = require('path')
const prompt = require('prompt')

const { Darknet } = require('../app/third/darknet/darknet')
const Server = require('../app/dist/server.js').default

// ----- USER PROMPT

// Prepare promp schema
const schema = {
  properties: {
    database: {
      description: 'Enter database name to use:',
      required: true
    },
    username: {
      description: 'Username:',
      required: true
    },
    password: {
      description: 'Password:',
      required: true,
      hidden: true
    }
  }
}

// Start the prompt
prompt.start()

// Get three properties from the user: database, username and password
prompt.get(schema, (error, result) => {
  if(error) {
    throw 'Error fetching user input parameters: [database, username, password]'
  }
  else {
    const database = result.database
    const username = result.username
    const password = result.password

    // Clear result just for SAFETY!
    result = ''

    // Variables
    const SERVER_CONFIG = {
      root: __dirname,
      port: {
        http: 8080,
        https: 8181,
      }
    }

    const DB_CONFIG = {
      database,
      username,
      password
    }

    // Create server
    const SERVER = new Server(Darknet, SERVER_CONFIG, DB_CONFIG)

    // Initialize server
    SERVER.initialize()
  }
})
