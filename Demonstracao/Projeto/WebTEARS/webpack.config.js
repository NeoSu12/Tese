// ----- IMPORTS

const path = require("path")
const webpack = require("webpack")

// ----- PLUGINS

module.exports = [
  {
    // ----- WEBPACK ENTRIES

		target: "node",
		entry: [
      __dirname + "/app/src/server.js"
    ],
		output: {
			path: __dirname + "/app/dist",
			filename: "server.js",
			library: "Server",
			libraryTarget: "umd",
			umdNamedDefine: true
		},

    // ----- BABEL + ESLINT

		module: {
			rules: [
        
        // ----- BABEL

        {
					test: /(\.js|\.jsx)$/,
					include: path.resolve(__dirname, "app/src/js"),
          exclude: [path.resolve(__dirname, "node_modules"), path.resolve(__dirname, "app/third")],
					use: [
            {
              loader: 'babel-loader'
            }
          ]
				},

        // ----- ESLINT

        {
          enforce: "pre",
					test: /(\.js)$/,
					include: path.resolve(__dirname, "app/src/js"),
          exclude: [path.resolve(__dirname, "node_modules"), path.resolve(__dirname, "app/third")],
          use: [
            {
              loader: 'eslint-loader',
              options: {
                fix: true,
                quiet: true
              }
            }
          ]
				}

			]
		},
    stats: {
      warnings: false
    }
	}
]
