// ----- IMPORTS

import fs from 'fs'
import path from 'path'
import http from 'http'
import https from 'https'

// Rest HTTP
import express from 'express'
import rateLimit from 'express-rate-limit'
import morgan from 'morgan'
import bodyParser from 'body-parser'
import compression from 'compression'
import cors from 'cors'

import basicAuth from 'express-basic-auth'

// Socket IO
import SocketIO from 'socket.io'

// Configurations
import Config from './config'

// Custom classes
import Database from './js/db/database'

// Custom routers
import Users from './js/routers/users'
import Models from './js/routers/models'
import Photos from './js/routers/photos'
import Annotations from './js/routers/annotations'
import Learning from './js/routers/learning'

// Custom utils
import { pick, log } from './js/utils'

// IMPORTANT - Server must be run from directory we consider to be the ROOT !!

// ----- DEFINE SERVER

export default class Server {
  // ----- CONSTRUCTOR

  constructor(darknet, serverConfig, databaseConfig) {
    // Darknet reference
    this._darknet = darknet

    // Server configuration
    this._inputServerConfig = serverConfig
    this._finalServerConfig = Object.assign(Config, this._inputServerConfig)

    // Database configuration
    this._inputDatabaseConfig = databaseConfig

    // Important variables
    this._root = this._finalServerConfig.root
    this._storage = this._root + '/storage'
    this._ssl = this._root + '/ssl'

    this._port = this._finalServerConfig.port
    this._endpoints = this._finalServerConfig.endpoints

    // SSL Certificate
    this._privateKey = fs.readFileSync(this._ssl + '/rootCA.key', 'utf8')
    this._certificate = fs.readFileSync(this._ssl + '/rootCA.pem', 'utf8')

    this._options = {
      key: this._privateKey,
      cert: this._certificate
    }
  }

  // ----- BASIC

  async initialize() {
    log('[SERVICE] Starting TEARS server', 'INFO')

    try {
      // Initialize database
      await this.database()
      log('[DATABASE] Database initialized successfully, preparing services ...', 'SUCCESS')

      // Initialize service
      try {
        // Variables
        this._app = express()
        this._app.use(express.static(this._root + '/assets'))

        // Configuration
        this.configuration()

        // Storage
        this.storage()

        // Routers
        this.routers()

        // Listen
        this.listenREST()
        this.listenSOCKET()

        log('[SERVICE] Services initialized successfully, now listening ...', 'SUCCESS')
      }
      catch (err) {
        log(`[SERVICE] Unable to start services: ${err}`, 'ERROR')
      }
    }
    catch (err) {
      log(`[DATABASE] Unable to start database: ${err}`, 'ERROR')
    }
  }

  // ----- METHODS

  configuration() {
    // RATE LIMIT - Prevent DOS Atacks
    const limiter = new rateLimit(
      {
        windowMs: 15 * 60 * 1000, // 15 minutes
        max: 1000, // Limit each IP to 100 requests per windowMs
        delayMs: 0 // Disable delaying - Full speed until the max limit is reached
      }
    )
    this._app.use(limiter)

    // MORGAN - Console logging
    this._app.use(morgan('dev'))

    // BODY-PARSER - Json Serialization/Deserialization
    this._app.use(bodyParser.json({limit: '30mb'}))

    // COMPRESSION - Compress response bodies with deflate or gzip
    this._app.use(compression())

    // CORS
    this._app.use(cors())

    // BASIC AUTH
    this._auth = basicAuth({
      authorizer: this.getAuthorizer.bind(this),
      authorizeAsync: true,
      unauthorizedResponse: this.getUnauthorizedResponse.bind(this)
    })
  }

  routers() {
    // Reference auth and database in routers
    this._rUsers = new Users(this._endpoints.users, this._auth, this._database, this._storage)
    this._rModels = new Models(this._endpoints.models, this._auth, this._database, this._storage)
    this._rPhotos = new Photos(this._endpoints.photos, this._auth, this._database, this._storage)
    this._rAnnotations = new Annotations(this._endpoints.annotations, this._auth, this._database, this._storage)
    this._rLearning = new Learning(this._endpoints.learning, this._auth, this._database, this._storage, this._darknet)

    // API routers
    this._app.use(this._endpoints.users, this._rUsers.router)
    this._app.use(this._endpoints.models, this._rModels.router)
    this._app.use(this._endpoints.photos, this._rPhotos.router)
    this._app.use(this._endpoints.annotations, this._rAnnotations.router)
    this._app.use(this._endpoints.learning, this._rLearning.router)
  }

  async database() {
    this._database = new Database(this._inputDatabaseConfig, this._storage)

    // Initialize database
    await this._database.initialize()
  }

  storage() {
    log('[STORAGE] Preparing storage unit', 'INFO')

    const exists = fs.existsSync(this._storage)

    if(!exists) {
      log('[STORAGE] WARNING - Creating new storage unit', 'WARNING')

      // Create directory if doesn't exist
      fs.mkdirSync(this._storage)
    }
    else {
      log('[STORAGE] WARNING - Storage unit already exists', 'WARNING')
    }
  }

  listenREST() {
    this._httpServer = http.createServer(this._app).listen(this._port.http, () => {
      log(`[SERVICE] HTTP Listening on *: ${this._port.http}`)
    })

    this._httpsServer = https.createServer(this._options, this._app).listen(this._port.https, () => {
      log(`[SERVICE] HTTPS Listening on *: ${this._port.https}`)
    })

    // Set server timeout to 15 minutes
    this._httpServer.timeout = 15 * 60 * 1000
    this._httpsServer.timeout = 15 * 60 * 1000
  }

  listenSOCKET() {
    // Initialize socket over http server
    this._io = new SocketIO(this._httpServer, {
      serveClient: false,
      transports: ['websocket']
    })

    // Handle socket communications
    this._users = {}

    // Connection
    this._io.on('connection', socket => {
      const id = socket.id
      var currentUser = {
          id,
          socket,
          auth: null,
          valid: false
      }

      // Check if user already connected to the server
      if (this._users.hasOwnProperty(currentUser.id)) {
        log(`[${currentUser.id}] User already connected, kicking`)
        socket.disconnect()
      } else {
        log(`[${currentUser.id}] New user connected`)

        // Add user locally
        this._users[currentUser.id] = currentUser;

        log(`[SOCKET] Total users: ${Object.keys(this._users).length}`)
      }

      // Ping test
      socket.on('ping', () => {
        socket.emit('pong')
      })

      // Disconnection
      socket.on('disconnect', () => {
        log(`[${currentUser.id}] User disconnected`)

        // Remove user locally
        if (this._users.hasOwnProperty(currentUser.id))
          delete this._users[currentUser.id]

        log(`[SOCKET] Total users: ${Object.keys(this._users).length}`)
      })

      // Auth
      socket.on('auth', async (auth) => {
        log(`[${currentUser.id}] User is trying to authenticate`)

        // Authenticate user
        try {
          const authObject = JSON.parse(auth)
          const user = await this.tryAuthorizer(authObject.username, authObject.password)

          log(`[${currentUser.id}] User is authenticated as: ${user.email}`)

          // Update auth value
          currentUser.auth = user

          // Create learning machine
          currentUser.valid = this._rLearning.createMachine(user.email)

          if(currentUser.valid)
            log(`[${currentUser.id}] User predicting machine was created`)
          else
            log(`[${currentUser.id}] User needs to train a new model to access a predicting machine`)

          // Warn user about authentication
          currentUser.socket.emit('auth', true)

        } catch(error) {
          log(`[${currentUser.id}] User did a bad authentication: ${error}`)

          // Warn user about authentication
          currentUser.socket.emit('auth', false)
        }
      })

      // Frame
      socket.on('frame', frame => {
        const frameObject = JSON.parse(frame)
        const id = frameObject.id
        const source = frameObject.source

        // Check if user is authed and has a created predicting machine
        if(currentUser.valid) {
          this._rLearning.predictMachine(currentUser.auth.email, source)
          .then(predictions => {
            // predictions = {id: ..., predictions: [{label: ..., probability: ..., x: ..., y: ..., width: ..., height: ... } ...]}

            // Warn user about predictions
            if(predictions && predictions.length > 0) {
              const result = {
                id,
                predictions
              }

              console.log(result);
              currentUser.socket.emit('predictions', JSON.stringify(result))
            }
          })
        }
      })
    })
  }

  // ----- EVENT HANDLERS

  async tryAuthorizer(email, password) {
    if(email && password) {
      // Fetch user data
      const user = await this._database.user.findById(email)
      const object = user ? user.toJSON() : null
      const filter = object ? pick(object, ['email', 'createdAt', 'updatedAt']) : null

      if(user) {
        const userPassword = user.password

        if(password === userPassword) {
          // Valid request
          return filter
        }
        else {
          throw "Passwords don't match"
        }
      }
      else {
        throw 'Username not found'
      }
    }
  }

  async getAuthorizer(email, password, callback) {
    let result = false

    if(email && password) {
      // Fetch user data
      const user = await this._database.user.findById(email)

      if(user) {
        const userPassword = user.password
        result = password === userPassword
      }
    }

    return callback(null, result)
  }

  async getUnauthorizedResponse(req) {
    const invalidCredentials = {
      code: 401,
      message: 'Unauthorized access'
    }

    const noCredentials = {
      code: 403,
      message: 'No credentials provided'
    }

    return req.auth ? invalidCredentials : noCredentials
  }
}
