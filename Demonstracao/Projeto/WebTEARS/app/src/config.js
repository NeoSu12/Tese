const config = {
  root: 'C:/Users/asilva/Documents/Tese/Demonstracao/Projeto/WebTEARS/scripts',
  port: {
    http: 8080,
    https: 8181,
  },
  endpoints: {
    users: '/api/users',
    models: '/api/models',
    photos: '/api/photos',
    annotations: '/api/annotations',
    learning: '/api/learning'
  }
}

export default config
