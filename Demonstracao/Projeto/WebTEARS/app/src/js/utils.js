const sequelize = require('sequelize')
const operation = sequelize.Op

const fs = require('fs')
const path = require('path')
const chalk = require('chalk')

const readChunk = require('read-chunk')
const imageType = require('image-type')
const jimp = require('jimp').default
const fstorm = require('fstorm')

// ----- SYSTEM

async function sleep(time) {
	const realTime = time || 1000

	return new Promise((resolve, reject) => {
		setTimeout(resolve, realTime)
	})
}

// ---- OBJECT MANIPULATION

function pick(obj, keys) {
  return keys.reduce((result, key) => {
      if(obj.hasOwnProperty(key))
        result[key] = obj[key]
      return result
  }, {})
}

function like(obj, keys) {
  return keys.reduce((result, key) => {
      if(obj.hasOwnProperty(key))
        result[key] = {[operation.iLike]:`%${obj[key]}%`}
      return result
  }, {})
}

function arrayDifference(a, b) {
  return a.filter(value => b.indexOf(value) < 0)
}

function arrayShuffle(a) {
  let result = JSON.parse(JSON.stringify(a))
	let size = result.length

  for (var i = 0; i < size; i++) {
      const randomIndex = Math.floor(Math.random()*(i+1))
      const itemAtIndex = result[randomIndex];

      result[randomIndex] = result[i]
      result[i] = itemAtIndex
  }

  return result
}

function arraySplit(a, proportion) {
	const size = a.length

	const calculatedSplit = size > 1 ? Math.ceil(size * proportion) : 0

	const firstStart = 0
	const firstEnd = calculatedSplit

	const secondStart = size > 1 ? calculatedSplit : 0
	const secondEnd = size

	const first = a.slice(firstStart, firstEnd)
	const second = a.slice(secondStart, secondEnd)

	return [first, second]
}

function getRandomRange(min, max) {
  return Math.floor(Math.random() * (max - min) + min)
}

// ---- IMAGING

function imageToBase64(path) {
  try {
    // Check if image is really an image or FU** that
    let isImage = validateImage(path)

    if(isImage) {
      // Read binary data
      const image = fs.readFileSync(path)

      // Convert binary data to base64 encoded string
      return Buffer.from(image).toString('base64')
    }
    else {
      return null
    }
  } catch(error) {
    return null
  }
}

function base64ToImage(path, base64) {
	return new Promise(async (resolve, reject) => {
		// Write image to file using FSTORM
		const writer = fstorm(path)

		writer.write(base64, 'base64', (err, status) => {
			if(!err) {
				// Check if image is really an image or FU** that
			  const valid = validateImage(path)

			  if(!valid) {
			    // Delete image if not valid
			    fs.unlinkSync(path)
			    reject('Invalid image file')
			  }
				else {
					resolve()
				}
			}
			else {
				reject('Impossible to write file')
			}
		})
	})
}

function validateImage(path) {
  // Check if image is really an image or FU** that
  const chunk = readChunk.sync(path, 0, 12)
  const result = imageType(chunk)

  const mimeType = result ? result.mime : null
	const isImage = mimeType && mimeType.indexOf('image') !== -1

  return isImage
}

async function thumbnailImage(source, target, width) {
  const realWidth = width || 500

  if(source && target) {
		// Read image from path
		const image = await jimp.read(source)

		// Resize image
		return image.resize(realWidth, realWidth).write(target)
	}
}

async function croppedImage(source, target, rectangle) {
  const realRectangle = rectangle || {
    'topLeft': {
      x: 0.0,
      y: 0.0
    },
    'bottomRight': {
      x: 1.0,
      y: 1.0
    }
  }

  if(source && target) {
    const topLeft = realRectangle.topLeft
    const bottomRight = realRectangle.bottomRight

		// Read image from path
		const image = await jimp.read(source)
    const width = image.bitmap.width
		const height = image.bitmap.height

		const x = Math.round(topLeft.x * width)
		const y = Math.round(topLeft.y * height)
		const w = Math.round((bottomRight.x - topLeft.x) * width)
		const h = Math.round((bottomRight.y - topLeft.y) * height)

		// Crop image
		return image.crop(x, y, w, h).write(target)
  }
}

async function drawPredictions(path, predictions) {
	// Fetch labels
	const labels = predictions.map(prediction => prediction.name)
	const uniqueLabels = labels.filter((prediction, index, array) => array.indexOf(prediction) === index)

	// Prepare different colors
	const colors = {}
	uniqueLabels.forEach(label => {
		colors[label] = randomColor()
	})

	console.log(colors)

	// Read image from path
	const image = await jimp.read(path)
	const width = image.bitmap.width
	const height = image.bitmap.height

	// Fill pixel function
	const fillFunction = (label) => {
		const color = colors[label]

		return (x, y, idx) => {
			// Red
			this.bitmap.data[idx + 0] = color.r

			// Green
	  	this.bitmap.data[idx + 1] = color.g

			// Blue
	  	this.bitmap.data[idx + 2] = color.b

			// Alpha
	  	this.bitmap.data[idx + 3] = 255
		}
	}

	// Draw all rectangles
	const border = 10

	predictions.forEach(prediction => {
		const label = prediction.name
		const color = colors[label]
		const box = prediction.box
		const w = Math.round(box.w)
		const h = Math.round(box.h)
		const x = Math.round(box.x - w * 0.5)
		const y = Math.round(box.y - h * 0.5)

		const myFill = fillCrimson(color)

		// Top
		image.scan(x, y, w, border, myFill)

		// Bottom
		image.scan(x, y + h - border, w, border, myFill)

		// Left
		image.scan(x, y, border, h, myFill)

		// Right
		image.scan(x + w - border, y, border, h, myFill)
	})

	// Save image
	await image.writeAsync(path)

	// Return label to color map
	return colors
}

function fillCrimson(color) {
	const intColor = jimp.rgbaToInt(color.r, color.g, color.b, 255)

	return function (x, y, idx) {
    this.bitmap.data.writeUInt32BE(intColor, idx, true)
  }
}

function randomColor() {
	return {
		r: Math.floor(Math.random() * 256),
		g: Math.floor(Math.random() * 256),
		b: Math.floor(Math.random() * 256)
	}
}

// ----- FYLE SYSTEM

function pathExists(path) {
	return fs.existsSync(path)
}

function isDirectory(dir) {
  return fs.lstatSync(dir).isDirectory()
}

function listDirectory(dir, userFilter) {
  let emails = fs.readdirSync(dir)
  let paths = emails.map(email => path.join(dir, email))

  paths.filter((path, index) => {
    let filterValue = userFilter ? userFilter(path) : true

    if(!filterValue) {
        // Remove from emails list too
        emails.splice(index, 1)
    }

    return filterValue
  })

  return {
    emails, paths
  }
}

function createDirectory(dir) {
	if(!pathExists(dir))
  	fs.mkdirSync(dir)
}

function emptyDirectory(dir) {
  // Clear directory files
  let files = fs.readdirSync(dir)

  files.forEach(file => {
    let filePath = path.join(dir, file)
    let checkDirectory = isDirectory(filePath)

    if (checkDirectory)
      deleteDirectory(filePath)
    else
      deleteFile(filePath)
  })
}

function deleteDirectory(dir) {
	if(pathExists(dir)) {
	  // Empty directory first
	  emptyDirectory(dir)

	  // Delete directory
	  fs.rmdirSync(dir)
	}
}

function createFile(file, text) {
	fs.writeFileSync(file, text)
}

function updateFile(file, text) {
	fs.appendFileSync(file, text)
}

function deleteFile(file) {
	if(pathExists(file)) {
	  let checkDirectory = isDirectory(file)

	  if(!checkDirectory)
	    fs.unlinkSync(file)
	}
}

// ----- DATE

function formatDate(date) {
  if(date && date instanceof Date) {
    let year = date.getFullYear()
    let month = ("0" + (date.getMonth() + 1)).slice(-2)
    let day = ("0" + date.getDate()).slice(-2)

    return `${year}-${month}-${day}`
  }
}

// ----- RESPONSE STATUS

function validRequest(res, body, code) {
  const responseCode = code || 200
  const response = {
    code: code || 200,
    body
  }

  res.status(responseCode).send(response)
}

function invalidRequest(res, exception) {
  const error = {
    code: 400,
    message: 'We could not handle your request at the moment, please try again later',
    extra: exception.errors || exception
  }

  // Log message
  console.log(`[EXCEPTION] - ${exception}`)

  // Error status
  res.status(400).send(error)
}

function invalidCredentials(res) {
  const error = {
    code: 403,
    message: 'No credentials provided'
  }

  res.status(403).send(error)
}

function notAuthorized(res) {
  const error = {
    code: 401,
    message: 'Not authorized to access this area'
  }

  res.status(401).send(error)
}

function notFound(res) {
  const error = {
    code: 404,
    message: 'Resource could not be found'
  }

  res.status(404).send(error)
}

// ----- LOGGING

function log(message, level) {
	const colors = {
		'SUCCESS': '#AED581',
		'ERROR': '#FF8A65',
		'WARNING': '#FFB74D',
		'INFO': '#4FC3F7',
		'SCRIPT': '#FFFFFF'
	}

	const upperLevel = level ? level.toUpperCase() : 'INFO'
	const chosenColor = colors[upperLevel] || colors['INFO']

	console.log(chalk.hex(chosenColor)(message))
}

export { sleep,
         pick,
				 like,
         arrayDifference,
				 arrayShuffle,
				 arraySplit,
				 getRandomRange,

         imageToBase64,
         base64ToImage,
         validateImage,
         thumbnailImage,
         croppedImage,
				 drawPredictions,

				 pathExists,
         isDirectory,
         listDirectory,
         createDirectory,
         emptyDirectory,
				 createFile,
				 updateFile,
         deleteFile,
         deleteDirectory,

         formatDate,

         validRequest,
         invalidRequest,
         invalidCredentials,
         notAuthorized,
         notFound,

			   log
			 }
