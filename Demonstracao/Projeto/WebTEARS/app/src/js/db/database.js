// ----- IMPORTS

import Sequelize from 'sequelize'

// Configurations
import Config from './config'

// Custom models
import User from './models/user'
import Model from './models/model'
import Photo from './models/photo'
import Annotation from './models/annotation'

// Custom utils
import { log } from '../utils'

// ----- DEFINE DATABASE

export default class Database {
  // ----- CONSTRUCTOR

  constructor(config, storage) {
    // Sequelize configuration
    this._config = Object.assign(Config, config)
    this._storage = storage

    // Sequelize connection
    this._sequelize = new Sequelize(
      this._config.database,
      this._config.username,
      this._config.password,
      this._config.options
    )
  }

  // ----- BASIC

  async initialize() {
    await this._sequelize.authenticate()

    log('[DATABASE] Connection has been established successfully', 'SUCCESS')

    // Build models
    this._build()

    // Build associations
    this._associate()

    // Sync all tables
    this._sync()

    log('[DATABASE] All models were correctly synced', 'SUCCESS')
  }

  // ----- METHODS

  _build() {
    this._models = {
      User: User.init(this._sequelize),
      Model: Model.init(this._sequelize),
      Photo: Photo.init(this._sequelize),
      Annotation: Annotation.init(this._sequelize)
    }
  }

  _associate() {
    let currentModels = Object.values(this._models)
    currentModels.filter(model => typeof model.associate === "function").forEach(model => model.associate(this._models))
  }

  _sync() {
    // Sync all models that aren't already in the database
    this._sequelize.sync()
  }

  // ----- GETTERS

  get user() {
    return this._models.User
  }

  get model() {
    return this._models.Model
  }

  get photo() {
    return this._models.Photo
  }

  get annotation() {
    return this._models.Annotation
  }
}
