// ----- IMPORTS

import Sequelize from 'sequelize'

// ----- DEFINE MODEL

export default class Model extends Sequelize.Model {
  // ----- INITIALIZE

  static init(sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.UUID,
          allowNull: false,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        // Information
        label: {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: 'UNKNOWN'
        },
        description: {
          type: Sequelize.STRING,
          allowNull: false,
          defaultValue: 'NO DESCRIPTION'
        }
      },
      { sequelize }
    )
  }

  // ----- ASSOCIATIONS

  static associate(models) {
    // MODEL (1 <- 1) USER
    this.belongsTo(models.User)

    // MODEL (1 -> N) PHOTO
    this.hasMany(models.Photo, {
      onDelete: 'cascade',
      hooks: true,
    })
  }
}
