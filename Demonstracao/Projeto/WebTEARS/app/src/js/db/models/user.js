// ----- IMPORTS

import Sequelize from 'sequelize'

// ----- DEFINE USER

export default class User extends Sequelize.Model {
  // ----- INITIALIZE

  static init(sequelize) {
    return super.init(
      {
        email: {
          type: Sequelize.STRING,
          allowNull: false,
          primaryKey: true
        },
        password: {
          type: Sequelize.STRING,
          allowNull: false
        }
      },
      { sequelize }
    )
  }

  // ----- ASSOCIATIONS

  static associate(models) {
    // USER (1 -> N) MODEL
    this.hasMany(models.Model, {
      onDelete: 'cascade',
      hooks: true,
    })
  }
}
