// ----- IMPORTS

import fs from 'fs'

import Sequelize from 'sequelize'
import { imageToBase64 } from '../../utils'

// ----- DEFINE ANNOTATION

export default class Annotation extends Sequelize.Model {
  // ----- INITIALIZE

  static init(sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.UUID,
          allowNull: false,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        // Cropped image
        croppedPath: {
          type: Sequelize.STRING,
          allowNull: true
        },
        cropped: {
          type: Sequelize.STRING,
          allowNull: true,
          get() {
            const path = this.getDataValue('croppedPath')
            return imageToBase64(path)
          }
        },

        // Rectangle
        topLeftX: {
          type: Sequelize.FLOAT,
          allowNull: true
        },
        topLeftY: {
          type: Sequelize.FLOAT,
          allowNull: true
        },
        bottomRightX: {
          type: Sequelize.FLOAT,
          allowNull: true
        },
        bottomRightY: {
          type: Sequelize.FLOAT,
          allowNull: true
        }
      },
      { sequelize }
    )
  }

  // ----- ASSOCIATIONS

  static associate(models) {
    // ANNOTATION (1 <- 1) PHOTO
    this.belongsTo(models.Photo)
  }
}
