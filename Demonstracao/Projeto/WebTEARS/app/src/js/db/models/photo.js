// ----- IMPORTS

import fs from 'fs'

import Sequelize from 'sequelize'
import { imageToBase64 } from '../../utils'

// ----- DEFINE CLIENT

export default class Photo extends Sequelize.Model {
  // ----- INITIALIZE

  static init(sequelize) {
    return super.init(
      {
        id: {
          type: Sequelize.UUID,
          allowNull: false,
          defaultValue: Sequelize.UUIDV4,
          primaryKey: true
        },

        // Original
        originalPath: {
          type: Sequelize.STRING,
          allowNull: true
        },
        original: {
          type: Sequelize.STRING,
          allowNull: true,
          get() {
            const path = this.getDataValue('originalPath')
            return imageToBase64(path)
          }
        },

        // Thumbnail
        thumbnailPath: {
          type: Sequelize.STRING,
          allowNull: true
        },
        thumbnail: {
          type: Sequelize.STRING,
          allowNull: true,
          get() {
            const path = this.getDataValue('thumbnailPath')
            return imageToBase64(path)
          }
        },
      },
      { sequelize }
    )
  }

  // ----- ASSOCIATIONS

  static associate(models) {
    // PHOTO (1 <- 1) MODEL
    this.belongsTo(models.Model)

    // PHOTO (1 -> N) ANNOTATION
    this.hasMany(models.Annotation, {
      onDelete: 'cascade',
      hooks: true,
    })
  }
}
