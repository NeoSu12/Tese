const config = {
  database: 'something',
  username: 'default',
  password: '1234',
  options: {
    host: 'localhost',
    dialect: 'postgres',
    operatorsAliases: false,
    logging: false
  }
}

export default config
