// ----- IMPORTS

import fs from 'fs'
import path from 'path'

import uniqid from 'uniqid'
import basic from 'basic-auth'

import { Router } from 'express'

import { pick, validRequest, invalidRequest, invalidCredentials, notAuthorized, notFound, createDirectory, deleteDirectory, arrayDifference, listDirectory, log } from '../utils'

// ----- DEFINE USERS

export default class Users {
  // ----- CONSTRUCTOR

  constructor(endpoint, auth, database, storage, python) {
    log('[ROUTER] Launching USERS router', 'INFO')

    // Variables
    this._endpoint = endpoint
    this._auth = auth
    this._database = database
    this._storage = storage
    this._python = python
    this._router = new Router()

    // Set router endpoints
    this.endpoints()

    // Sync existant users (REMOVE EXCESS)
    this.sync()
  }

  endpoints() {
    // Authentication
    this._router.get('/auth', this.auth.bind(this)) // UNPROTECTED

    this._router.post('/getUsers', this.getUsers.bind(this)) // UNPROTECTED
    this._router.get('/getUser/:email', this._auth, this.getUser.bind(this)) // PROTECTED FOR USER
  }

  async sync() {
    const storageFolder = this._storage
    const usersFolder = `${this._storage}/users/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create users directory if does not exist
    createDirectory(usersFolder)

    // List of current storage users
    const storageList = listDirectory(usersFolder)
    const storageUsers = storageList.emails
    log(`[SYNC] STORAGE Users: ${JSON.stringify(storageUsers)}`, 'WARNING')

    // List of current database users
    const query = {
      attributes: ['email'],
      raw: true
    }
    const result = await this._database.user.findAndCountAll(query)
    const databaseUsers = result.rows.map(user => user.email)
    log(`[SYNC] DATABASE Users: ${JSON.stringify(databaseUsers)}`, 'WARNING')

    // List of users to remove from database
    // const deleteDatabaseUsers = arrayDifference(databaseUsers, storageUsers)
    // log(`[SYNC] DATABASE Users to DELETE: ${JSON.stringify(deleteDatabaseUsers)}`, 'WARNING')

    // Delete excess database users
    // await this._database.user.destroy({ where: { email: deleteDatabaseUsers }})

    // List of users to remove from storage
    const deleteStorageUsers = arrayDifference(storageUsers, databaseUsers)
    log(`[SYNC] STORAGE Users to DELETE: ${JSON.stringify(deleteStorageUsers)}`, 'WARNING')

    // Delete excess storage users
    deleteStorageUsers.forEach(userEmail => {
      const userFolder = `${this._storage}/users/${userEmail}`
      deleteDirectory(userFolder)
    })
  }

  // ----- GETTERS -----

  get router() {
    return this._router
  }

  // ----- BASIC -----

  async auth(req, res) {
    const auth = basic(req)

    if(!auth) {
      // Invalid credentials
      invalidCredentials(res)
    }
    else {
      const email = auth.name
      const password = auth.pass

      try {
        // Fetch user data
        const user = await this._database.user.findById(email)

        // Create new user data if DOES NOT EXISTS
        if(!user) {
          const create = await this._database.user.create({
            email,
            password
          })

          const object = create ? create.toJSON() : null
          const filter = object ? pick(object, ['email', 'createdAt', 'updatedAt']) : null

          // Valid request
          validRequest(res, filter, 201)
        }
        else {
          // Redirect user if ALREADY EXISTS
          res.redirect(`${this._endpoint}/getUser/${email}`)
        }
      } catch (error) {
        // Invalid request
        invalidRequest(res, error)
      }
    }
  }

  async getUsers(req, res) {
    const body = req.body
    const where = body && body.where ? pick(body.where, ['email']) : {}
    const order = body && body.order ? body.order : [['email', 'ASC']]
    const offset = body && body.offset ? body.offset : 0
    const limit = body && body.limit ? Math.min(body.limit, 100) : 10

    try {
      // Format user query
      const realQuery = {
        attributes: ['email', 'createdAt', 'updatedAt'],
        where,
        order,
        offset,
        limit,
        raw: true
      }

      // Fetch users data
      const result = await this._database.user.findAndCountAll(realQuery)

      // Filter annotations rows
      // ...

      // Valid request
      validRequest(res, result)
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async getUser(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsEmail = params.email

    if(paramsEmail !== authEmail) {
      notAuthorized(res)
    }
    else {
      try {
        // Fetch user data
        const user = await this._database.user.findById(authEmail)
        const object = user ? user.toJSON() : null
        const filter = object ? pick(object, ['email', 'createdAt', 'updatedAt']) : null

        // Valid request
        validRequest(res, filter)
      } catch (error) {
        // Invalid request
        invalidRequest(res, error)
      }
    }
  }

  // ----- METHODS -----
  // ...

  // ----- PROCESSES -----
  // ...
}
