// ----- IMPORTS

import fs from 'fs'
import path from 'path'
import uniqid from 'uniqid'

import { Router } from 'express'

import { sleep, pick, arrayShuffle, arraySplit, validRequest, invalidRequest, notFound, log, pathExists, createDirectory, createFile, updateFile, deleteFile, base64ToImage, imageToBase64, drawPredictions } from '../utils'

// ----- DEFINE LEARNING

export default class Learning {
  // ----- CONSTRUCTOR

  constructor(endpoint, auth, database, storage, darknet) {
    log('[ROUTER] Launching LEARNING router', 'INFO')

    // Variables
    this._endpoint = endpoint
    this._auth = auth
    this._database = database
    this._storage = storage
    this._darknet = darknet
    this._router = new Router()

    this._trainingStatus = false
    this._predictStatus = false
    this._machines = {}

    // Set router endpoints
    this.endpoints()
  }

  endpoints() {
    // Packages
    this._router.get('/train', this._auth, this.status.bind(this)) // PROTECTED FOR USER
    this._router.post('/train', this._auth, this.train.bind(this)) // PROTECTED FOR USER

    this._router.post('/predict', this._auth, this.predict.bind(this)) // PROTECTED FOR USER
  }

  // ----- GETTERS -----

  get router() {
    return this._router
  }

  // ----- BASIC -----

  async status(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const query = req.query

    try {
      // Train status
      const result = this._trainingStatus

      // Valid request
      validRequest(res, result)
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async train(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const body = req.body

    try {
      const startTraining = !this._trainingStatus

      // Start training process if model is not being trained
      if(startTraining) {
        // Train model
        await this.trainModel(authEmail)
      }

      const result = this._trainingStatus

      // Valid request
      validRequest(res, result, startTraining ? 200 : 418)
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async predict(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const body = req.body

    try {
      // User photo
      const photo = body.source

      // Create and validate temporary photo
      const tempId = await this.createTemporaryPhoto(authEmail, photo)

      // Predict object label
      const predictions = await this.predictModel(authEmail, tempId)
      console.log(predictions)

      // Draw rectangles on photo
      const tempPath = `${this._storage}/users/${authEmail}/temps/${tempId}.jpg`
      const colorMap = await drawPredictions(tempPath, predictions)

      // Fetch result image
      const labeled = imageToBase64(tempPath);

      // Delete temporary photo
      this.deleteTemporaryPhoto(authEmail, tempId)

      // Valid request
      validRequest(res, {
        labeled,
        predictions,
        colorMap
      })
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  // ----- PROCESSES -----

  async trainModel(userEmail) {
    // Model started training
    this._trainingStatus = true

    // Fetch models, photos and annotations
    const result = await this.fetchTrainingData(userEmail)
    const count = result.count
    const rows = result.rows

    if(count > 0) {
      // Names
      const names = this.filterNames(userEmail, rows)

      // Photo paths
      const photos = this.filterPhotoPaths(userEmail, rows)

      const shuffledPhotos = arrayShuffle(photos)
      const splitPhotos = arraySplit(shuffledPhotos, 0.8)

      const trainingPhotos = splitPhotos[0]
      const testingPhotos = splitPhotos[1]

      // Generate photo annotations
      this.createTearsAnnotations(userEmail, rows)

      // Generate tears data file
      this.createTearsData(userEmail, count)

      // Generate tears names file
      this.createTearsNames(userEmail, names)

      // Generate training set file
      this.createTearsTraining(userEmail, trainingPhotos)

      // Generate validation set file
      this.createTearsTesting(userEmail, testingPhotos)

      // Generate tears network configuration file
      this.createTearsNetwork(userEmail, count)

      // Call shell command to train and WAIT
      // TODO ...
    }
    else {
      throw 'No classes available to train'
    }

    // Model stopped training
    this._trainingStatus = false
  }

  async predictModel(userEmail, tempId) {
    const tempPath = `${this._storage}/users/${userEmail}/temps/${tempId}.jpg`

    const networkPath = `${this._storage}/users/${userEmail}/learning/tears.cfg`
    const weightsPath = `${this._storage}/users/${userEmail}/learning/tears.weights`
    const namesPath = `${this._storage}/users/${userEmail}/learning/tears.names`

    // Check if files exist
    const networkExists = pathExists(networkPath)
    const weightsExists = pathExists(weightsPath)
    const namesExists = pathExists(namesPath)
    const filesExist = networkExists && weightsExists && namesExists

    if(filesExist) {
      // Init
      var darknet = new this._darknet({
        config: networkPath,
        weights: weightsPath,
        namefile: namesPath
      })

      // Detect
      return darknet.detectAsync(tempPath)
    }
    else {
      throw 'User predicting machine is not trained'
    }
  }

  // ----- LIVE

  createMachine(userEmail) {
    const networkPath = `${this._storage}/users/${userEmail}/learning/tears.cfg`
    const weightsPath = `${this._storage}/users/${userEmail}/learning/tears.weights`
    const namesPath = `${this._storage}/users/${userEmail}/learning/tears.names`

    // Check if files exist
    const networkExists = pathExists(networkPath)
    const weightsExists = pathExists(weightsPath)
    const namesExists = pathExists(namesPath)
    const filesExist = networkExists && weightsExists && namesExists

    if(filesExist) {
      // Initialize machine
      var darknet = new this._darknet({
        config: networkPath,
        weights: weightsPath,
        namefile: namesPath
      })

      // Save reference locally
      this._machines[userEmail] = darknet
    }

    return filesExist
  }

  deleteMachine(userEmail) {
    if(this._machines.hasOwnProperty(userEmail))
      delete this._machines[userEmail]
  }

  async predictMachine(userEmail, base64) {
    if(!this._predictStatus) {
      // Change prediction status
      this._predictStatus = true

      // Create and validate temporary photo
      const tempId = await this.createTemporaryPhoto(userEmail, base64)

      // Temporary image path
      const tempPath = `${this._storage}/users/${userEmail}/temps/${tempId}.jpg`

      // Make detections
      let result = []
      let formattedResult = []
      const darknet = this._machines[userEmail]

      if(darknet) {
        result = await darknet.detectAsync(tempPath)

        if(result && result.length > 0) {
          // Draw predictions
          // await drawPredictions(tempPath, result);

          // Format prediction
          formattedResult = result.map(prediction => {
            return {
              label: prediction.name.toUpperCase(),
              probability: prediction.prob,
              x: prediction.box.x,
              y: prediction.box.y,
              width: prediction.box.w,
              height: prediction.box.h
            }
          })
        }
      }

      // Delete temporary photo
      this.deleteTemporaryPhoto(userEmail, tempId)

      // Change prediction status
      this._predictStatus = false

      // Log formattedResult
      return formattedResult
    }
  }

  // ----- DATA

  filterNames(userEmail, data) {
    return data.map(model => model.label)
  }

  filterPhotoPaths(userEmail, data) {
    const result = []

    // Iterate over all models
    data.forEach(model => {
      const modelId = model.id
      const modelPhotos = model.photos

      // Iterate over all model photos
      modelPhotos.forEach(photo => {
        const photoId = photo.id
        const photoPath = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/original.jpg`

        result.push(photoPath)
      })
    })

    return result
  }

  async fetchTrainingData(userEmail) {
    const query = {
      attributes: ['id', 'label'],
      where: {UserEmail: userEmail},
      order: [['label', 'ASC']],
      include: [{
        model: this._database.photo,
        include: [{
          model: this._database.annotation
        }]
      }],
      distinct: true
    }

    const result = await this._database.model.findAndCountAll(query)
    const count = result.count
    const rows = result.rows

    // Filter models data
    const filter = rows.map(model => {
      const modelObject = model.toJSON()
      const modelPhotos = modelObject.Photos

      // Filter photos
      const filteredPhotos = modelPhotos.map(photo => {
        const photoAnnotations = photo.Annotations

        // Filter annotations
        const filteredAnnotations = photoAnnotations.map(annotation => pick(annotation, ['id', 'topLeftX', 'topLeftY', 'bottomRightX', 'bottomRightY']))

        // Filtered photo
        let photoFiltered = pick(photo, ['id'])
        photoFiltered.annotations = filteredAnnotations

        return photoFiltered
      })

      // Filtered model
      let modelFiltered = pick(modelObject, ['id', 'label'])
      modelFiltered.photos = filteredPhotos

      return modelFiltered
    })

    return {count, rows:filter}
  }

  // ----- METHODS -----

  async createTemporaryPhoto(userEmail, base64) {
    const tempId = uniqid()
    const tempPath = `${this._storage}/users/${userEmail}/temps/${tempId}.jpg`

    // Check if storage is well built
    const storageFolder = this._storage
    const usersFolder = `${this._storage}/users/`
    const ownerFolder = `${this._storage}/users/${userEmail}/`
    const tempsFolder = `${this._storage}/users/${userEmail}/temps/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create users directory if does not exist
    createDirectory(usersFolder)

    // Create owner directory if does not exist
    createDirectory(ownerFolder)

    // Create temps directory if does not exist
    createDirectory(tempsFolder)

    // Create and validate file from base64
    await base64ToImage(tempPath, base64)

    // Return temporary id
    return tempId
  }

  deleteTemporaryPhoto(userEmail, tempId) {
    const tempPath = `${this._storage}/users/${userEmail}/temps/${tempId}.jpg`

    // Delete file
    deleteFile(tempPath)
  }

  createTearsAnnotations(userEmail, data) {
    // Check if storage is well built
    const storageFolder = this._storage
    const usersFolder = `${this._storage}/users/`
    const ownerFolder = `${this._storage}/users/${userEmail}/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create users directory if does not exist
    createDirectory(usersFolder)

    // Create owner directory if does not exist
    createDirectory(ownerFolder)

    // Iterate over all models
    let classIndex = 0

    data.forEach(model => {
      const modelId = model.id
      const modelFolder = `${this._storage}/users/${userEmail}/models/${modelId}/`

      // Create model directory if does not exist
      createDirectory(modelFolder)

      const modelPhotos = model.photos

      // Iterate over all model photos
      modelPhotos.forEach(photo => {
        const photoId = photo.id
        const photoFolder = `${this._storage}/users/${userEmail}/models//${modelId}/${photoId}/`

        // Create photo directory if does not exist
        createDirectory(photoFolder)

        const photoAnnotations = photo.annotations
        const annotationsPath = `${this._storage}/users/${userEmail}/models//${modelId}/${photoId}/original.txt`

        // Create photo annotations file
        createFile(annotationsPath, '')

        // Append annotations to file
        photoAnnotations.forEach(annotation => {
          const x = (annotation.topLeftX + annotation.bottomRightX) * 0.5
          const y = (annotation.topLeftY + annotation.bottomRightY) * 0.5
          const width = annotation.bottomRightX - annotation.topLeftX
          const height = annotation.bottomRightY - annotation.topLeftY

          updateFile(annotationsPath, `${classIndex} ${x} ${y} ${width} ${height}\n`)
        })
      })

      classIndex++
    })
  }

  createTearsData(userEmail, classes) {
    const dataPath = `${this._storage}/users/${userEmail}/learning/tears.data`

    const namesPath = `${this._storage}/users/${userEmail}/learning/tears.names`
    const trainPath = `${this._storage}/users/${userEmail}/learning/train.txt`
    const validPath = `${this._storage}/users/${userEmail}/learning/test.txt`
    const backupPath = `${this._storage}/users/${userEmail}/learning/backup`

    // Check if storage is well built
    const storageFolder = this._storage
    const usersFolder = `${this._storage}/users/`
    const ownerFolder = `${this._storage}/users/${userEmail}/`
    const learningFolder = `${this._storage}/users/${userEmail}/learning/`
    const backupFolder = `${this._storage}/users/${userEmail}/learning/backup/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create users directory if does not exist
    createDirectory(usersFolder)

    // Create owner directory if does not exist
    createDirectory(ownerFolder)

    // Create learning directory if does not exist
    createDirectory(learningFolder)

    // Create backup directory if does not exist
    createDirectory(backupFolder)

    // Create tears data file
    const data = `classes=${classes}\nnames=${namesPath}\ntrain=${trainPath}\nvalid=${validPath}\nbackup=${backupPath}`
    createFile(dataPath, data)
  }

  createTearsNames(userEmail, names) {
    const namesPath = `${this._storage}/users/${userEmail}/learning/tears.names`

    // Check if storage is well built
    const storageFolder = this._storage
    const usersFolder = `${this._storage}/users/`
    const ownerFolder = `${this._storage}/users/${userEmail}/`
    const learningFolder = `${this._storage}/users/${userEmail}/learning/`
    const backupFolder = `${this._storage}/users/${userEmail}/learning/backup/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create users directory if does not exist
    createDirectory(usersFolder)

    // Create owner directory if does not exist
    createDirectory(ownerFolder)

    // Create learning directory if does not exist
    createDirectory(learningFolder)

    // Create backup directory if does not exist
    createDirectory(backupFolder)

    // Create tears names file
    createFile(namesPath, '')

    // Append names to file
    names.forEach(name => {
      updateFile(namesPath, `${name}\n`)
    })
  }

  createTearsTraining(userEmail, photos) {
    const trainingPath = `${this._storage}/users/${userEmail}/learning/train.txt`

    // Check if storage is well built
    const storageFolder = this._storage
    const usersFolder = `${this._storage}/users/`
    const ownerFolder = `${this._storage}/users/${userEmail}/`
    const learningFolder = `${this._storage}/users/${userEmail}/learning/`
    const backupFolder = `${this._storage}/users/${userEmail}/learning/backup/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create users directory if does not exist
    createDirectory(usersFolder)

    // Create owner directory if does not exist
    createDirectory(ownerFolder)

    // Create learning directory if does not exist
    createDirectory(learningFolder)

    // Create backup directory if does not exist
    createDirectory(backupFolder)

    // Create tears training file
    createFile(trainingPath, '')

    // Append names to file
    photos.forEach(photo => {
      updateFile(trainingPath, `${photo}\n`)
    })
  }

  createTearsTesting(userEmail, photos) {
    const testingPath = `${this._storage}/users/${userEmail}/learning/test.txt`

    // Check if storage is well built
    const storageFolder = this._storage
    const usersFolder = `${this._storage}/users/`
    const ownerFolder = `${this._storage}/users/${userEmail}/`
    const learningFolder = `${this._storage}/users/${userEmail}/learning/`
    const backupFolder = `${this._storage}/users/${userEmail}/learning/backup/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create users directory if does not exist
    createDirectory(usersFolder)

    // Create owner directory if does not exist
    createDirectory(ownerFolder)

    // Create learning directory if does not exist
    createDirectory(learningFolder)

    // Create backup directory if does not exist
    createDirectory(backupFolder)

    // Create tears training file
    createFile(testingPath, '')

    // Append names to file
    photos.forEach(photo => {
      updateFile(testingPath, `${photo}\n`)
    })
  }

  createTearsNetwork(userEmail, classes) {
    const networkPath = `${this._storage}/users/${userEmail}/learning/tears.cfg`

    // Check if storage is well built
    const storageFolder = this._storage
    const usersFolder = `${this._storage}/users/`
    const ownerFolder = `${this._storage}/users/${userEmail}/`
    const learningFolder = `${this._storage}/users/${userEmail}/learning/`
    const backupFolder = `${this._storage}/users/${userEmail}/learning/backup/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create users directory if does not exist
    createDirectory(usersFolder)

    // Create owner directory if does not exist
    createDirectory(ownerFolder)

    // Create learning directory if does not exist
    createDirectory(learningFolder)

    // Create backup directory if does not exist
    createDirectory(backupFolder)

    // Create tears network config file
    const filters = (classes + 5) * 3
    const data = `
[net]
batch=64
subdivisions=32
width=416
height=416
channels=3
momentum=0.9
decay=0.0005
angle=0
saturation = 1.5
exposure = 1.5
hue=.1

learning_rate=0.001
burn_in=1000
max_batches = 500200
policy=steps
steps=400000,450000
scales=.1,.1

[convolutional]
batch_normalize=1
filters=32
size=3
stride=1
pad=1
activation=leaky

# Downsample

[convolutional]
batch_normalize=1
filters=64
size=3
stride=2
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=32
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=64
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

# Downsample

[convolutional]
batch_normalize=1
filters=128
size=3
stride=2
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=64
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=128
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=64
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=128
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

# Downsample

[convolutional]
batch_normalize=1
filters=256
size=3
stride=2
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

# Downsample

[convolutional]
batch_normalize=1
filters=512
size=3
stride=2
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

# Downsample

[convolutional]
batch_normalize=1
filters=1024
size=3
stride=2
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=1024
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=512
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=1024
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=512
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=1024
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

[convolutional]
batch_normalize=1
filters=512
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
filters=1024
size=3
stride=1
pad=1
activation=leaky

[shortcut]
from=-3
activation=linear

######################

[convolutional]
batch_normalize=1
filters=512
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=1024
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=1024
activation=leaky

[convolutional]
batch_normalize=1
filters=512
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=1024
activation=leaky

[convolutional]
size=1
stride=1
pad=1
filters=${filters}
activation=linear

[yolo]
mask = 6,7,8
anchors = 10,13,  16,30,  33,23,  30,61,  62,45,  59,119,  116,90,  156,198,  373,326
classes=${classes}
num=9
jitter=.3
ignore_thresh = .7
truth_thresh = 1
random=1

[route]
layers = -4

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[upsample]
stride=2

[route]
layers = -1, 61

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=512
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=512
activation=leaky

[convolutional]
batch_normalize=1
filters=256
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=512
activation=leaky

[convolutional]
size=1
stride=1
pad=1
filters=${filters}
activation=linear

[yolo]
mask = 3,4,5
anchors = 10,13,  16,30,  33,23,  30,61,  62,45,  59,119,  116,90,  156,198,  373,326
classes=${classes}
num=9
jitter=.3
ignore_thresh = .7
truth_thresh = 1
random=1

[route]
layers = -4

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[upsample]
stride=2

[route]
layers = -1, 36

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=256
activation=leaky

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=256
activation=leaky

[convolutional]
batch_normalize=1
filters=128
size=1
stride=1
pad=1
activation=leaky

[convolutional]
batch_normalize=1
size=3
stride=1
pad=1
filters=256
activation=leaky

[convolutional]
size=1
stride=1
pad=1
filters=${filters}
activation=linear

[yolo]
mask = 0,1,2
anchors = 10,13,  16,30,  33,23,  30,61,  62,45,  59,119,  116,90,  156,198,  373,326
classes=${classes}
num=9
jitter=.3
ignore_thresh = .7
truth_thresh = 1
random=1
`

    createFile(networkPath, data)
  }
