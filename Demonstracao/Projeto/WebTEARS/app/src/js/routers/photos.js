// ----- IMPORTS

import path from 'path'
import uniqid from 'uniqid'

import { pick, validRequest, invalidRequest, notAuthorized, notFound, createDirectory, deleteDirectory, deleteFile, base64ToImage, thumbnailImage, log } from '../utils'

import { Router } from 'express'

// ----- DEFINE PHOTOS

export default class Photos {
  // ----- CONSTRUCTOR

  constructor(endpoint, auth, database, storage, python) {
    log('[ROUTER] Launching PHOTOS router', 'INFO')

    // Variables
    this._endpoint = endpoint
    this._auth = auth
    this._database = database
    this._storage = storage
    this._python = python
    this._router = new Router()

    // Set router endpoints
    this.endpoints()

    // Sync existant photos (REMOVE MISSING AND EXCESS)
    // TODO ...
  }

  endpoints() {
    // Photos
    this._router.post('/getPhotos', this._auth, this.getPhotos.bind(this)) // PROTECTED FOR USER
    this._router.get('/getPhoto/:id', this._auth, this.getPhoto.bind(this)) // PROTECTED FOR USER
    this._router.put('/updatePhoto/:id', this._auth, this.updatePhoto.bind(this)) // PROTECTED FOR USER
    this._router.delete('/removePhoto/:id', this._auth, this.removePhoto.bind(this)) // PROTECTED FOR USER

    this._router.post('/getModelThumbnails/:id', this._auth, this.getModelThumbnails.bind(this)) // PROTECTED FOR USER
    this._router.post('/addModelPhoto/:id', this._auth, this.addModelPhoto.bind(this)) // PROTECTED FOR USER
  }

  // ----- GETTERS -----

  get router() {
    return this._router
  }

  // ----- BASIC -----

  async getPhotos(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const body = req.body
    const where = body && body.where ? pick(body.where, ['ModelId', 'createdAt', 'updatedAt']) : {}
    const order = body && body.order ? body.order : [['createdAt', 'DESC']]
    const offset = body && body.offset ? body.offset : 0
    const limit = body && body.limit ? Math.min(body.limit, 100) : 10

    try {
      // Format user query
      const realQuery = {
        attributes: ['id', 'ModelId', 'createdAt', 'updatedAt'],
        where,
        order,
        offset,
        limit,
        include: [{
          model: this._database.model,
          attributes: [],
          where: {
            UserEmail: authEmail,
          }
        }],
        distinct: true,
        raw: true
      }

      // Fetch photos data
      const result = await this._database.photo.findAndCountAll(realQuery)

      // Filter photos rows
      // ...

      // Valid request
      validRequest(res, result)
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async getPhoto(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    try {
      // Fetch photo data
      const photo = await this._database.photo.findById(paramsId)

      if(photo) {
        // const photoId = photo.id

        const model = await photo.getModel()
        // const modelId = model.id

        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Basic information
          const object = photo ? photo.toJSON() : null
          const filter = object ? pick(object, ['id', 'original', 'thumbnail', 'ModelId', 'createdAt', 'updatedAt']) : null

          // Valid request
          validRequest(res, filter)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async updatePhoto(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    const body = req.body

    try {
      // Fetch photo data
      const photo = await this._database.photo.findById(paramsId)

      if(photo) {
        const photoId = photo.id

        const model = await photo.getModel()
        const modelId = model.id
        const modelName = model.label ? model.label.toUpperCase() : 'UNKNOWN'

        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Format user body
          const source = body ? body.source : null

          // Validate temporary source
          await this.validateTemporaryPhoto(userEmail, source)

          // Add new photo to storage
          await this.createStoragePhoto(userEmail, modelId, photoId, source)

          // Add new photo to backups
          await this.createBackupPhoto(modelName, photoId, source)

          // Update photo original and thumbnail path values
          const originalPath = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/original.jpg`
          const thumbnailPath = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/thumbnail.jpg`

          await photo.update({ originalPath, thumbnailPath })

          // Delete all photo annotations
          await this._database.annotation.destroy({
            where: { PhotoId : photoId }
          })

          const object = photo ? photo.toJSON() : null
          const filter = object ? pick(object, ['id', 'ModelId', 'createdAt', 'updatedAt']) : null

          // Valid request
          validRequest(res, filter)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async removePhoto(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    try {
      // Fetch photo data
      const photo = await this._database.photo.findById(paramsId)

      if(photo) {
        const photoId = photo.id

        const model = await photo.getModel()
        const modelId = model.id

        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Remove photo from database
          await photo.destroy()

          // Remove photo from storage
          this.deleteStoragePhoto(userEmail, modelId, photoId) // Remove original, thumbnail, [cropped]

          // Valid request
          validRequest(res)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async getModelThumbnails(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    const body = req.body
    const where = body && body.where ? pick(body.where, ['createdAt', 'updatedAt']) : {}
    const order = body && body.order ? body.order : [['createdAt', 'DESC']]
    const offset = body && body.offset ? body.offset : 0
    const limit = body && body.limit ? Math.min(body.limit, 100) : 10

    try {
      // Fetch model data
      const model = await this._database.model.findById(paramsId)

      if(model) {
        const modelId = model.id

        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Format user query
          const realQuery = {
            // attributes: ['id', 'thumbnail', 'createdAt', 'updatedAt'], DEBUG - Object needs path to return value!
            where,
            order,
            offset,
            limit,
            include: [{
              model: this._database.annotation,
              attributes: ['id']
            }]
          }

          // Fetch model thumbnails
          const result = await model.getPhotos(realQuery)

          // Filter photos data
          const filter = result.map(photo => {
            const object = photo.toJSON()
            const annotations = object.Annotations

            // Filter object
            let filtered = pick(object, ['id', 'thumbnail', 'createdAt', 'updatedAt'])

            // Format annotations
            filtered.annotations = annotations.map(annotation => pick(annotation, ['id']))

            return filtered
          })

          // Valid request
          validRequest(res, filter)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async addModelPhoto(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    const body = req.body

    try {
      // Fetch model data
      const model = await this._database.model.findById(paramsId)

      if(model) {
        const modelId = model.id
        const modelName = model.label ? model.label.toUpperCase() : 'UNKNOWN'

        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Format user body
          const source = body ? body.source : null

          // Validate temporary source
          await this.validateTemporaryPhoto(userEmail, source)

          // Add temporary link between the photo and the model's owner
          const create = await model.createPhoto()
          const photoId = create.id

          // Add photo to storage
          await this.createStoragePhoto(userEmail, modelId, photoId, source)

          // Add new photo to backups
          await this.createBackupPhoto(modelName, photoId, source)

          // Update photo original and thumbnail path values
          const originalPath = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/original.jpg`
          const thumbnailPath = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/thumbnail.jpg`

          await create.update({ originalPath, thumbnailPath })

          const object = create ? create.toJSON() : null
          const filter = object ? pick(object, ['id', 'createdAt', 'updatedAt']) : null

          // Valid request
          validRequest(res, filter)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  // ----- METHODS -----

  async validateTemporaryPhoto(userEmail, base64) {
    const tempId = uniqid()
    const tempPath = `${this._storage}/users/${userEmail}/temps/${tempId}.jpg`

    // Check if storage is well built
    const storageFolder = this._storage
    const usersFolder = `${this._storage}/users/`
    const userFolder = `${this._storage}/users/${userEmail}/`
    const tempsFolder = `${this._storage}/users/${userEmail}/temps/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create users directory if does not exist
    createDirectory(usersFolder)

    // Create user directory if does not exist
    createDirectory(userFolder)

    // Create temps directory if does not exist
    createDirectory(tempsFolder)

    // Create and validate file from base64
    await base64ToImage(tempPath, base64)

    // Delete file
    deleteFile(tempPath)
  }

  async createStoragePhoto(userEmail, modelId, photoId, base64) {
    const originalPath = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/original.jpg`
    const thumbnailPath = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/thumbnail.jpg`

    // Check if storage is well built
    const storageFolder = this._storage
    const usersFolder = `${this._storage}/users/`
    const ownerFolder = `${this._storage}/users/${userEmail}/`
    const modelsFolder = `${this._storage}/users/${userEmail}/models/`
    const modelFolder = `${this._storage}/users/${userEmail}/models/${modelId}/`
    const photoFolder = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create users directory if does not exist
    createDirectory(usersFolder)

    // Create owner directory if does not exist
    createDirectory(ownerFolder)

    // Create models directory if does not exist
    createDirectory(modelsFolder)

    // Create model directory if does not exist
    createDirectory(modelFolder)

    // Create photo directory if does not exist
    createDirectory(photoFolder)

    // Save original base64 locally
    await base64ToImage(originalPath, base64)

    // Save resized base64 locally
    await thumbnailImage(originalPath, thumbnailPath)
  }

  deleteStoragePhoto(userEmail, modelId, photoId) {
    const photoFolder = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/`
    deleteDirectory(photoFolder)
  }

  async createBackupPhoto(modelName, photoId, base64) {
    const originalPath = `${this._storage}/backups/${modelName}/${photoId}.jpg`

    // Check if storage is well built
    const storageFolder = this._storage
    const backupsFolder = `${this._storage}/backups/`
    const nameFolder = `${this._storage}/backups/${modelName}/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create backups directory if does not exist
    createDirectory(backupsFolder)

    // Create name directory if does not exist
    createDirectory(nameFolder)

    // Save original base64 locally
    await base64ToImage(originalPath, base64)
  }

  // ----- PROCESSES -----
  // ...
}
