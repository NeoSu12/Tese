// ----- IMPORTS

import fs from 'fs'
import path from 'path'

import { Router } from 'express'

import { pick, like, getRandomRange, validRequest, invalidRequest, notAuthorized, notFound, deleteDirectory, log } from '../utils'

// ----- DEFINE MODELS

export default class Models {
  // ----- CONSTRUCTOR

  constructor(endpoint, auth, database, storage, python) {
    log('[ROUTER] Launching MODELS router', 'INFO')

    // Variables
    this._endpoint = endpoint
    this._auth = auth
    this._database = database
    this._storage = storage
    this._python = python
    this._router = new Router()

    // Set router endpoints
    this.endpoints()

    // Sync existant models (REMOVE MISSING AND EXCESS)
    // TODO ...
  }

  endpoints() {
    // Models
    this._router.post('/getModels', this._auth, this.getModels.bind(this)) // PROTECTED FOR USER

    this._router.post('/addModel', this._auth, this.addModel.bind(this)) // PROTECTED FOR USER
    this._router.get('/getModel/:id', this._auth, this.getModel.bind(this)) // PROTECTED FOR USER
    this._router.delete('/removeModel/:id', this._auth, this.removeModel.bind(this)) // PROTECTED FOR USER
    this._router.put('/updateModel/:id', this._auth, this.updateModel.bind(this)) // PROTECTED FOR USER
  }

  // ----- GETTERS -----

  get router() {
    return this._router
  }

  // ----- BASIC -----

  async getModels(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const body = req.body

    const where = body && body.where ? like(body.where, ['label', 'createdAt', 'updatedAt']) : {}
    const order = body && body.order ? body.order : [['label', 'ASC']]
    const offset = body && body.offset ? body.offset : 0
    const limit = body && body.limit ? Math.min(body.limit, 100) : 10

    try {
      // Format user where
      const realWhere = Object.assign({}, {UserEmail: authEmail}, where)

      // Format user query
      const realQuery = {
        attributes: ['id', 'label', 'createdAt', 'updatedAt'],
        where: realWhere,
        order,
        offset,
        limit,
        include: [{
          model: this._database.photo
        }],
        distinct: true
      }

      const result = await this._database.model.findAndCountAll(realQuery)
      const rows = result.rows
      const count = result.count

      // Filter models data
      const filter = rows.map(model => {
        const object = model.toJSON()
        const photos = object.Photos
        const photosSize = photos ? photos.length : 0

        // Filter object
        let filtered = pick(object, ['id', 'label', 'createdAt', 'updatedAt'])

        // Format photo
        if(photosSize > 0) {
          const pickRandom = getRandomRange(0, photosSize)
          const photo = photos[pickRandom]

          filtered.photo = pick(photo, ['id', 'thumbnail', 'createdAt', 'updatedAt'])
        }
        else
          filtered.photo = null

        return filtered
      })

      // Valid request
      validRequest(res, {rows: filter, count})
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async getModel(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    try {
      // Fetch model data
      const model = await this._database.model.findById(
        paramsId,
        {
          include: [{
            model: this._database.photo
          }]
        }
      )

      if(model) {
        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Basic information
          const object = model ? model.toJSON() : null
          const photos = await model.Photos
          const photosSize = photos ? photos.length : 0

          // Filter object
          let filter = pick(object, ['id', 'label', 'description', 'createdAt', 'updatedAt'])

          // Format photo
          if(photosSize > 0) {
            const pickRandom = getRandomRange(0, photosSize)
            const photo = photos[pickRandom]
            const photoObject = photo.toJSON()

            // Filter photo data
            filter.photo = pick(photoObject, ['id', 'thumbnail', 'createdAt', 'updatedAt'])
          }
          else
            filter.photo = null

          // Valid request
          validRequest(res, filter)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async addModel(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const body = req.body

    try {
      // Format user body
      const realBody = pick(body, ['label', 'description'])

      // Fetch user data
      const user = await this._database.user.findById(authEmail)

      // Add new model to user
      const create = await user.createModel(realBody)
      const object = create ? create.toJSON() : null
      const filter = object ? pick(object, ['id', 'label', 'description', 'createdAt', 'updatedAt']) : null

      // Valid request
      validRequest(res, filter)
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async removeModel(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    try {
      // Fetch model data
      const model = await this._database.model.findById(paramsId)

      if(model) {
        const modelId = model.id

        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Destroy model
          model.destroy()

          // Remove model from storage
          this.deleteStorageModel(userEmail, modelId)

          // Valid request
          validRequest(res)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async updateModel(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    const body = req.body

    try {
      // Fetch model data
      const model = await this._database.model.findById(paramsId)

      if(model) {
        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Format user body
          const realBody = pick(body, ['label', 'description'])

          // Update model
          await model.update(realBody)

          // Fetch updated model data
          const object = model ? model.toJSON() : null
          const filter = object ? pick(object, ['id', 'label', 'description', 'createdAt', 'updatedAt']) : null

          // Valid request
          validRequest(res, filter)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  // ----- METHODS -----

  deleteStorageModel(userEmail, modelId) {
    const modelFolder = `${this._storage}/users/${userEmail}/models/${modelId}/`
    deleteDirectory(modelFolder)
  }

  // ----- PROCESSES -----
  // ...
}
