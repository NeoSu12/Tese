// ----- IMPORTS

import path from 'path'
import { pick, validRequest, invalidRequest, notFound, createDirectory, deleteFile, croppedImage, log } from '../utils'

import { Router } from 'express'

// ----- DEFINE ANNOTATIONS

export default class Annotations {
  // ----- CONSTRUCTOR

  constructor(endpoint, auth, database, storage) {
    log('[ROUTER] Launching ANNOTATIONS router', 'INFO')

    // Variables
    this._endpoint = endpoint
    this._auth = auth
    this._database = database
    this._storage = storage
    this._router = new Router()

    // Set router endpoints
    this.endpoints()

    // Sync existant annotations (REMOVE MISSING AND EXCESS)
    // TODO ...
  }

  endpoints() {
    // Photos
    this._router.post('/getAnnotations', this._auth, this.getAnnotations.bind(this)) // PROTECTED FOR USER
    this._router.get('/getAnnotation/:id', this._auth, this.getAnnotation.bind(this)) // PROTECTED FOR USER
    this._router.put('/updateAnnotation/:id', this._auth, this.updateAnnotation.bind(this)) // PROTECTED FOR USER
    this._router.delete('/removeAnnotation/:id', this._auth, this.removeAnnotation.bind(this)) // PROTECTED FOR USER

    this._router.post('/getPhotoAnnotations/:id', this._auth, this.getPhotoAnnotations.bind(this)) // PROTECTED FOR USER
    this._router.post('/addPhotoAnnotation/:id', this._auth, this.addPhotoAnnotation.bind(this)) // PROTECTED FOR USER
  }

  // ----- GETTERS -----

  get router() {
    return this._router
  }

  // ----- BASIC -----

  async getAnnotations(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const body = req.body
    const where = body && body.where ? pick(body.where, ['PhotoId', 'createdAt', 'updatedAt']) : {}
    const order = body && body.order ? body.order : [['createdAt', 'DESC']]
    const offset = body && body.offset ? body.offset : 0
    const limit = body && body.limit ? Math.min(body.limit, 100) : 10

    try {
      // Format user query
      const realQuery = {
        attributes: ['id', 'PhotoId', 'createdAt', 'updatedAt'],
        where,
        order,
        offset,
        limit,
        include: [{
          model: this._database.photo,
          attributes: [],
          required: true,
          include: [{
            model: this._database.model,
            attributes: [],
            where: {
              UserEmail: authEmail,
            }
          }]
        }],
        distinct: true,
        raw: true
      }

      // Fetch annotations data
      const result = await this._database.annotation.findAndCountAll(realQuery)

      // Filter annotations rows
      // ...

      // Valid request
      validRequest(res, result)
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async getAnnotation(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    try {
      // Fetch annotation data
      const annotation = await this._database.annotation.findById(paramsId)

      if(annotation) {
        // const annotationId = annotation.id

        const photo = await annotation.getPhoto()
        // const photoId = photo.id

        const model = await photo.getModel()
        // const modelId = model.id

        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Basic information
          const object = annotation ? annotation.toJSON() : null
          const filter = object ? pick(object, ['id', 'cropped', 'topLeftX', 'topLeftY', 'bottomRightX', 'bottomRightY', 'PhotoId', 'createdAt', 'updatedAt']) : null

          // Valid request
          validRequest(res, filter)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async updateAnnotation(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    const body = req.body

    try {
      // Fetch annotation data
      const annotation = await this._database.annotation.findById(paramsId)

      if(annotation) {
        const annotationId = annotation.id

        const photo = await annotation.getPhoto()
        const photoId = photo.id

        const model = await photo.getModel()
        const modelId = model.id

        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Format user body
          const rectangle = body ? pick(body, ['topLeftX', 'topLeftY', 'bottomRightX', 'bottomRightY']) : null

          // Update annotation rectangle values
          await annotation.update(rectangle)

          // Format annotation rectangle values
          const formattedRectangle = {
            'topLeft': {
              x: annotation.topLeftX,
              y: annotation.topLeftY
            },
            'bottomRight': {
              x: annotation.bottomRightX,
              y: annotation.bottomRightY
            }
          }

          // Update annotation cropped photo locally
          await this.createStorageAnnotation(userEmail, modelId, photoId, annotationId, formattedRectangle)

          // Update annotation cropped path value
          const croppedPath = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/annotations/${annotationId}.jpg`

          await annotation.update({ croppedPath })

          const object = annotation ? annotation.toJSON() : null
          const filter = object ? pick(object, ['id', 'topLeftX', 'topLeftY', 'bottomRightX', 'bottomRightY']) : null

          // Valid request
          validRequest(res, filter)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async removeAnnotation(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    try {
      // Fetch annotation data
      const annotation = await this._database.annotation.findById(paramsId)

      if(annotation) {
        const annotationId = annotation.id

        const photo = await annotation.getPhoto()
        const photoId = photo.id

        const model = await photo.getModel()
        const modelId = model.id

        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Remove annotation from database
          await annotation.destroy()

          // Remove annotation from storage
          this.deleteStorageAnnotation(userEmail, modelId, photoId, annotationId)

          // Valid request
          validRequest(res)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async getPhotoAnnotations(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    const body = req.body
    const where = body && body.where ? pick(body.where, ['topLeftX', 'topLeftY', 'bottomRightX', 'bottomRightY', 'createdAt', 'updatedAt']) : {}
    const order = body && body.order ? body.order : [['createdAt', 'DESC']]
    const offset = body && body.offset ? body.offset : 0
    const limit = body && body.limit ? Math.min(body.limit, 100) : 10

    try {
      // Fetch photo data
      const photo = await this._database.photo.findById(paramsId)

      if(photo) {
        const photoId = photo.id

        const model = await photo.getModel()
        // const modelId = model.id

        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Format user query
          const realQuery = {
            attributes: ['id', 'topLeftX', 'topLeftY', 'bottomRightX', 'bottomRightY', 'createdAt', 'updatedAt'],
            where,
            order,
            offset,
            limit,
            raw: true
          }

          // Fetch photo annotations
          const result = await photo.getAnnotations(realQuery)

          // Valid request
          validRequest(res, result)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  async addPhotoAnnotation(req, res) {
    const auth = req.auth
    const authEmail = auth.user

    const params = req.params
    const paramsId = params.id

    const body = req.body

    try {
      // Fetch photo data
      const photo = await this._database.photo.findById(paramsId)

      if(photo) {
        const photoId = photo.id

        const model = await photo.getModel()
        const modelId = model.id

        const user = await model.getUser()
        const userEmail = user.email

        if(userEmail === authEmail) {
          // Add temporary link between the annotation and the model's photo
          const create = await photo.createAnnotation()
          const annotationId = create.id

          // Format user body
          const rectangle = body ? pick(body, ['topLeftX', 'topLeftY', 'bottomRightX', 'bottomRightY']) : null

          // Update annotation rectangle values
          await create.update(rectangle)

          // Format annotation rectangle values
          const formattedRectangle = {
            'topLeft': {
              x: create.topLeftX,
              y: create.topLeftY
            },
            'bottomRight': {
              x: create.bottomRightX,
              y: create.bottomRightY
            }
          }

          // Create annotation cropped photo locally
          await this.createStorageAnnotation(userEmail, modelId, photoId, annotationId, formattedRectangle)

          // Update annotation cropped path value
          const croppedPath = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/annotations/${annotationId}.jpg`

          await create.update({ croppedPath })

          const object = create ? create.toJSON() : null
          const filter = object ? pick(object, ['id', 'topLeftX', 'topLeftY', 'bottomRightX', 'bottomRightY', 'createdAt', 'updatedAt']) : null

          // Valid request
          validRequest(res, filter)
        }
        else {
          // Not authorized
          notAuthorized(res)
        }
      }
      else {
        // Not found
        notFound(res)
      }
    } catch (error) {
      // Invalid request
      invalidRequest(res, error)
    }
  }

  // ----- METHODS -----

  async createStorageAnnotation(userEmail, modelId, photoId, annotationId, rectangle) {
    const originalPath = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/original.jpg`
    const croppedPath = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/annotations/${annotationId}.jpg`

    // Check if storage is well built
    const storageFolder = this._storage
    const ownerFolder = `${this._storage}/users/${userEmail}/`
    const modelsFolder = `${this._storage}/users/${userEmail}/models/`
    const modelFolder = `${this._storage}/users/${userEmail}/models/${modelId}/`
    const photoFolder = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/`
    const annotationsFolder = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/annotations/`

    // Create storage directory if does not exist
    createDirectory(storageFolder)

    // Create owner directory if does not exist
    createDirectory(ownerFolder)

    // Create models directory if does not exist
    createDirectory(modelsFolder)

    // Create model directory if does not exist
    createDirectory(modelFolder)

    // Create photo directory if does not exist
    createDirectory(photoFolder)

    // Create annotations directory if does not exist
    createDirectory(annotationsFolder)

    // Save cropped image locally
    await croppedImage(originalPath, croppedPath, rectangle)
  }

  deleteStorageAnnotation(userEmail, modelId, photoId, annotationId) {
    const croppedFile = `${this._storage}/users/${userEmail}/models/${modelId}/${photoId}/annotations/${annotationId}.jpg`
    deleteFile(croppedFile)
  }

  // ----- PROCESSES -----
  // ...
}
