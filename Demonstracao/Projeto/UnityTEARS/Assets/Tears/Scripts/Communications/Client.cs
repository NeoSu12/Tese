﻿using UnityEngine;

using Communication;
using System;
using Data;

public class Client : MonoBehaviour {


    #region Variables

    #region Events

    public delegate void OnStepEvent(string id, Step step);
    public static event OnStepEvent OnStep;

    public delegate void OnPredictionsEvent(PredictionsPacket predictions);
    public static event OnPredictionsEvent OnPredictions;

    #endregion Events

    #region Public

    public PROTOCOL protocol;
    public string ip;
    public int port;

    public string username;
    public string password;

    #endregion Public

    #region Private

    TearsClient _client;

    #endregion Private

    #endregion Variables

    #region Basic

    void OnEnable()
    {
        TearsClient.OnConnect += ClientOnConnect;
        TearsClient.OnConnectError += ClientOnConnectError;
        TearsClient.OnConnectTimeout += ClientOnConnectTimeout;

        TearsClient.OnReconnect += ClientOnReconnect;
        TearsClient.OnReconnecting += ClientOnReconnecting;

        TearsClient.OnDisconnect += ClientOnDisconnect;

        TearsClient.OnAuth += ClientOnAuth;
        TearsClient.OnPredictions += ClientOnPredictions;
        VideoRecord.OnFrame += ClientOnFrame;
        VideoRecord.OnStep += ClientOnStep;
    }


    void OnDisable()
    {
        TearsClient.OnConnect -= ClientOnConnect;
        TearsClient.OnConnectError -= ClientOnConnectError;
        TearsClient.OnConnectTimeout -= ClientOnConnectTimeout;

        TearsClient.OnReconnect -= ClientOnReconnect;
        TearsClient.OnReconnecting -= ClientOnReconnecting;

        TearsClient.OnDisconnect -= ClientOnDisconnect;

        TearsClient.OnAuth -= ClientOnAuth;
        TearsClient.OnPredictions -= ClientOnPredictions;
        VideoRecord.OnFrame -= ClientOnFrame;
    }

    void OnDestroy()
    {
        // Clear client
        ClearClient();
    }

    void Start()
    {
        // Create socket client for communications
        InitClient();
    }

    void Update()
    {
        // Nothing to do here
    }

    #endregion Basic

    #region Communications

    void InitClient()
    {
        this._client = new TearsClient(this.protocol, this.ip, this.port);
    }

    void ClearClient()
    {
        if (this._client == null)
            return;

        this._client.Disconnect();
    }

    #region Events

    void ClientOnConnect()
    {
        // LogMessage("[COMMUNICATIONS] OK - Client connected");
        this.authenticate(this.username, this.password);
    }

    void ClientOnConnectError(string error)
    {
        // LogMessage(String.Format("[COMMUNICATIONS] ERROR - Could not connect to the server, {0}", error.ToString()));
    }

    void ClientOnConnectTimeout()
    {
        // LogMessage("[COMMUNICATIONS] ERROR - Connection to the server timed out");
    }

    void ClientOnReconnect(int attempt)
    {
        // LogMessage(String.Format("[COMMUNICATIONS] OK - Reconnected to the server after {0} attemps", attempt));
        this.authenticate(this.username, this.password);
    }

    void ClientOnReconnecting(int attempt)
    {
        // LogMessage(String.Format("[COMMUNICATIONS] WARNING - Trying to reconnect to the server ... attempt number {0}", attempt));
    }

    void ClientOnDisconnect()
    {
        // LogMessage("[COMMUNICATIONS] OK - Client disconnected");
    }

    void ClientOnAuth(bool success)
    {
        /*if (success)
            LogMessage("[COMMUNICATIONS] OK - Client authenticated successfully");
        else
            LogMessage("[COMMUNICATIONS] ERROR - Something happened while trying to authenticate");*/
    }

    void ClientOnPredictions(PredictionsPacket packet)
    {
        if (IsOnline() && IsAuthed())
        {
            // Custom callbacks
            if (OnPredictions != null)
            {
                UnityEngine.WSA.Application.InvokeOnAppThread(() =>
                {
                    OnPredictions(packet);
                }, false);
            }
        }
    }

    void ClientOnStep(string id, Step step)
    {
        if (IsOnline() && IsAuthed())
        {
            // Custom callbacks
            if (OnPredictions != null)
            {
                UnityEngine.WSA.Application.InvokeOnAppThread(() =>
                {
                    if (OnStep != null)
                        OnStep(id, step);
                }, false);
            }
        }
    }

    void ClientOnFrame(string id, string base64, int width, int height)
    {
        if (this._client == null)
            return;

        if (IsOnline() && IsAuthed())
            this._client.frame(id, base64, width, height);
    }

    #endregion Events

    #region Methods

    bool IsOnline()
    {
        if (this._client == null)
            return false;

        return this._client.Online;
    }

    bool IsAuthed()
    {
        if (this._client == null)
            return false;

        return this._client.Authed;
    }

    void authenticate(string username, string password)
    {
        if (this._client == null)
            return;

        if (IsOnline())
            this._client.authenticate(username, password);
    }

    #endregion Methods

    #region Auxiliary

    void LogMessage(string message)
    {
        UnityEngine.WSA.Application.InvokeOnAppThread(() =>
        {
            DebugConsole.Log(message);
        }, false);
    }

    #endregion Auxiliary

    #endregion Communications
}
