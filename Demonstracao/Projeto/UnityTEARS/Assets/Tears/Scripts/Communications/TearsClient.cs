﻿using UnityEngine;
using System;
using System.Collections.Generic;

#if UNITY_UWP

using Quobject.SocketIoClientDotNet.Client;
using Quobject.EngineIoClientDotNet.Client.Transports;
using Quobject.Collections.Immutable;

#endif

namespace Communication
{

    public enum PROTOCOL
    {
        HTTP,
        HTTPS
    }

    public class TearsClient
    {

        #region Variables

        #region Events

        public delegate void OnConnectEvent();
        public static event OnConnectEvent OnConnect;

        public delegate void OnConnectErrorEvent(string error);
        public static event OnConnectErrorEvent OnConnectError;

        public delegate void OnConnectTimeoutEvent();
        public static event OnConnectTimeoutEvent OnConnectTimeout;

        public delegate void OnDisconnectEvent();
        public static event OnDisconnectEvent OnDisconnect;

        public delegate void OnErrorEvent();
        public static event OnErrorEvent OnError;

        public delegate void OnMessageEvent();
        public static event OnMessageEvent OnMessage;

        public delegate void OnReconnectEvent(int attempt);
        public static event OnReconnectEvent OnReconnect;

        public delegate void OnReconnectingEvent(int attempt);
        public static event OnReconnectingEvent OnReconnecting;

        public delegate void OnReconnectAttemptEvent();
        public static event OnReconnectAttemptEvent OnReconnectAttempt;

        public delegate void OnReconnectErrorEvent(string error);
        public static event OnReconnectErrorEvent OnReconnectError;

        public delegate void OnReconnectFailedEvent();
        public static event OnReconnectFailedEvent OnReconnectFailed;

        public delegate void OnAuthEvent(bool success);
        public static event OnAuthEvent OnAuth;

        public delegate void OnPredictionsEvent(PredictionsPacket predictions);
        public static event OnPredictionsEvent OnPredictions;

        #endregion Events

        #region Public

        public string Hostname { get { return this._hostname; } }
        public bool Online { get { return this._online; } }
        public bool Authed { get { return this._authed; } }

        #endregion Public

        #region Private

        // Connection
        PROTOCOL _protocol;
        string _protocolString;

        string _ip;
        int _port;
        string _hostname;

        #if UNITY_UWP
        Socket _socket;
        IO.Options _options;
        #endif

        bool _online;
        bool _authed;

        #endregion Private

        #endregion Variables

        #region Constructor

        public TearsClient(PROTOCOL protocol, string ip, int port)
        {
            // LogMessage("[SOCKET] INFO - Initializing client communication's module");

            this._protocol = protocol;
            this._ip = ip;
            this._port = port;

            // Protocol
            this._protocolString = this._protocol == PROTOCOL.HTTP ? "Http" : "Https";

            // Hostname
            this._hostname = string.Format("{0}://{1}:{2}", this._protocolString, this._ip, this._port);

            // Connection options
            this._online = false;
            this._authed = false;

#if UNITY_UWP

            this._socket = null;
            this._options = new IO.Options
            {
                AutoConnect = true,
                Reconnection = true,
                ReconnectionAttempts = 9999,
                ReconnectionDelay = 1000,
                Timeout = 10000,

                IgnoreServerCertificateValidation = true,
                Transports = ImmutableList.Create(new string[] { WebSocket.NAME })
            };

#endif

            // Connect to the server
            Connect();
        }

        #endregion Constructor

        #region Basic

        public void Connect()
        {
#if UNITY_UWP

            if (this._online)
            {
                // LogMessage("[SOCKET] WARNING - Cannot make new connection to the server, client is already connected");
                return;
            }

            // LogMessage("[SOCKET] INFO - Connecting to the server");

            // Create new socket
            this._socket = IO.Socket(this._hostname, this._options);

            // Set socket events
            SetEvents();

#endif
        }

        public void Disconnect()
        {
#if UNITY_UWP

            if (this._socket == null)
            {
                // LogMessage("[SOCKET] WARNING - Cannot disconnect from the server, client is already disconnected");
                return;
            }

            // LogMessage("[SOCKET] OK - Client disconnected");
            this._socket.Disconnect();

#endif
        }

        #endregion Basic

        #region Events

        private void SetEvents()
        {
#if UNITY_UWP

            // On Connect
            this._socket.On(Socket.EVENT_CONNECT, () =>
            {
                // LogMessage("[SOCKET] OK - Client connected");

                this._online = true;

                // Custom callbacks
                if (OnConnect != null)
                    OnConnect();
            });

            // On Connect Error
            this._socket.On(Socket.EVENT_CONNECT_ERROR, (error) =>
            {
                // LogMessage(string.Format("[Socket] ERROR - Could not connect to the server, {0}", error.ToString()));

                // Custom callbacks
                if (OnConnectError != null)
                    OnConnectError(error.ToString());
            });

            // On Connect Timeout
            this._socket.On(Socket.EVENT_CONNECT_TIMEOUT, () =>
            {
                // LogMessage("[SOCKET] ERROR - Connection to the server timed out");

                // Custom callbacks
                if (OnConnectTimeout != null)
                    OnConnectTimeout();
            });

            // On Disconnect
            this._socket.On(Socket.EVENT_DISCONNECT, () =>
            {
                // LogMessage("[SOCKET] WARNING - Client disconnected");

                this._online = false;
                this._authed = false;
                this._socket = null;

                // Custom callbacks
                if (OnDisconnect != null)
                    OnDisconnect();
            });

            // On Error
            this._socket.On(Socket.EVENT_ERROR, () =>
            {
                // Custom callbacks
                if (OnError != null)
                    OnError();
            });

            // On Message
            this._socket.On(Socket.EVENT_MESSAGE, () =>
            {
                // Custom callbacks
                if (OnMessage != null)
                    OnMessage();
            });

            // On Reconnect
            this._socket.On(Socket.EVENT_RECONNECT, (attempt) =>
            {
                // LogMessage(string.Format("[SOCKET] OK - Reconnected to the server after {0} attemps", attempt));

                this._online = true;

                // Custom callbacks
                if (OnReconnect != null)
                    OnReconnect((int) attempt);
            });

            // On Reconnecting
            this._socket.On(Socket.EVENT_RECONNECTING, (attempt) =>
            {
                // LogMessage(string.Format("[SOCKET] WARNING - Trying to reconnect to the server ... attempt number {0}", attempt));

                // Custom callbacks
                if (OnReconnecting != null)
                    OnReconnecting((int) attempt);
            });

            // On Reconnect Attempt
            this._socket.On(Socket.EVENT_RECONNECT_ATTEMPT, () =>
            {
                // Custom callbacks
                if (OnReconnectAttempt != null)
                    OnReconnectAttempt();
            });

            // On Reconnect Error
            this._socket.On(Socket.EVENT_RECONNECT_ERROR, (error) =>
            {
                // Custom callbacks
                if (OnReconnectError != null)
                    OnReconnectError(error.ToString());
            });

            // On Reconnect Failed
            this._socket.On(Socket.EVENT_RECONNECT_FAILED, () =>
            {
                // Custom callbacks
                if (OnReconnectFailed != null)
                    OnReconnectFailed();
            });

            // On Auth
            this._socket.On("auth", (success) =>
            {
                /*if((bool) success)
                    LogMessage("[SOCKET] OK - Client authenticated successfully");
                else
                    LogMessage("[SOCKET] ERROR - Something happened while trying to authenticate");*/

                this._authed = (bool) success;

                // Custom callbacks
                if (OnAuth != null)
                    OnAuth((bool) success);
            });

            // On Predictions
            this._socket.On("predictions", (predictions) =>
            {
                PredictionsPacket packet = JsonUtility.FromJson<PredictionsPacket>((string) predictions);

                // Custom callbacks
                if (OnPredictions != null)
                    OnPredictions(packet);
            });

#endif
        }

        #endregion Events

        #region Methods

        public void authenticate(string username, string password)
        {
#if UNITY_UWP
            AuthPacket packet = new AuthPacket();
            packet.username = username;
            packet.password = password;

            this._socket.Emit("auth", JsonUtility.ToJson(packet));
#endif
        }

        public void frame(string id, string base64, int width, int height)
        {
#if UNITY_UWP
            FramePacket packet = new FramePacket();
            packet.id = id;
            packet.source = base64;
            packet.width = width;
            packet.height = height;

            this._socket.Emit("frame", JsonUtility.ToJson(packet));
#endif
        }

        #endregion Methods

        #region Auxiliary

        void LogMessage(string message)
        {
            UnityEngine.WSA.Application.InvokeOnAppThread(() =>
            {
                DebugConsole.Log(message);
            }, false);
        }

        #endregion Auxiliary
    }

    [Serializable]
    public class AuthPacket
    {
        public string username;
        public string password;
    }

    [Serializable]
    public class FramePacket
    {
        public string id;
        public string source;
        public int width;
        public int height;
    }

    [Serializable]
    public class PredictionsPacket
    {
        public string id;
        public List<Prediction> predictions;
    }

    [Serializable]
    public class Prediction
    {
        public string label;
        public float probability;
        public float x;
        public float y;
        public float width;
        public float height;

        public Prediction(string label, float probability, float x, float y, float width, float height)
        {
            this.label = label;
            this.probability = probability;
            this.x = x;
            this.y = y;
            this.width = width;
            this.height = height;
        }
    }

}
