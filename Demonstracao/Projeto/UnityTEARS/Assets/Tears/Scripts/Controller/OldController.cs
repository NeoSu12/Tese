﻿using System.Collections.Generic;
using UnityEngine;

using System.Linq;
using System;
using System.Collections;

using UnityEngine.XR.WSA.Input;
using Data;

[RequireComponent(typeof(SpatialMapping))]
public class OldController : MonoBehaviour
{

    #region Variables

    #region Public
    public GameObject PointModel
    {
        get
        {
            return pointModel;
        }

        set
        {
            if (pointModel != value)
            {
                pointModel = value;
            }
        }
    }

    #endregion Public

    #region Private

    [SerializeField]
    [Tooltip("Point model to use in mesh point draw.")]
    private GameObject pointModel;
    private List<Point> points;

    private List<Point> cloud;

    private Vector3 headPosition;
    private Vector3 gazeDirection;
    private Vector3 hitPoint;

    private GameObject lastHitObject;
    private GameObject hitObject;
    private bool validHit;

    private GestureRecognizer recognizer;
    private SpatialMapping spatial;

    #endregion Private

    #endregion Variables

    #region Methods

    #region Basic

    private void Awake()
    {
        this.points = new List<Point>();
        this.cloud = new List<Point>();

        this.headPosition = new Vector3();
        this.gazeDirection = new Vector3();
        this.hitPoint = new Vector3();

        this.lastHitObject = null;
        this.hitObject = null;
        this.validHit = false;

        this.recognizer = null;
        this.spatial = gameObject.GetComponent<SpatialMapping>();
    }

    private void Start()
    {
        // Gesture input configuration
        GestureConfiguration();

        // Voice input configuration
        VoiceConfiguration();

        // Prepare drawing cloud points
        DrawConfiguration();

        // Start spatial watcher
        if (this.spatial != null)
            this.spatial.StartWatching();
    }

    private void Update()
    {
        // Camera view update
        this.headPosition = Camera.main.transform.position;
        this.gazeDirection = Camera.main.transform.forward;

        RaycastHit rayHit;
        if (Physics.Raycast(this.headPosition, this.gazeDirection, out rayHit))
        {
            // Update raycast hit
            updateRaycastHit(rayHit);

            // Draw raycast hitted surface
            drawVisualSurface();
        }
    }

    #endregion Basic

    #region Configuration

    #region Draw

    private void DrawConfiguration()
    {
        DebugConsole.Log(String.Format("[{0}] Draw configuration", name));

        int numPoints = 1000;

        // Prepare cloud point with 1000 points (TEMPORARY)
        for (int i = 0; i < numPoints; i++)
        {
            Point temp = new Point(new Vector3(0.0F, 0.0F, 0.0F), new Vector3(0.01F, 0.01F, 0.01F), Color.white, this.pointModel);

            // Add cloud point available to draw
            this.cloud.Add(temp);

            // Build and cook point
            temp.build();
        }

        // Start drawing stars
        StartCoroutine(drawVisualCloud());
    }

    #endregion Draw

    #region Gesture

    private void GestureConfiguration()
    {
        DebugConsole.Log(String.Format("[{0}] Gesture configuration", name));

        this.recognizer = new GestureRecognizer();
        this.recognizer.Tapped += OnClick;

        this.recognizer.StartCapturingGestures();
    }

    private void OnClick(TappedEventArgs args)
    {
        if (this.validHit)
        {
            // Draw point
            if (!removePoint(this.hitPoint, 0.20F))
                drawPoint(this.hitPoint);
        }
    }

    #endregion Gesture

    #region Voice

    private void VoiceConfiguration()
    {
        DebugConsole.Log(String.Format("[{0}] Voice configuration", name));
    }

    #endregion Voice

    #endregion Configuration

    #region Raycast

    private void updateRaycastHit(RaycastHit hit)
    {
        this.hitPoint = hit.point;
        this.hitObject = hit.transform.gameObject;

        // Only validate raycast hit if hit point and object are not null and surface is closer to the viewer (Maximum distance = 3 meters)
        this.validHit = this.hitObject != null && Vector3.Distance(this.headPosition, this.hitPoint) <= 3.0F;
    }

    #endregion Raycast

    #region Draw

    #region Point

    private bool drawPoint(Vector3 position)
    {
        bool result = false;

        Point newPoint = new Point(position, new Vector3(0.05F, 0.05F, 0.05F), Color.blue, this.pointModel, 0.20f);
        if (!this.points.Contains(newPoint))
        {
            result = true;

            // Add point to list
            this.points.Add(newPoint);

            // Draw point gameobject
            newPoint.show();
        }

        return result;
    }

    private bool removePoint(Vector3 position, float betweenDistance)
    {
        bool result = false;

        Point point = nearPoint(position, betweenDistance);
        if (point != null)
        {
            result = true;

            // Remove point from list
            this.points.Remove(point);

            // Destroy point gameobject
            point.destroy();
        }

        return result;
    }

    private Point nearPoint(Vector3 position, float betweenDistance)
    {
        Point found = null;

        foreach (Point point in this.points)
        {
            if (Point.near(point.Position, position, betweenDistance))
            {
                found = point;
                break;
            }
        }

        return found;
    }

    #endregion Point

    #region Surface

    private void drawVisualSurface()
    {
        if (this.lastHitObject != this.hitObject)
        {
            hideSurface(this.lastHitObject);
            drawSurface(this.hitObject);

            this.lastHitObject = this.hitObject;
        }
    }

    private void drawSurface(GameObject obj)
    {
        if (obj != null)
        {
            // Enable new object mesh visualization
            MeshRenderer renderer = obj.GetComponent<MeshRenderer>();

            if (renderer != null)
                renderer.enabled = true;
        }
    }

    private void hideSurface(GameObject obj)
    {
        if (obj != null)
        {
            // Enable new object mesh visualization
            MeshRenderer renderer = obj.GetComponent<MeshRenderer>();

            if (renderer != null)
                renderer.enabled = false;
        }
    }

    #endregion Surface

    #region Cloud

    private IEnumerator drawVisualCloud()
    {
        WaitForSeconds wait = new WaitForSeconds(5.0F);

        int i, j;
        int count;
        int numPoints;
        int maxPoints = 1000;

        List<Vector3> allPoints;
        List<int> randomInts = new List<int>();

        Vector3 tempSurfacePoint;
        Point tempCloudPoint;

        while (true)
        {
            // Get all mesh points
            allPoints = this.spatial.GetPoints();

            count = 0;
            numPoints = allPoints.Count();

            // Get random indexes
            randomInts.Clear();

            for (i = 0; i < maxPoints && i < numPoints; i++)
            {
                randomInts.Add(UnityEngine.Random.Range(0, numPoints));
            }

            foreach (int index in randomInts)
            {
                if (count < maxPoints)
                {
                    tempSurfacePoint = allPoints[index];
                    tempCloudPoint = this.cloud[count];

                    if (tempCloudPoint != null)
                    {
                        tempCloudPoint.show();

                        // Lerp change
                        StartCoroutine(tempCloudPoint.lerp(tempSurfacePoint));

                        // Suddenly change
                        // tempCloudPoint.Position = tempSurfacePoint;
                    }
                    else
                        break;
                }
                else
                    break;

                count++;
            }

            for (j = count; j < this.cloud.Count; j++)
            {
                tempCloudPoint = this.cloud[j];

                if (tempCloudPoint != null)
                {
                    tempCloudPoint.hide();
                }
                else
                    break;
            }

            yield return wait;
        }
    }

    #endregion Cloud

    #endregion Draw

    #endregion Methods
}
