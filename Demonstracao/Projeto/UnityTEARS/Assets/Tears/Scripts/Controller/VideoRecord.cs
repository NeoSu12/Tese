﻿using UnityEngine;
using HoloLensCameraStream;
using System;
using System.IO;
using Data;

#if UNITY_UWP
using Windows.Storage.Streams;
using System.Threading;
using Windows.Graphics.Imaging;
using System.Threading.Tasks;
#endif

public class VideoRecord : MonoBehaviour {

    #region Events

    public delegate void OnFrameEvent(string id, string base64, int width, int height);
    public static event OnFrameEvent OnFrame;

    public delegate void OnStepEvent(string id, Step step);
    public static event OnStepEvent OnStep;

    #endregion Events

    #region Variables

    #region Public

    #endregion Public

    #region Private

    Transform cameraTransform;
    Vector3 position;
    Vector3 up;
    Vector3 right;
    Vector3 forward;

    VideoCapture capture = null;
    CameraParameters parameters;
    HoloLensCameraStream.Resolution resolution;
    float framerate;
    bool recording;

    byte[] imageBytes;
    string imageBase64;

    #endregion Private

    #endregion Variables

    #region Basic

    void OnEnable()
    {
        Controller.OnRequestFrame += RequestFrame;
    }

    void OnDisable()
    {
        Controller.OnRequestFrame -= RequestFrame;
    }

    void OnDestroy()
    {
        // Clear video capture
        ClearVideoCapture();
    }

    void Awake()
    {
        // ...
    }

    void Start()
    {
        // Create video capture
        VideoHelper.Instance.GetVideoCaptureAsync(InitVideoCapture);
    }

    void Update()
    {
        // ...
    }

    #endregion Basic

    #region Methods

    void InitVideoCapture(VideoCapture videoCapture)
    {
        // LogMessage("[VIDEO] OK - Initializing video capture");

        if (videoCapture != null)
        {
            this.capture = videoCapture;
            // LogMessage("[VIDEO] OK - Created a new instance successfully");

            this.resolution = VideoHelper.Instance.GetLowestResolution();
            this.framerate = VideoHelper.Instance.GetHighestFrameRate(this.resolution);
            this.recording = false;

            this.parameters = new CameraParameters
            {
                frameRate = Mathf.RoundToInt(this.framerate),
                cameraResolutionWidth = this.resolution.width,
                cameraResolutionHeight = this.resolution.height,
                pixelFormat = CapturePixelFormat.BGRA32
            };

            this.capture.StartVideoModeAsync(this.parameters, OnStartedVideoCaptureMode);
        }
        else {
            // LogMessage("[VIDEO] ERROR - Failed to create a new instance");
        }
    }

    void ClearVideoCapture()
    {
        if(this.capture != null)
            this.capture.StopVideoModeAsync(OnStoppedVideoCaptureMode);
    }

    void OnStartedVideoCaptureMode(VideoCaptureResult result)
    {
        // LogMessage("[VIDEO] OK - Started recording video");
        this.recording = true;
    }

    void OnStoppedVideoCaptureMode(VideoCaptureResult result)
    {
        // LogMessage("[VIDEO] OK - Stopped recording video");
        this.recording = false;
    }

    void RequestFrame()
    {
        if (this.capture == null || !this.recording)
            return;

        // Grab current camera step
        this.cameraTransform = Camera.main.transform;

        this.position = cameraTransform.position;
        this.up = cameraTransform.up;
        this.right = cameraTransform.right;
        this.forward = cameraTransform.forward;

#if UNITY_UWP
        // Request frame sample
        this.capture.RequestNextFrameSample(OnFrameSampleAcquired);
#endif
    }

    #endregion Methods

    #region Handlers

#if UNITY_UWP
    async void OnFrameSampleAcquired(VideoCaptureSample sample)
    {
        // Generate id
        String frameId = Uniqid("tears-");
        Step step = new Step(this.position, this.up, this.right, this.forward, this.resolution.width, this.resolution.height);

        // Check sample
        if (sample == null)
            return;

        // Check sample bytes
        if (imageBytes == null || imageBytes.Length != sample.dataLength)
            imageBytes = new byte[sample.dataLength];

        // Copy sample
        sample.CopyRawImageDataIntoBuffer(imageBytes);

        // Dispose sample
        sample.Dispose();

        // Encode image
        this.imageBase64 = await EncodeToBase64(this.imageBytes, this.resolution.width, this.resolution.height);

        // Send step position, up, right and forward vectors
        if (OnStep != null)
            OnStep(frameId, step);

        // Send Base64 sample
        if (OnFrame != null)
            OnFrame(frameId, this.imageBase64, this.resolution.width, this.resolution.height);
    }

    async Task<string> EncodeToBase64(byte[] image, int width, int height, double dpiX = 96, double dpiY = 96)
    {
        // Initialize local
        InMemoryRandomAccessStream encoded = new InMemoryRandomAccessStream();
        BitmapEncoder encoder = await BitmapEncoder.CreateAsync(BitmapEncoder.JpegEncoderId, encoded);
        encoder.SetPixelData(BitmapPixelFormat.Bgra8, BitmapAlphaMode.Ignore, (uint)width, (uint)height, dpiX, dpiY, image);

        // Clear image data
        await encoder.FlushAsync();

        // Set stream position
        encoded.Seek(0);

        // Read bytes
        var bytes = new byte[encoded.Size];
        await encoded.AsStream().ReadAsync(bytes, 0, bytes.Length);

        // Convert image to Base64
        return Convert.ToBase64String(bytes);
    }
#endif

    #endregion Handlers

    #region Auxiliary

    void LogMessage(string message)
    {
        UnityEngine.WSA.Application.InvokeOnAppThread(() =>
        {
            DebugConsole.Log(message);
        }, false);
    }

    string Uniqid(string prefix)
    {
        if (string.IsNullOrEmpty(prefix))
            prefix = string.Empty;

        return (prefix + (Guid.NewGuid().ToString()).Substring(0, 13)).ToUpper();
    }

    #endregion Auxiliary
}
