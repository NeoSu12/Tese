﻿using HoloLensCameraStream;
using System;
using System.Linq;
using UnityEngine;

public class VideoHelper : MonoBehaviour
{
	event OnVideoCaptureResourceCreatedCallback VideoCaptureCreated;

	static VideoCapture videoCapture;
	static VideoHelper instance;

	public static VideoHelper Instance {
		get {
			return instance;
		}
	}

	public void SetNativeISpatialCoordinateSystemPtr (IntPtr ptr)
	{
		// videoCapture.WorldOriginPtr = ptr;
	}

	public void GetVideoCaptureAsync (OnVideoCaptureResourceCreatedCallback onVideoCaptureAvailable)
	{
		if (onVideoCaptureAvailable == null)
            Debug.LogError("You must supply the onVideoCaptureAvailable delegate.");

        if (videoCapture == null)
        {
            VideoCaptureCreated += onVideoCaptureAvailable;
        }
        else
            onVideoCaptureAvailable(videoCapture);
	}

	public HoloLensCameraStream.Resolution GetHighestResolution ()
	{
		if (videoCapture == null)
			throw new Exception ("Please call this method after a VideoCapture instance has been created.");
		return videoCapture.GetSupportedResolutions ().OrderByDescending ((r) => r.width * r.height).FirstOrDefault ();
	}

	public HoloLensCameraStream.Resolution GetLowestResolution ()
	{
		if (videoCapture == null)
			throw new Exception ("Please call this method after a VideoCapture instance has been created.");
		return videoCapture.GetSupportedResolutions ().OrderBy ((r) => r.width * r.height).FirstOrDefault ();
	}

	public float GetHighestFrameRate (HoloLensCameraStream.Resolution forResolution)
	{
		if (videoCapture == null)
			throw new Exception ("Please call this method after a VideoCapture instance has been created.");
		return videoCapture.GetSupportedFrameRatesForResolution (forResolution).OrderByDescending (r => r).FirstOrDefault ();
	}

	public float GetLowestFrameRate (HoloLensCameraStream.Resolution forResolution)
	{
		if (videoCapture == null)
			throw new Exception ("Please call this method after a VideoCapture instance has been created.");
		return videoCapture.GetSupportedFrameRatesForResolution (forResolution).OrderBy (r => r).FirstOrDefault ();
	}

	void Awake ()
	{
		if (instance != null) {
			Debug.LogError ("Cannot create two instances of CamStreamManager.");
			return;
		}

		instance = this;
		VideoCapture.CreateAync (OnVideoCaptureInstanceCreated);
	}

	void OnDestroy ()
	{
		if(instance == this)
		    instance = null;
	}

	void OnVideoCaptureInstanceCreated (VideoCapture videoCapture)
	{
		if (videoCapture == null) {
			Debug.LogError ("Creating the VideoCapture object failed.");
			return;
		}

        VideoHelper.videoCapture = videoCapture;
		if (VideoCaptureCreated != null)
			VideoCaptureCreated (videoCapture);
	}
}
