﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.XR.WSA;

namespace Data
{
    #region Enums

    public enum ObserverStates
    {
        Running = 0,
        Stopped = 1
    }

    public enum ObserverVolumeTypes
    {
        AxisAlignedBox = 0,
        OrientedBox = 1,
        Sphere = 2
    }

    #endregion Enums

    public class SpatialMapping : MonoBehaviour
    {
        #region Variables

        #region Public

        public static SpatialMapping instance = null;

        public float TimeBetweenUpdates
        {
            get
            {
                return this.timeBetweenUpdates;
            }

            set
            {
                if (this.timeBetweenUpdates != value)
                {
                    this.timeBetweenUpdates = value;
                }
            }
        }

        public float TrianglesPerCubicMeter
        {
            get
            {
                return this.trianglesPerCubicMeter;
            }

            set
            {
                if (this.trianglesPerCubicMeter != value)
                {
                    this.trianglesPerCubicMeter = value;
                }
            }
        }

        public ObserverVolumeTypes ObserverVolumeType
        {
            get
            {
                return this.observerVolumeType;
            }

            set
            {
                if (this.observerVolumeType != value)
                {
                    this.observerVolumeType = value;
                    SwitchObserverVolume();
                }
            }
        }

        public Vector3 Extents
        {
            get
            {
                return this.extents;
            }
            set
            {
                if (this.extents != value)
                {
                    this.extents = value;
                    SwitchObserverVolume();
                }
            }
        }

        public Vector3 Origin
        {
            get
            {
                return this.origin;
            }
            set
            {
                if (this.origin != value)
                {
                    this.origin = value;
                    SwitchObserverVolume();
                }
            }
        }

        public Quaternion Orientation
        {
            get
            {
                return this.orientation;
            }
            set
            {
                if (this.orientation != value)
                {
                    this.orientation = value;

                    // Only needs to be changed if the corresponding mode is active.
                    if (this.ObserverVolumeType == ObserverVolumeTypes.OrientedBox)
                    {
                        SwitchObserverVolume();
                    }
                }
            }
        }

        public bool DrawVisualMeshes
        {
            get
            {
                return this.drawVisualMeshes;
            }
            set
            {
                if (this.drawVisualMeshes != value)
                {
                    this.drawVisualMeshes = value;
                    SwitchDrawVisualMeshes();
                }
            }
        }

        public MeshTopology VisualMeshesTopology
        {
            get
            {
                return this.visualMeshesTopology;
            }
            set
            {
                if (this.visualMeshesTopology != value)
                {
                    this.visualMeshesTopology = value;
                    SwitchVisualMeshesTopology();
                }
            }
        }

        public Material SurfaceMaterial
        {
            get
            {
                return this.surfaceMaterial;
            }
            set
            {
                if (this.surfaceMaterial != value)
                {
                    this.surfaceMaterial = value;

                    if(this.surfaceMaterial != null)
                        SwitchSurfaceMaterial();
                }
            }
        }


        #endregion Public

        #region Private

        [SerializeField]
        [Tooltip("How long to wait (in sec) between Spatial Mapping updates.")]
        float timeBetweenUpdates = 3.5f;

        [SerializeField]
        [Tooltip("The number of triangles to calculate per cubic meter.")]
        float trianglesPerCubicMeter = 500f;

        [SerializeField]
        [Tooltip("The shape of the observation volume")]
        ObserverVolumeTypes observerVolumeType = ObserverVolumeTypes.AxisAlignedBox;

        [SerializeField]
        [Tooltip("The extents of the observation volume")]
        Vector3 extents = Vector3.one * 10.0f;

        [SerializeField]
        [Tooltip("The origin of the observation volume.")]
        Vector3 origin = Vector3.zero;

        [SerializeField]
        [Tooltip("The direction of the observation volume")]
        Quaternion orientation = Quaternion.identity;

        [Tooltip("Determines if spatial mapping data will be rendered.")]
        [SerializeField]
        bool drawVisualMeshes = false;

        [Tooltip("Determines spatial mapping data topology type")]
        [SerializeField]
        MeshTopology visualMeshesTopology = MeshTopology.Points;

        [Tooltip("The material to use for rendering spatial mapping data.")]
        [SerializeField]
        Material surfaceMaterial;

        // Surface observer
        SurfaceObserver surfaceObserver;

        ObserverStates observerState;
        float lastUpdateTime;

        // Surface update queue
        Queue<SurfaceId> surfaceUpdateQueue;

        // Surface data
        Dictionary<SurfaceId, GameObject> surfaceData;

        // Surface points
        Dictionary<SurfaceId, List<Vector3>> surfacePoints;

        #endregion Private

        #endregion Variables

        #region Methods

        #region Basic

        // Use this for initialization
        void Awake()
        {
            // Check if instance already exists
            if (instance == null)
            {
                // If not, set instance to this
                instance = this;
            }
            // If instance already exists and it's not this
            else
            {
                if (instance != this)
                {
                    // Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a SpatialMapping
                    Destroy(gameObject);
                }
            }

            // Sets this to not be destroyed when reloading scene
            DontDestroyOnLoad(gameObject);

            this.surfaceObserver = new SurfaceObserver();
            SwitchObserverVolume();

            this.observerState = ObserverStates.Stopped;
            this.lastUpdateTime = 0.0F;

            this.surfaceUpdateQueue = new Queue<SurfaceId>();
            this.surfaceData = new Dictionary<SurfaceId, GameObject>();
            this.surfacePoints = new Dictionary<SurfaceId, List<Vector3>>();
        }

        void Start()
        {
            // Start watching surfaces
            StartWatching();
        }

        // Update is called once per frame
        void Update()
        {
            // Surfaces update
            if (this.observerState == ObserverStates.Running)
            {
                if (this.surfaceUpdateQueue.Count > 0)
                {
                    // Simple FIFO rule for requesting meshes (Maybe prioritize the queue based on distance to the user or some other metric?)
                    SurfaceId surfaceId = this.surfaceUpdateQueue.Dequeue();
                    AddOrUpdateSurface(surfaceId);
                }
                else if ((Time.unscaledTime - this.lastUpdateTime) >= this.timeBetweenUpdates)
                {
                    this.surfaceObserver.Update(OnSurfaceChanged);
                    this.lastUpdateTime = Time.unscaledTime;
                }
            }
        }

        void OnDestroy()
        {
            // Stop watching surfaces
            StopWatching();

            // Clear surface observer memory use
            if (this.surfaceObserver != null)
            {
                this.surfaceObserver.Dispose();
                this.surfaceObserver = null;
            }
        }

        #endregion Basic

        #region Calculations

        void OnSurfaceChanged(SurfaceId surfaceId, SurfaceChange changeType, Bounds bounds, DateTime updateTime)
        {
            // Verify that the client of the Surface Observer is expecting updates.
            if (this.observerState != ObserverStates.Running)
            {
                return;
            }

            switch (changeType)
            {
                case SurfaceChange.Added:
                case SurfaceChange.Updated:
                    this.surfaceUpdateQueue.Enqueue(surfaceId);
                    break;

                case SurfaceChange.Removed:
                    RemoveSurface(surfaceId);
                    break;

                default:
                    break;
            }
        }

        void OnDataReady(SurfaceData cookedData, bool outputWritten, float elapsedCookTimeSeconds)
        {
            if (outputWritten)
            {
                SurfaceId id = cookedData.id;
                WorldAnchor anchor = cookedData.outputAnchor;
                MeshFilter filter = cookedData.outputMesh;
                Mesh mesh = filter.sharedMesh;

                List<Vector3> points = this.surfacePoints[id];
                points.Clear();

                Vector3[] tempVertices;
                Vector3 tempPoint;

                // Set mesh topology
                if (this.visualMeshesTopology != MeshTopology.Triangles)
                {
                    mesh.SetIndices(mesh.GetIndices(0), this.visualMeshesTopology, 0);
                }

                // Set mesh points
                tempVertices = mesh.vertices;

                for (int i = 0; i < tempVertices.Length; i++)
                {
                    tempPoint = tempVertices[i];
                    points.Add(anchor.transform.TransformPoint(tempPoint));
                }
            }
            else
            {
                // Request has failed.
            }
        }

        #endregion Calculations

        #region Control

        public void StartWatching()
        {
            // DebugConsole.Log(String.Format("[{0}] Start watching for surface changes", name));

            if (this.surfaceObserver == null)
            {
                this.surfaceObserver = new SurfaceObserver();
            }

            SwitchObserverVolume();

            if (this.observerState != ObserverStates.Running)
            {
                this.observerState = ObserverStates.Running;
                this.lastUpdateTime = 0.0F;
            }
        }

        public void StopWatching()
        {
            if (this.observerState == ObserverStates.Running)
            {
                // DebugConsole.Log(String.Format("[{0}] Stop watching for surface changes", name));

                this.observerState = ObserverStates.Stopped;
                this.lastUpdateTime = 0.0F;

                this.surfaceUpdateQueue.Clear();
                this.surfaceData.Clear();
            }
        }

        #endregion Control

        #region Data

        void AddOrUpdateSurface(SurfaceId id)
        {
            // Mesh data
            if (!this.surfaceData.ContainsKey(id))
            {
                this.surfaceData[id] = new GameObject("Surface - " + id);
                this.surfaceData[id].transform.parent = this.transform;

                MeshRenderer renderer = this.surfaceData[id].AddComponent<MeshRenderer>();

                // Draw mesh
                renderer.enabled = this.drawVisualMeshes;

                // Mesh material
                if(this.surfaceMaterial != null)
                    renderer.sharedMaterial = this.surfaceMaterial;
            }

            // Points data
            if(!this.surfacePoints.ContainsKey(id))
            {
                this.surfacePoints[id] = new List<Vector3>();
            }

            GameObject target = this.surfaceData[id];
            MeshFilter meshFilter = target.GetComponent<MeshFilter>() ?? target.AddComponent<MeshFilter>();
            WorldAnchor worldAnchor = target.GetComponent<WorldAnchor>() ?? target.AddComponent<WorldAnchor>();
            MeshCollider meshCollider = target.GetComponent<MeshCollider>() ?? target.AddComponent<MeshCollider>();

            SurfaceData data = new SurfaceData(
                // The surface id returned from the system
                id,
                // The mesh filter that is populated with the spatial mapping data for this mesh
                meshFilter,
                // The world anchor used to position the spatial mapping mesh in the world
                worldAnchor,
                // The mesh collider that is populated with collider data for this mesh, if true is passed to bakeMeshes below
                meshCollider,
                // Triangles per cubic meter requested for this mesh
                this.trianglesPerCubicMeter,
                // bakeMeshes - If true, the mesh collider is populated, if false, the mesh collider is empty
                true
            );

            if (this.surfaceObserver.RequestMeshAsync(data, OnDataReady))
            {
                // New surface request is in the queue and the specified callback will be invoked at a later frame
            }
            else
            {
                // New surface request has failed. No callback for this request will be issued
            }
        }

        void RemoveSurface(SurfaceId id)
        {
            GameObject surface = this.surfaceData[id];

            // Remove surface from surfaces list
            this.surfaceData.Remove(id);

            // Remove surface from points list
            List<Vector3> list = this.surfacePoints[id];

            if(list != null)
                list.Clear();

            this.surfacePoints.Remove(id);

            // Destroy gameobject
            if (surface != null)
            {
                Destroy(surface);
            }
        }

        List<MeshRenderer> GetRenderers()
        {
            List<MeshRenderer> result = new List<MeshRenderer>();

            foreach (GameObject obj in this.surfaceData.Values)
            {
                result.Add(obj.GetComponent<MeshRenderer>());
            }

            return result;
        }

        List<MeshFilter> GetFilters()
        {
            List<MeshFilter> result = new List<MeshFilter>();

            foreach (GameObject obj in this.surfaceData.Values)
            {
                result.Add(obj.GetComponent<MeshFilter>());
            }

            return result;
        }

        List<Mesh> GetMeshes()
        {
            List<Mesh> result = new List<Mesh>();

            foreach (GameObject obj in this.surfaceData.Values)
            {
                result.Add(obj.GetComponent<MeshFilter>().sharedMesh);
            }

            return result;
        }

        public List<Vector3> GetPoints()
        {
            List<Vector3> result = new List<Vector3>();

            foreach (List<Vector3> partial in this.surfacePoints.Values)
            {
                result.AddRange(partial);
            }

            return result;
        }

        public List<Vector3> GetPoints(SurfaceId id)
        {
            List<Vector3> result = new List<Vector3>();

            if (this.surfacePoints.ContainsKey(id))
            {
                result = this.surfacePoints[id];
            }

            return result;
        }

        public List<Vector3> GetPointsSphere(SurfaceId id, Vector3 center, float radius)
        {
            List<Vector3> result = new List<Vector3>();

            if (this.surfacePoints.ContainsKey(id))
            {
                List<Vector3> list = this.surfacePoints[id];
                
                foreach(Vector3 point in list)
                {
                    if (Point.near(center, point, radius))
                    {
                        result.Add(point);
                    }
                }
            }

            return result;
        }

        #endregion Data

        #region Utils

        void SwitchObserverVolume()
        {
            if (this.surfaceObserver == null)
            {
                return;
            }

            switch (observerVolumeType)
            {
                case ObserverVolumeTypes.AxisAlignedBox:
                    surfaceObserver.SetVolumeAsAxisAlignedBox(origin, extents);
                    break;
                case ObserverVolumeTypes.OrientedBox:
                    surfaceObserver.SetVolumeAsOrientedBox(origin, extents, orientation);
                    break;
                case ObserverVolumeTypes.Sphere:
                    surfaceObserver.SetVolumeAsSphere(origin, extents.magnitude);
                    break;
                default:
                    surfaceObserver.SetVolumeAsAxisAlignedBox(origin, extents);
                    break;
            }
        }

        void SwitchDrawVisualMeshes()
        {
            if (this.surfaceObserver != null)
            {
                List<MeshRenderer> renderers = GetRenderers();

                foreach (MeshRenderer renderer in renderers)
                {
                    if (renderer != null)
                    {
                        renderer.enabled = this.drawVisualMeshes;
                    }
                }
            }
        }

        void SwitchVisualMeshesTopology()
        {
            if (this.surfaceObserver != null)
            {
                List<Mesh> meshes = GetMeshes();

                foreach (Mesh mesh in meshes)
                {
                    if (mesh != null)
                    {
                        mesh.SetIndices(mesh.GetIndices(0), this.visualMeshesTopology, 0);
                    }
                }
            }
        }

        void SwitchSurfaceMaterial()
        {
            if (this.surfaceObserver != null)
            {
                List<MeshRenderer> renderers = GetRenderers();

                foreach (MeshRenderer renderer in renderers)
                {
                    if (renderer != null)
                    {
                        renderer.sharedMaterial = this.surfaceMaterial;
                    }
                }
            }
        }

        #endregion Utils

        #endregion Methods
    }
}
