﻿using System;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.XR.WSA.Input;

using Communication;
using Data;

public class Controller : MonoBehaviour {

    #region Events

    public delegate void OnRequestFrameEvent();
    public static event OnRequestFrameEvent OnRequestFrame;

    #endregion Events

    #region Variables

    #region Public

    public GameObject MagnetModel
    {
        get
        {
            return _magnetModel;
        }

        set
        {
            if (_magnetModel != value)
            {
                _magnetModel = value;
            }
        }
    }

    #endregion Public

    #region Private

    [SerializeField]
    [Tooltip("Magnet model to use in labels draw")]
    GameObject _magnetModel;

    Dictionary<string, Color> _colorMap;
    Dictionary<string, Step> _stepMap;
    Dictionary<string, int> _magnetMap;

    List<GameObject> _magnetPool;

    float _fieldOfView;
    float _aspect;

    GameObject _hitMagnet;
    Vector3 _hitMagnetPoint;
    bool _validMagnetHit;

    GameObject _lastHitSurface;
    GameObject _hitSurface;
    Vector3 _hitSurfacePoint;
    bool _validSurfaceHit;

    GestureRecognizer _recognizer;
    SpatialMapping _spatial;

    #endregion Private

    #endregion Variables

    #region Methods

    #region Basic

    void Enable()
    {
        // DEBUG - Normally we should put events starting here, but it's not working somehow!
        // Client.OnStep += ControllerOnStep;
        // Client.OnPredictions += ControllerOnPredictions;
        // Magnet.OnTimedDestroy += ControllerOnMagnetDestroy;
    }

    void Disable()
    {
        Client.OnStep -= ControllerOnStep;
        Client.OnPredictions -= ControllerOnPredictions;
        Magnet.OnTimedDestroy -= ControllerOnMagnetDestroy;
    }

    void Awake()
    {
        // Initialize events
        Client.OnStep += ControllerOnStep;
        Client.OnPredictions += ControllerOnPredictions;
        Magnet.OnTimedDestroy += ControllerOnMagnetDestroy;

        // Initialize values
        this._colorMap = new Dictionary<string, Color>();
        this._stepMap = new Dictionary<string, Step>();

        this._magnetMap = new Dictionary<string, int>();
        this._magnetPool = new List<GameObject>();

        this._fieldOfView = Camera.main.fieldOfView;
        this._aspect = Camera.main.aspect;

        this._recognizer = null;
        this._spatial = SpatialMapping.instance;
    }

    void Start()
    {
        // Gesture input configuration
        GestureConfiguration();

        // Magnet pool configuration
        MagnetConfiguration();
    }

    void OnApplicationFocus(bool focus)
    {
        if (!focus)
            Application.Quit();
    }

    void Update()
    {
        // ...
    }

    #endregion Basic

    #region Configuration

    void GestureConfiguration()
    {
        // DebugConsole.Log(String.Format("[{0}] Gesture configuration", name));

        this._recognizer = new GestureRecognizer();
        this._recognizer.Tapped += ControllerOnClick;

        this._recognizer.StartCapturingGestures();
    }

    void MagnetConfiguration()
    {
        // DebugConsole.Log(String.Format("[{0}] Magnet garbage collector configuration", name));

        // Instantiate 'N' magnets
        int num = 25;
        for (int i = 0; i < num; i++)
            CreateMagnet();
    }

    #endregion Configuration

    #region Raycast

    void UpdateRays(Vector3 origin, Vector3 direction)
    {
        int magnetsLayer = LayerMask.GetMask("Spatial Magnets"); // 30
        int surfacesLayer = LayerMask.GetMask("Spatial Mapping"); // 31

        // Surface raycast
        RaycastHit raySurfaceHit;
        if (Physics.Raycast(origin, direction, out raySurfaceHit, 5.0f, 31))
        {
            // Update raycast hit
            UpdateRaycastSurfaceHit(raySurfaceHit);

            // Draw raycast hitted surface
            DrawVisualSurface();
        }
        else
        {
            this._validSurfaceHit = false;
        }

        // Magnets raycast
        RaycastHit rayMagnetHit;
        if (Physics.Raycast(origin, direction, out rayMagnetHit, 5.0f, magnetsLayer))
        {
            // Update raycast hit
            UpdateRaycastMagnetHit(rayMagnetHit);
        }
        else
        {
            this._validMagnetHit = false;
        }
    }

    void UpdateRaycastSurfaceHit(RaycastHit hit)
    {
        this._hitSurfacePoint = hit.point;
        this._hitSurface = hit.transform.gameObject;

        // Only validate raycast hit if hit point and object are not null
        this._validSurfaceHit = this._hitSurface != null;
    }

    void UpdateRaycastMagnetHit(RaycastHit hit)
    {
        this._hitMagnetPoint = hit.point;
        this._hitMagnet = hit.transform.gameObject;

        // Only validate raycast hit if hit point and object are not null
        this._validMagnetHit = this._hitMagnet != null;
    }

    #endregion Raycast

    #region Step

    void ClearStep(string id)
    {
        if(this._stepMap.ContainsKey(id))
            this._stepMap.Remove(id);
    }

    #endregion Step

    #region Prediction

    void ProcessPrediction(string id, int index, Prediction prediction)
    {
        string predictionId = String.Format("{0}_{1}", id, index);

        string label = prediction.label;
        float probability = prediction.probability;

        // Generate color if necessary
        if (!this._colorMap.ContainsKey(label))
            this._colorMap.Add(label, RandomColor());

        Color color = this._colorMap[label];

        // Calculate real position and return width and height
        Vector2 size = CalculateDeviation(id, prediction);

        // If any valid hit draw magnet
        if (this._validMagnetHit || this._validSurfaceHit)
            DrawMagnet(predictionId, prediction, color, label, probability, size);
    }

    Vector2 CalculateDeviation(string id, Prediction prediction)
    {
        Vector2 result = new Vector2();

        if (this._stepMap.ContainsKey(id))
        {
            // Calculate position and check hit object
            Step step = this._stepMap[id];

            // Shoot new rays to handle plane proximity distance
            UpdateRays(step.position, step.forward);

            float x = prediction.x;
            float y = prediction.y;
            float w = prediction.width;
            float h = prediction.height;

            if (this._validSurfaceHit)
            {
                // Frustum planes calculation
                float ratioWidth = step.width / Screen.width;
                float ratioHeight = step.height / Screen.height;

                Vector3 up = step.up;
                Vector3 down = -up;
                Vector3 right = step.right;
                Vector3 left = -right;

                float distance = Vector3.Distance(step.position, this._hitSurfacePoint);
                float customHeight = distance + Mathf.Tan(this._fieldOfView * 0.5f * Mathf.Deg2Rad);
                float customWidth = customHeight * this._aspect;

                customWidth = customWidth * ratioWidth;
                customHeight = customHeight * ratioHeight;

                // Calculate positions
                float percentageX = x / step.width;
                float percentageY = y / step.height;

                float percentageW = w / step.width;
                float percentageH = h / step.height;

                float positionX = percentageX * customWidth;
                float positionY = percentageY * customHeight;

                result.x = percentageW * customWidth;
                result.y = percentageH * customHeight;

                Vector3 customCenter = step.position + step.forward * distance;
                Vector3 customTopLeft = customCenter + (up * customHeight * 0.5f) + (left * customWidth * 0.5f);

                Vector3 finalPoint = customTopLeft + (down * (positionY + 0.045f * positionY)) + (right * (positionX + 0.045f * positionX));
                // Vector3 finalPoint = customTopLeft + (down * positionY) + (right * positionX);
                Vector3 deviation = finalPoint - step.position;

                // Shoot new rays to handle object labelling
                UpdateRays(step.position, deviation);
            }
        }

        return result;
    }

    #endregion Prediction

    #region Draw

    #region Magnet

    GameObject CreateMagnet()
    {
        // Instatiate new magnet
        GameObject magnet = Instantiate(this._magnetModel, Vector3.zero, Quaternion.identity);

        // Make it invisible
        magnet.SetActive(false);

        // Add magnet to pool
        this._magnetPool.Add(magnet);

        return magnet;
    }

    GameObject FetchMagnet(string id)
    {
        GameObject result = null;

        int size = this._magnetPool.Count;
        int index = 0;
        for(index = 0; index < size; index++)
        {
            GameObject magnet = this._magnetPool[index];
            if (!magnet.activeSelf)
            {
                result = magnet;
                break;
            }
        }

        // If no magnet is available, create a new one
        if (!result)
            result = CreateMagnet();

        // Make it visible
        result.SetActive(true);

        // Add magnet to mapped magnets
        this._magnetMap.Add(id, index + 1);

        return result;
    }

    void ClearMagnet(string id)
    {
        if (this._magnetMap.ContainsKey(id))
        {
            // Get magnet reference
            int index = this._magnetMap[id];
            GameObject magnet = this._magnetPool[index];

            // Make it invisible
            magnet.SetActive(false);

            // Remove magnet from mapped magnets
            this._magnetMap.Remove(id);
        }
    }

    bool DrawMagnet(string id, Prediction prediction, Color color, string label, float probability, Vector2 size)
    {
        bool result = false;
        bool done = false;

        // Check prediction values
        if(prediction != null)
        {
            // Check if any object was hit during raycast process
            if (this._validSurfaceHit|| this._validMagnetHit)
            {
                // Check if there was a collision with a magnet object and labels are equal
                if(this._validMagnetHit)
                {
                    Magnet data = this._hitMagnet.GetComponent<Magnet>();
                    
                    if (data != null)
                    {
                        string dataId = data.id;
                        string dataLabel = data.text;

                        // Check if hit object has the same label
                        if (label.Equals(dataLabel))
                        {
                            // Update values
                            data.UpdateValues(dataId, this._hitSurfacePoint, color, label, probability, size);

                            // All done
                            done = true;
                        }
                    }
                }

                // If there was no magnet object or there was a magnet object hit with difference labels check if there was a surface collision
                if(!done && this._validSurfaceHit)
                {
                    UpdateMagnet(id, this._hitSurfacePoint, color, label, probability, size);
                }
            }
        }

        return result;
    }

    void UpdateMagnet(string id, Vector3 position, Color color, string label, float probability, Vector2 size)
    {
        // Get new magnet
        GameObject magnet = FetchMagnet(id);

        if(magnet != null)
        {
            // Configure magnet
            Magnet data = magnet.GetComponent<Magnet>();

            if (data != null)
            {
                // Update values
                data.UpdateValues(id, position, color, label, probability, size);
            }
        }
    }

    #endregion Magnet

    #region Surface

    void DrawVisualSurface()
    {
        if (this._lastHitSurface != this._hitSurface)
        {
            HideSurface(this._lastHitSurface);
            DrawSurface(this._hitSurface);

            this._lastHitSurface = this._hitSurface;
        }
    }

    void DrawSurface(GameObject obj)
    {
        if (obj != null)
        {
            // Enable new object mesh visualization
            MeshRenderer renderer = obj.GetComponent<MeshRenderer>();

            if (renderer != null)
                renderer.enabled = true;
        }
    }

    void HideSurface(GameObject obj)
    {
        if (obj != null)
        {
            // Enable new object mesh visualization
            MeshRenderer renderer = obj.GetComponent<MeshRenderer>();

            if (renderer != null)
                renderer.enabled = false;
        }
    }

    #endregion Surface

    #endregion Draw

    #region Handlers

    void ControllerOnClick(TappedEventArgs args)
    {
        if (OnRequestFrame != null)
            OnRequestFrame();
    }

    void ControllerOnStep(string id, Step step)
    {
        // Add value to step map
        this._stepMap.Add(id, step);
    }

    void ControllerOnPredictions(PredictionsPacket packet)
    {
        string id = packet.id;
        List<Prediction> predictions = packet.predictions;

        if (this._stepMap.ContainsKey(id))
        {
            Step step = this._stepMap[id];
            Prediction closest = null;
            float minDistance = float.MaxValue;
            float minX = float.MaxValue;
            float minY = float.MaxValue;

            foreach (Prediction prediction in predictions)
            {
                float x = prediction.x;
                float y = prediction.y;
                float pX = x / step.width;
                float pY = y / step.height;

                float mX = Mathf.Abs(pX - 0.5f);
                float mY = Mathf.Abs(pY - 0.5f);
                float mDistance = mX + mY;

                if(mDistance < minDistance)
                {
                    minDistance = mDistance;
                    minX = mX;
                    minY = mY;
                    closest = prediction;
                }
            }

            // Verify and process closest prediction to center
            if (closest != null && minX < 0.15f && minY < 0.15f)
                ProcessPrediction(id, 0, closest);

            // Clear step
            ClearStep(id);
        }
    }

    void ControllerOnMagnetDestroy(string id)
    {
        ClearMagnet(id);
    }

    #endregion Handlers

    #region Auxiliary

    Color RandomColor()
    {
        return UnityEngine.Random.ColorHSV(0f, 1f, 1f, 1f, 0.5f, 1f);
    }

    #endregion Auxiliary

    #endregion Methods
}
