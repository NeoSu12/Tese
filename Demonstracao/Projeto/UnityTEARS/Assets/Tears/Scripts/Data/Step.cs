﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    public class Step
    {
        #region Variables

        #region Public

        public Vector3 position;
        public Vector3 up;
        public Vector3 right;
        public Vector3 forward;

        public float width;
        public float height;

        #endregion Public

        #region Private


        #endregion Private

        #endregion Variables

        #region Constructor

        public Step(Vector3 position, Vector3 up, Vector3 right, Vector3 forward, float w, float h)
        {
            this.position = new Vector3(position.x, position.y, position.z);
            this.up = new Vector3(up.x, up.y, up.z);
            this.right = new Vector3(right.x, right.y, right.z);
            this.forward = new Vector3(forward.x, forward.y, forward.z);
            this.width = w;
            this.height = h;
        }

        #endregion Constructor
    }
}
