﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Data
{
    public class Point
    {

        #region Variables

        #region Public

        public GameObject Body
        {
            get
            {
                return this.body;
            }

            set
            {
                if (this.body != value)
                {
                    this.body = value;

                    this.transform = this.body.GetComponent<Transform>();
                    this.renderer = this.body.GetComponent<Renderer>();

                    // Hide object
                    hide();
                }
            }
        }

        public Vector3 Position
        {
            get
            {
                return this.position;
            }

            set
            {
                if (this.position != value)
                {
                    this.position = value;

                    setPosition();
                }
            }
        }

        public Vector3 Scale
        {
            get
            {
                return this.scale;
            }

            set
            {
                if (this.scale != value && this.body != null)
                {
                    this.scale = value;

                    setScale();
                }
            }
        }

        public Color Color
        {
            get
            {
                return this.color;
            }

            set
            {
                if (this.color != value && this.body != null)
                {
                    this.color = value;

                    setColor();
                }
            }
        }

        #endregion Public

        #region Private

        private GameObject body;
        private Transform transform;
        private Renderer renderer;

        private GameObject model;

        private Vector3 position;
        private bool changingPosition;

        private Vector3 scale;
        private Color color;

        private float betweenDistance; // Meters

        #endregion Private

        #endregion Variables

        #region Constructor

        public Point(Vector3 p, Vector3 s, Color c, GameObject m)
        {
            this.model = m;

            this.position = p;
            this.scale = s;
            this.color = c;

            this.betweenDistance = 0.10f;
        }

        public Point(Vector3 p, Vector3 s, Color c, GameObject m, float d)
        {
            this.model = m;

            this.position = p;
            this.scale = s;
            this.color = c;

            this.betweenDistance = d;
        }

        #endregion Constructor

        #region Methods

        #region Public

        public void build()
        {
            setObject();
            setPosition();
            setScale();
            setColor();
        }

        public void show()
        {
            if (this.body == null)
            {
                build();
            }

            if (this.body != null)
            {
                this.body.SetActive(true);
            }
        }

        public void hide()
        {
            if (this.body != null)
            {
                this.body.SetActive(false);
            }
        }

        public void destroy()
        {
            if (this.body != null)
            {
                GameObject.Destroy(body);
            }
        }

        public IEnumerator lerp(Vector3 end)
        {
            float time = 0.0f;
            float lerpSpeed = 0.05F;

            while (this.transform.position != end)
            {
                if (time < 1.0f)
                {
                    time += lerpSpeed * Time.deltaTime;
                    this.transform.position = Vector3.Lerp(this.transform.position, end, time);
                }
                yield return null;
            }
        }

        public override bool Equals(object obj)
        {
            var item = obj as Point;

            if (item == null)
            {
                return false;
            }

            return near(this.position, item.position, this.betweenDistance);
        }

        public override int GetHashCode()
        {
            return this.position.GetHashCode();
        }

        #endregion Public

        #region Private

        private void setObject()
        {
            if (this.model != null)
                this.body = (GameObject)GameObject.Instantiate(this.model);
            else
                this.body = GameObject.CreatePrimitive(PrimitiveType.Cube);

            this.transform = this.body.GetComponent<Transform>();
            this.renderer = this.body.GetComponent<Renderer>();

            // Hide object
            hide();
        }

        private void setPosition()
        {
            this.transform.position = this.position;
        }

        private void setScale()
        {
            this.transform.localScale = this.scale;
        }

        private void setColor()
        {
            this.renderer.material.color = Color.black;
            this.renderer.material.SetColor("_EmissionColor", this.color);
        }

        #endregion Private

        #endregion Methods

        #region Auxiliary

        public static bool near(Vector3 pos1, Vector3 pos2, float distance)
        {
            return Vector3.Distance(pos1, pos2) < distance;
        }

        #endregion Auxiliary

    }
}
