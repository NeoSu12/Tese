Shader "TEARS/Wireframe"
{
    Properties
    {
        _BaseColor("Base color", Color) = (0.0, 0.0, 0.0, 1.0)
        _WireColor("Wire color", Color) = (1.0, 1.0, 1.0, 1.0)
        _WireThickness("Wire thickness", Range(0, 800)) = 100
    }
    SubShader
    {
        Tags { "RenderType" = "Opaque" }

        Pass
        {
            Offset 50, 100

            CGPROGRAM
            #pragma vertex vert
            #pragma geometry geom
            #pragma fragment frag

            #pragma target 5.0
            #pragma only_renderers d3d11

            #include "UnityCG.cginc"

            float4 _BaseColor;
            float4 _WireColor;
            float _WireThickness;

			struct appdata {
				float4 vertex : POSITION;
			};

            struct v2g
            {
                float4 vertex : SV_POSITION;
            };

			struct g2f
			{
				float4 vertex : SV_POSITION;

				float inverseW : TEXCOORD0;
				float3 dist : TEXCOORD1;
			};

            v2g vert(appdata v)
            {
                v2g o;

                o.vertex = UnityObjectToClipPos(v.vertex);

                return o;
            }

            [maxvertexcount(3)]
            void geom(triangle v2g input[3], inout TriangleStream<g2f> output)
            {
                // Calculate the vectors that define the triangle from the input points.
                float2 point0 = input[0].vertex.xy / input[0].vertex.w;
                float2 point1 = input[1].vertex.xy / input[1].vertex.w;
                float2 point2 = input[2].vertex.xy / input[2].vertex.w;

                // Calculate the area of the triangle.
                float2 vector0 = point2 - point1;
                float2 vector1 = point2 - point0;
                float2 vector2 = point1 - point0;
                float area = abs(vector1.x * vector2.y - vector1.y * vector2.x);

                float3 distScale[3];
                distScale[0] = float3(area / length(vector0), 0, 0);
                distScale[1] = float3(0, area / length(vector1), 0);
                distScale[2] = float3(0, 0, area / length(vector2));

                float wireScale = 800 - _WireThickness;

                g2f o;

				[unroll]
                for (int i = 0; i < 3; i++)
                {
                   o.vertex = input[i].vertex;
                   o.inverseW = 1.0 / o.vertex.w;
                   o.dist = distScale[i] * o.vertex.w * wireScale;

				   output.Append(o);
                }
            }

            float4 frag(g2f i) : COLOR
            {
                float dist = min(i.dist[0], min(i.dist[1], i.dist[2])) * i.inverseW;
                float I = exp2(-2 * dist * dist);
                float4 color = I * _WireColor + (1 - I) * _BaseColor;

                color.a = I;
                return color;
            }
            ENDCG
        }
    }
    FallBack "Diffuse"
}
