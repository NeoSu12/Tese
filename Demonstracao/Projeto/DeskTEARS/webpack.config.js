// ----- IMPORTS

const path = require("path")
const webpack = require("webpack")
const html = require("html-webpack-plugin")

// ----- PLUGINS

const desktopHTML = new html(
  {
    template: __dirname + "/src/html/desktop.html",
    filename: "./desktop.html"
  }
)

module.exports = [
  {
    // ----- WEBPACK ENTRIES

    watch: true,
		target: "web",
		entry: [
      __dirname + "/src/desktop.js"
    ],
		output: {
			path: __dirname + "/dist",
			filename: "desktop.js",
			library: "Desktop",
			libraryTarget: "umd",
			umdNamedDefine: true
		},

    // ----- BABEL + ESLINT + HTML + CSS + FILES

		module: {
			rules: [
        // ----- BABEL

        {
					test: /(\.js|\.jsx)$/,
					include: path.resolve(__dirname, "src"),
          exclude: path.resolve(__dirname, "node_modules"),
					use: [
            {
              loader: 'babel-loader'
            }
          ]
				},

        // ----- ESLINT

        {
          enforce: "pre",
					test: /(\.js)$/,
					include: path.resolve(__dirname, "src"),
          exclude: path.resolve(__dirname, "node_modules"),
          use: [
            {
              loader: 'eslint-loader',
              options: {
                fix: true,
                quiet: true
              }
            }
          ]
				},

        // ----- HTML

        {
          test: /(\.html)$/,
          include: path.resolve(__dirname, "src/html"),
          exclude: path.resolve(__dirname, "node_modules"),
          use: [
            {
              loader: 'html-loader',
              options: {
                minimize: true
              }
            }
          ]
        },

        // ----- FILES

        {
          test: /\.(png|svg|jpg|gif)$/,
          include: path.resolve(__dirname, "src/assets"),
          exclude: path.resolve(__dirname, "node_modules"),
          use: [
            {
              loader: 'file-loader',
              options: {
                name: '[path][name].[ext]'
              }
            }
          ]
        }

			]
		},
    plugins: [
      desktopHTML
    ]
	}
]
