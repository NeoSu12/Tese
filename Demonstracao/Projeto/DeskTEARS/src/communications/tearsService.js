// ----- IMPORTS

import sha512 from 'sha512'

// ----- DEFINE TEARS SERVICE

export default class TService {
  // ----- CONSTRUCTOR

  constructor() {
    // Initialize
    this.initialize()
  }

  // ----- BASIC

  initialize() {
    // User
    this.username = null
    this.secret = null

    // Client information
    this.client = null
  }

  // ----- METHODS

  // Validation

  validation() {
    console.log(`Trying to validate logged user`)
    let self = this

    return new Promise((resolve, reject) => {
      let cached = this.getAuth()

      if(cached) {
        // User
        self.username = cached.username
        self.secret = cached.secret

        // Client information
        self.client = cached.client

        // Fetch update user profile information
        self.get()
        .then(data => {
          resolve(data)
        })
        .catch(error => {
          // Clear cache
          self.clearAuth()

          reject()
        })
      }
      else {
        reject()
      }
    })
  }

  // Authentication

  authenticate(username, password) {
    console.log(`User '${username}' is trying to authenticate`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'GET'
      let url = `http://localhost:8080/api/users/auth`

      // Hash password using sha512
      let hashPassword = sha512(password).toString('hex')

      let encoded = window.btoa(`${username}:${hashPassword}`)
      let auth = `Basic ${encoded}`

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.send()

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
          case 201:
            let status = result.code
            let client = result.body

            // User
            self.username = username
            self.secret = encoded

            // Model
            self.client = client

            // Save to cache
            let cached = {
              status,
              username: self.username,
              secret: self.secret,
              client: self.client
            }
            this.setAuth(cached)

            resolve(cached)
            break

          case 401:
            error.code = 401
            error.message = 'Unauthorized access'

            reject(error)
            break

          case 403:
            error.code = 403
            error.message = 'No credentials provided'

            reject(error)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  logout() {
    console.log(`User '${this.username}' is trying to logout`)

    // Clear local
    this.username = null
    this.secret = null
    this.client = null

    // Clear cache
    this.clearAuth()

    // Logout with endpoint
    // TODO ...
  }

  // Users

  get() {
    console.log(`User '${this.username}' is trying to fetch updated profile information`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'GET'
      let url = `http://localhost:8080/api/users/getUser/${self.username}`

      let auth = `Basic ${self.secret}`

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.send()

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
          case 201:
            let status = result.code
            let client = result.body

            // Model
            self.client = client

            // Save to cache
            let cached = {
              status,
              username: self.username,
              secret: self.secret,
              client: self.client
            }
            this.setAuth(cached)

            resolve(cached)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  getUsers(filter) {
    console.log(`Anonymous user is trying to fetch all available users in the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'POST'
      let url = `http://localhost:8080/api/users/getUsers`

      let body = filter

      request.open(method, url, true)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let users = result.body
            resolve(users)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  // Objects

  getObjects(filter) {
    console.log(`User '${this.username}' is trying to fetch owned object models from the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'POST'
      let url = `http://localhost:8080/api/models/getModels`

      let auth = `Basic ${self.secret}`
      let body = filter

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let models = result.body
            resolve(models)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  getObject(id) {
    console.log(`User '${this.username}' is trying to fetch object model '${id}' from the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'GET'
      let url = `http://localhost:8080/api/models/getModel/${id}`

      let auth = `Basic ${self.secret}`

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.send()

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let model = result.body
            resolve(model)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  updateObject(id, value) {
    console.log(`User '${this.username}' is trying to update object model '${id}' in the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'PUT'
      let url = `http://localhost:8080/api/models/updateModel/${id}`

      let auth = `Basic ${self.secret}`
      let body = value

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let model = result.body
            resolve(model)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  addObject(value) {
    console.log(`User '${this.username}' is trying to add a new object model to the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'POST'
      let url = `http://localhost:8080/api/models/addModel`

      let auth = `Basic ${self.secret}`
      let body = value

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let model = result.body
            resolve(model)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  removeObject(id) {
    console.log(`User '${this.username}' is trying to remove object model '${id}' from the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'DELETE'
      let url = `http://localhost:8080/api/models/removeModel/${id}`

      let auth = `Basic ${self.secret}`

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.send()

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            resolve()
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  // Photos

  getPhotos(filter) {
    console.log(`User '${this.username}' is trying to fetch all photos from the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'POST'
      let url = `http://localhost:8080/api/photos/getPhotos`

      let auth = `Basic ${self.secret}`
      let body = filter

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let photos = result.body
            resolve(photos)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  getPhoto(id) {
    console.log(`User '${this.username}' is trying to fetch photo '${id}' from the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'GET'
      let url = `http://localhost:8080/api/photos/getPhoto/${id}`

      let auth = `Basic ${self.secret}`

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.send()

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let photo = result.body
            resolve(photo)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  updatePhoto(id, value) {
    console.log(`User '${this.username}' is trying to update photo '${id}' in the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'PUT'
      let url = `http://localhost:8080/api/photos/updatePhoto/${id}`

      let auth = `Basic ${self.secret}`
      let body = {
        source: value
      }

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let photo = result.body
            resolve(photo)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  removePhoto(id) {
    console.log(`User '${this.username}' is trying to remove photo '${id}' from the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'DELETE'
      let url = `http://localhost:8080/api/photos/removePhoto/${id}`

      let auth = `Basic ${self.secret}`

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.send()

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            resolve()
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  addObjectPhoto(id, value) {
    console.log(`User '${this.username}' is trying to add a new photo to object model '${id}' in the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'POST'
      let url = `http://localhost:8080/api/photos/addModelPhoto/${id}`

      let auth = `Basic ${self.secret}`
      let body = {
        source: value
      }

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let photo = result.body
            resolve(photo)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  getObjectThumbnails(id, filter) {
    console.log(`User '${this.username}' is trying to fetch object model '${id}' thumbnails from the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'POST'
      let url = `http://localhost:8080/api/photos/getModelThumbnails/${id}`

      let auth = `Basic ${self.secret}`
      let body = filter

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let thumbnails = result.body
            resolve(thumbnails)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  // Annotations

  getAnnotations(filter) {
    console.log(`User '${this.username}' is trying to fetch all annotations from the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'POST'
      let url = `http://localhost:8080/api/annotations/getAnnotations`

      let auth = `Basic ${self.secret}`
      let body = filter

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let annotations = result.body
            resolve(annotations)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  getAnnotation(id) {
    console.log(`User '${this.username}' is trying to fetch annotation '${id}' from the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'GET'
      let url = `http://localhost:8080/api/annotations/getAnnotation/${id}`

      let auth = `Basic ${self.secret}`

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.send()

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let annotation = result.body
            resolve(annotation)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  updateAnnotation(id, value) {
    console.log(`User '${this.username}' is trying to update annotation '${id}' in the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'PUT'
      let url = `http://localhost:8080/api/annotations/updateAnnotation/${id}`

      let auth = `Basic ${self.secret}`
      let body = value

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let annotation = result.body
            resolve(annotation)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  removeAnnotation(id) {
    console.log(`User '${this.username}' is trying to remove annotation '${id}' from the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'DELETE'
      let url = `http://localhost:8080/api/annotations/removeAnnotation/${id}`

      let auth = `Basic ${self.secret}`

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.send()

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            resolve()
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  addPhotoAnnotation(id, value) {
    console.log(`User '${this.username}' is trying to add a new annotation to photo '${id}' in the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'POST'
      let url = `http://localhost:8080/api/annotations/addPhotoAnnotation/${id}`

      let auth = `Basic ${self.secret}`
      let body = value

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let annotation = result.body
            resolve(annotation)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  getPhotoAnnotations(id, filter) {
    console.log(`User '${this.username}' is trying to fetch photo '${id}' annotations from the platform`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'POST'
      let url = `http://localhost:8080/api/annotations/getPhotoAnnotations/${id}`

      let auth = `Basic ${self.secret}`
      let body = filter

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let annotations = result.body
            resolve(annotations)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  // Learning

  learn() {
    console.log(`User '${this.username}' is trying to initiate intelligent system learning process`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'POST'
      let url = `http://localhost:8080/api/learning/train/`

      let auth = `Basic ${self.secret}`
      let body = {}

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
          case 418:
            resolve(result)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  predict(value) {
    console.log(`User '${this.username}' is trying to predict object model of given photo`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'POST'
      let url = `http://localhost:8080/api/learning/predict/`

      let auth = `Basic ${self.secret}`
      let body = {
        source: value
      }

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.setRequestHeader('Content-Type', 'application/json')
      request.send(JSON.stringify(body))

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let predictions = result.body
            resolve(predictions)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  getLearningStatus() {
    console.log(`User '${this.username}' is trying to check intelligent system learning process status`)
    let self = this

    return new Promise((resolve, reject) => {
      let error = {
        code: 500,
        message: 'Internal server error'
      }

      let request = new XMLHttpRequest()
      let method = 'GET'
      let url = `http://localhost:8080/api/learning/train/`

      let auth = `Basic ${self.secret}`

      request.open(method, url, true)
      request.setRequestHeader('Authorization', auth)
      request.send()

      request.onload = () => {
        let result = JSON.parse(request.responseText)

        switch (request.status) {
          case 200:
            let trainStatus = result.body
            let isTraining = trainStatus.status
            resolve(isTraining)
            break

          default:
            reject(result)
            break
        }
      }

      request.onerror = () => {
        reject(error)
      }

      request.onabort = () => {
        reject(error)
      }
    })
  }

  // ----- CACHE

  setAuth(data) {
    let temp = JSON.stringify(data)

    // Save to session storage
    localStorage.setItem('tears-service-auth', temp)
  }

  getAuth() {
    let temp = localStorage.getItem('tears-service-auth')

    // Load from session storage
    return JSON.parse(temp)
  }

  clearAuth() {
    localStorage.removeItem('tears-service-auth')
  }

}
