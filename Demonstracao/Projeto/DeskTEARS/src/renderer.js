// ----- IMPORTS

const { app, BrowserWindow } = require('electron')
const path = require("path")

// ----- DEFINE RENDERER

// Variables

/*
  Keep a global reference of the window object, if you don't, the window will
  be closed automatically when the JavaScript object is garbage collected.
*/
let WINDOW
const HTML = path.resolve(__dirname, "../dist/desktop.html")
const PROJECT = path.resolve(__dirname, "../")

// Constructor
initialize()

// ----- EVENT HANDLERS
// ...

// ----- METHODS
// ...

// ----- BASIC

function initialize() {
  /*
    Auto reload page on target changes
  */
  // require('electron-reload')(PROJECT)

  /*
    Inject REACT DevTools
  */
  // DEBUG - Must PASTE "require('electron-react-devtools').install()" on BrowserWindow console after launch!

  /*
    This method will be called when Electron has finished
    initialization and is ready to create browser windows.
    Some APIs can only be used after this event occurs.
  */
  app.on('ready', createWindow)

  /*
    Quit when all windows are closed.
  */
  app.on('window-all-closed', () => {
    /*
      On macOS it is common for applications and their menu bar
      to stay active until the user quits explicitly with Cmd + Q
    */
    if (process.platform !== 'darwin') {
      app.quit()
    }
  })

  app.on('activate', () => {
    /*
      On macOS it's common to re-create a window in the app when the
      dock icon is clicked and there are no other windows open.
    */
    if (WINDOW === null) {
      createWindow()
    }
  })
}

function createWindow () {
  /*
    Create the browser window.
  */
  WINDOW = new BrowserWindow({width: 1280, height: 770}) // {width: 1280, height: 720} - 720P

  /*
    and load the index.html of the app.
  */
  WINDOW.loadFile(HTML)

  /*
    Open the DevTools.
  */
  WINDOW.webContents.openDevTools()

  /*
    Emitted when the window is closed.
  */
  WINDOW.on('closed', () => {
    /*
      Dereference the window object, usually you would store windows
      in an array if your app supports multi windows, this is the time
      when you should delete the corresponding element.
    */
    WINDOW = null
  })
}

/*
  In this file you can include the rest of your app's specific main process
  code. You can also put them in separate files and require them here.
*/
