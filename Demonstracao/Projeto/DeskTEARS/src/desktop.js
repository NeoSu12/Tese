// ----- IMPORTS

import React from 'react'
import ReactDOM from 'react-dom'

// Stylesheets
import { CssBaseline } from '@material-ui/core'
import { MuiThemeProvider, createMuiTheme, withStyles } from '@material-ui/core/styles'

import blue from '@material-ui/core/colors/blue'
import orange from '@material-ui/core/colors/orange'

// ----- CUSTOM

// Custom pages
import Login from './pages/login'
import Application from './pages/application'

// Custom components
import CustomParticles from './components/customParticles'
import CustomNotification from './components/customNotification'

// Custom classes
import TearsService from './communications/tearsService'

// ----- GLOBAL VARIABLES
// ...

// ----- THEME

const theme = createMuiTheme({
  palette: {
    primary: blue,
    secondary: orange
  }
})

// ----- DEFINE DESKTOP

export default class CustomDesktop extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Validate services
    await this.validation()
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ...
  }

  // ----- EVENT HANDLERS

  handleLogin() {
    const data = this.tearsService.getAuth()

    // Warning notification
    this.handleNotification('Authentication', `User '${data.username}' just logged in the platform`, 'success')

    // Update scene state
    this.setState({
      scene: 'application'
    })
  }

  handleLogout() {
    // Warning notification
    this.handleNotification('Authentication', `User just logged out from the platform`, 'success')

    // Update scene state
    this.setState({
      scene: 'login'
    })
  }

  handleNotification(title, message, level, hideDuration) {
    // Add new notification
    this.notification.addNotification(title, message, level, hideDuration)
  }

  // ----- METHODS

  setApplication() {
    const data = this.tearsService.getAuth()

    // Warning notification
    this.handleNotification('Authentication', `We found that user '${data.username}' already logged in`, 'warning')

    // Update scene state
    this.setState({
      scene: 'application'
    })
  }

  setLogin() {
    // Warning notification
    this.handleNotification('Authentication', `No user found, please authenticate yourself`, 'warning')

    // Update scene state
    this.setState({
      scene: 'login'
    })
  }

  // ----- COMMUNICATIONS

  async validation() {
    try {
      // Validate tears service
      const logged = await this.tearsService.validation()

      // User already logged in
      this.setApplication()
    } catch(error) {
      // User needs to login
      this.setLogin()
    }
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    // Authentication
    this.handleLogin = this.handleLogin.bind(this)
    this.handleLogout = this.handleLogout.bind(this)

    // Notification
    this.handleNotification = this.handleNotification.bind(this)

    // ----- STATE

    this.state = {
      scene: ''
    }

    // ----- REFERENCES

    // Service
    this.tearsService = new TearsService()

    // Scenes
    this.scenes = {
      'login': <Login service={this.tearsService} notification={this.handleNotification} onLogin={this.handleLogin}/>,
      'application': <Application service={this.tearsService} notification={this.handleNotification} onLogout={this.handleLogout}/>
    }

    // Notifications
    this.notification = React.createRef()

  }

  render() {
    const sceneName = this.state.scene
    const sceneValue = this.scenes[sceneName]

    return (
      <div>
        <CssBaseline />
        <MuiThemeProvider theme={theme}>
          {/* PARTICLES */}
          <CustomParticles/>

          {/* SCENE */}
          {sceneValue}

          {/* NOTIFICATIONS */}
          <CustomNotification onRef={ref => this.notification = ref}/>

          {/* CHILDREN */}
          {this.props.children}
        </MuiThemeProvider>
      </div>
    )
  }
}

// ----- REGISTER COMPONENT

ReactDOM.render(<CustomDesktop />, document.getElementById('app'))
