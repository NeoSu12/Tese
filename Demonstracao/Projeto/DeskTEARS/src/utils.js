// ----- SYSTEM

async function sleep(time) {
  const realTime = Math.abs(time) || 0

  return new Promise((resolve, reject) => {
    setInterval(() => {
      resolve()
    }, realTime)
  })
}

function getRandomRange(min, max) {
    return Math.floor(Math.random() * (max - min) + min)
}

// ----- FILE

async function fileToBase64(file) {
	return new Promise((resolve, reject) => {
		if(file) {
			const reader = new FileReader()

			reader.onload = () => {
	      const base64Header = /data:(.*?),/g
	      const result = reader.result
	      const filter = result ? result.replace(base64Header, '') : ''

				resolve(filter)
			}

			reader.readAsDataURL(file)
		}
		else
			reject()
	})
}

// ----- COLOR

function randomColor() {
  return '#' + Math.floor(Math.random() * 16777215).toString(16)
}

export {
  sleep,
  getRandomRange,

  fileToBase64,

  randomColor
}
