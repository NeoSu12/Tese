// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Icons
import ExpandMoreIcon from '@material-ui/icons/ExpandMore'

// Frontend components
import Typography from '@material-ui/core/Typography'

import Grid from '@material-ui/core/Grid'

import ExpansionPanel from '@material-ui/core/ExpansionPanel'
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary'
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  container: {
    position: 'absolute',
    top: 88,
    bottom: 24,
    right: 24,
    left: 104
  },

  table: {

  },

  header: {
    height: 64,
    padding: 24,
    marginBottom: 24,

    backgroundColor: 'white',

    border: '1px solid #FF9800',
    borderRadius: 4
  },
  headerCell: {

  },
  headerText: {

  },

  body: {
    paddingRight: 24,
    maxHeight: 'calc(100vh - 198px)',

    overflowX: 'hidden',
    overflowY: 'auto'
  },

  row: {

  },
  rowHeader: {

  },
  rowCell: {

  },
  rowDetail: {

  },
  rowText: {

  }

})

// ----- DEFINE TABLE

class CustomTable extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Fetch available objects
    await this.fetchObjects()
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({

    })
  }

  // ----- EVENT HANDLERS

  // Select
  handleSelect(key, event, expanded) {
    this.setState({
      selected: expanded ? key : '',
    })
  }

  // Unselect
  handleUnselect(event) {
    this.setState({
      selected: ''
    })
  }

  // ----- METHODS
  // ...

  // ----- COMMUNICATIONS

  async fetchObjects() {
    try {
      // Fetch available objects
      let availableObjects = await this.state.service.getObjects()
      let formattedObjects = availableObjects.reduce((result, value) => {
          result[value.id] = value
          return result
      }, {})

      this.setState({
        values: formattedObjects
      })

      // Notification
      this.state.notification('Models', `Fetched available platform objects`, 'success')
    } catch(error) {
      this.setState({
        values: ''
      })

      // Notification
      this.state.notification('Models', `Could not retrieve available platform objects: ${error.message}`, 'error')
    }
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleSelect = this.handleSelect.bind(this)
    this.handleUnselect = this.handleUnselect.bind(this)

    // ----- STATE

    this.state = {
      notification: this.props.notification,
      service: this.props.service,

      values: '',
      page: 1,
      itemsPerPage: 10,

      selected: '',

      add: false,
      addValue: '',
      addInvalid: [],

      remove: ''
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props

    const currentValues = this.state.values
    const currentKeys = Object.keys(currentValues)
    const currentSize = currentKeys.length
    const objectsExist = currentSize > 0

    // Format list
    let content = <Grid container direction='row' justify='center' alignItems='center' className={classes.row}>
      <Grid key={-1} item xs={12} className={classes.rowCell}>
        <Typography className={classes.rowText} variant="headline">
          WARNING - Currently there are no available values to list
        </Typography>
      </Grid>
    </Grid>

    if(objectsExist) {
      content = currentKeys.map(key => {
          const value = currentValues[key]

          const id = value.id
          const label = value.label
          const isSelected = this.state.selected === id

          return <ExpansionPanel expanded={isSelected} className={classes.row} onChange={this.handleSelect.bind(this, id)}>
            <ExpansionPanelSummary className={classes.rowHeader} expandIcon={<ExpandMoreIcon />}>
              <Grid container direction='row' justify='center' alignItems='center' className={classes.rowHeader}>
                <Grid key={0} item xs={4} className={classes.rowCell}>
                  {/* THUMBNAIL */}
                  {/*}<img src={BackgroundImage} className={ClassNames(classes.thumbnail, classes.unselectable)} />*/}
                </Grid>
                <Grid key={1} item xs={4} className={classes.rowCell}>
                  {/* ID */}
                  <Typography className={classes.rowText}>
                    {id}
                  </Typography>
                </Grid>
                <Grid key={2} item xs={4} className={classes.rowCell}>
                  {/* LABEL */}
                  <Typography className={classes.rowText}>
                    {label}
                  </Typography>
                </Grid>
              </Grid>
            </ExpansionPanelSummary>

            <ExpansionPanelDetails className={classes.rowDetail}>
              <Typography>
                  Detailed information here!
              </Typography>
            </ExpansionPanelDetails>
          </ExpansionPanel>
        }
      )
    }

    return (
      <div className={classes.container}>
        {/* HEADER */}
        <Grid container direction='row' justify='center' alignItems='center' className={classes.header}>
          <Grid key={0} item xs={4} className={classes.headerCell}>
            {/* THUMBNAIL */}
            <Typography color='secondary' className={classes.headText}>
              THUMBNAIL
            </Typography>
          </Grid>
          <Grid key={1} item xs={4} className={classes.headerCell}>
            {/* ID */}
            <Typography color='secondary' className={classes.headText}>
              ID
            </Typography>
          </Grid>
          <Grid key={2} item xs={4} className={classes.headerCell}>
            {/* LABEL */}
            <Typography color='secondary' className={classes.headText}>
              LABEL
            </Typography>
          </Grid>
        </Grid>

        {/* BODY */}
        <div className={classes.body}>
          {content}
        </div>

        {/* PAGINATION */}

        {this.props.children}
      </div>
    )
  }
}

// ----- EXPORT

CustomTable.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomTable)
