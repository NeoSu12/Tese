// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'
import { Spring, config } from 'react-spring'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  container: {
    display: 'block',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    height: '100%',
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    willChange: 'background'
  },

  view: {
    width: '100%',
    height: '100%'
  },

  shape: {
    cursor: 'pointer'
  }
})

// ----- DEFINE ANIMATOR

class CustomAnimator extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ...
  }

  // ----- EVENT HANDLERS

  handleToggle(event) {
    const currentToggle = this.state.toggle

    this.setState({
      toggle: !currentToggle
    })
  }

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleToggle = this.handleToggle.bind(this)

    // ----- STATE

    this.state = {
      toggle: true
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props

    const toggle = this.state.toggle
    const options = {
      color: toggle ? '#2196F3' : '#FF9800',
      start: toggle ? '#90977A' : '#90977A',
      end: toggle ? '#2196F3' : '#FF9800',
      scale: toggle ? 1 : 20
    }

    return (
      <Spring config={config.default} to={options}>
        {
          ({ color, start, end, scale }) => (
            <div className={classes.container}>
              <svg
                className={classes.view}
                version="1.1">
                <circle className={classes.shape} cx={`${10 + (2 * scale)}%`} cy={`${90 - (2 * scale)}%`} r={25} fill={color} onClick={this.handleToggle}/>
              </svg>
            </div>
          )
        }
      </Spring>
    )
  }
}

// ----- EXPORT

CustomAnimator.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomAnimator)
