// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Icons
import AddPhoto from '@material-ui/icons/AddAPhoto'

// Frontend components
import Typography from '@material-ui/core/Typography'

import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'
import GridListTileBar from '@material-ui/core/GridListTileBar'

import CircularProgress from '@material-ui/core/CircularProgress'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  container: {
    position: 'relative',
    height: '100%',
    width: '100%'
  },

  emptyContainer: {
    textTransform: 'uppercase',
    color: 'lightgray'
  },

  progressContainer: {

  },

  progressIcon: {
    color: '#2196F3',
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -64,
    marginLeft: -64
  },

  grid: {
    marginTop: 24,
    height: 'inherit',
    justifyContent: 'space-around',
    alignItems: 'center'
  },

  cell: {
    margin: 12,

    border: '1px solid transparent',
    borderRadius: 4,

    cursor: 'Pointer',
    transition: '0.1s',

    '&:hover': {
      backgroundColor: 'rgba(33, 150, 243, 0.6)',
      transform: 'scale(1.05)'
    },
    '&:active, &:focus': {
      backgroundColor: 'rgba(0, 0, 0, 0.15)',
      transform: 'scale(1)'
    }
  },

  cellEmpty: {
    backgroundColor: 'rgba(0, 0, 0, 0.15)'
  },

  image: {

  },

  missing: {
    display: 'flex',
    height: 'calc(100% - 68px)',
    alignItems: 'center',
    justifyContent: 'center'
  },

  missingIcon: {
    color: 'rgba(255, 255, 255, 0.54)',
    fontSize: 72
  },

  bar: {
    background: 'rgba(0, 0, 0, 0.3)'
  },

  subtitle: {
    fontSize: 10
  }
})

// ----- DEFINE GRID

class CustomGrid extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({
      loading: nextProps.loading,
      values: nextProps.values || {},
      onSelect: nextProps.onSelect
    })
  }

  // ----- EVENT HANDLERS

  handleSelect(id, event) {
    const currentSelectEvent = this.state.onSelect

    // Custom select event
    if(currentSelectEvent)
      currentSelectEvent(id, event)
  }

  // ----- METHODS
  // ...

  // ----- COMMUNICATIONS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleSelect = this.handleSelect.bind(this)

    // ----- STATE

    this.state = {
      loading: this.props.loading,
      values: this.props.values || {},
      onSelect: this.props.onSelect
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props

    const currentLoading = this.state.loading
    const currentValues = this.state.values
    const currentKeys = Object.keys(currentValues)
    const currentSize = currentKeys.length
    const objectsExist = currentSize > 0

    const fixSize = {
      width: 'unset',
      height: 'unset'
    }

    let content = currentLoading ? <div className={classes.progressContainer}>
      <CircularProgress size={128} thickness={2} className={classes.progressIcon} />
    </div> :
    <Typography variant='title' className={classes.emptyContainer} style={fixSize}>
      No objects available to list at the moment
    </Typography>

    // Format list
    if(objectsExist) {
      content = currentKeys.map(key => {
        const value = currentValues[key]

        const id = value.id
        const label = value.label
        const photo = value.photo
        const thumbnail = photo ? 'data:image/png;base64,' + photo.thumbnail : null
        const isValid = Boolean(thumbnail)

        const fixWidth = {
          width: 200
        }

        const backgroundColor = !isValid ? classes.cellEmpty : null
        const content = isValid ? <img src={thumbnail} className={classes.image} /> :
        <div className={classes.missing}>
          <AddPhoto fontSize={'inherit'} className={classes.missingIcon}/>
        </div>

        return <GridListTile key={id} style={fixWidth} className={ClassNames(classes.cell, backgroundColor)} onClick={this.handleSelect.bind(this, id)}>
            {content}
            <GridListTileBar
              classes={{
                titlePositionBottom: classes.bar
              }}
              title={label}
              subtitle={<span className={classes.subtitle}>{id}</span>}
            />
          </GridListTile>
        }
      )
    }

    return (
      <div className={classes.container}>
        <GridList cellHeight={200} cols={5} spacing={0} className={classes.grid}>
          {content}
        </GridList>

        {this.props.children}
      </div>
    )
  }
}

// ----- EXPORT

CustomGrid.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomGrid)
