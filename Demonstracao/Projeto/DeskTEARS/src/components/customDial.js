// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Icons
import EditIcon from '@material-ui/icons/Edit'

// Frontend components
import SpeedDial from '@material-ui/lab/SpeedDial'
import SpeedDialIcon from '@material-ui/lab/SpeedDialIcon'
import SpeedDialAction from '@material-ui/lab/SpeedDialAction'

// ----- CUSTOM

// Custom components
// ...

// Custom assets
// ...

// Custom utils
// ...

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  dial: {
    position: 'fixed',

    bottom: 24,
    left: 24
  },

  button: {
    boxShadow: 'none',
    backgroundColor: '#FF9800',

    '&:hover': {
      backgroundColor: '#FF9800'
    }
  },

  action: {
    width: 50,
    height: 50,
    margin: '10px 3px',

    boxShadow: 'none',
    border: '1px solid #2196F3',

    backgroundColor: 'white',
    color: '#2196F3',

    '&:hover': {
      backgroundColor: '#2196F3',
      color: 'white',
    }
  }
})

// ----- DEFINE DIAL

class CustomDial extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({

    })
  }

  // ----- EVENT HANDLERS

  handleClick(event) {
    this.setState(prevState => ({
      open: !prevState.open,
    }))
  }

  handleOpen(event) {
    this.setState({
      open: true,
    })
  }

  handleClose(event) {
    this.setState({
      open: false
    })
  }

  handleAction(action, event) {
    const currentActionEvent = this.state.onAction

    // Custom onAction event
    if(currentActionEvent)
      currentActionEvent(action, event)

    // Close dial
    this.handleClose()
  }

  // ----- METHODS
  // ...

  // ----- COMMUNICATIONS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS
    this.handleClick = this.handleClick.bind(this)
    this.handleOpen = this.handleOpen.bind(this)
    this.handleClose = this.handleClose.bind(this)

    this.handleAction = this.handleAction.bind(this)

    // ----- STATE

    this.state = {
      open: false,
      actions: this.props.actions || [],
      onAction: this.props.onAction
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props
    const currentOpen = this.state.open
    const currentActions = this.state.actions

    return (
      <div>
        <SpeedDial
          ariaLabel='SpeedDial Button'
          classes={{
            root: classes.dial,
            fab: classes.button
          }}
          className={classes.dial}
          icon={<SpeedDialIcon openIcon={<EditIcon />}/>}
          onClick={this.handleClick}
          onClose={this.handleClose}

          onFocus={this.handleOpen}
          onBlur={this.handleClose}

          onMouseEnter={this.handleOpen}
          onMouseLeave={this.handleClose}
          open={currentOpen}
        >
          {currentActions.map(action => (
            <SpeedDialAction
              classes={{
                button: ClassNames(classes.action, action.classe)
              }}
              key={action.name}
              icon={action.icon}
              tooltipTitle={action.name}
              tooltipOpen
              onClick={this.handleAction.bind(this, action.name)}
            />
          ))}
        </SpeedDial>

        {this.props.children}
      </div>
    )
  }
}

// ----- EXPORT

CustomDial.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomDial)
