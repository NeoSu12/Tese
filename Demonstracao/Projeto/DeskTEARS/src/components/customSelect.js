// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'

// Utils
import uniqid from 'uniqid'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Frontend components
import FormControl from '@material-ui/core/FormControl'

import Select from '@material-ui/core/Select'
import InputLabel from '@material-ui/core/InputLabel'
import MenuItem from '@material-ui/core/MenuItem'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  form: {
    width: '100%',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    textAlign: 'center'
  },

  input: {
    paddingBottom: 6,
    '&:hover:before': {
      borderColor: 'gray !important',
      borderWidth: '1px !important'
    },

    '&:after': {
      borderBottom: '2px solid #FF9800'
    }
  },

  select: {
    paddingLeft: 6
  }
})

// ----- DEFINE SELECT

class CustomSelect extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({
      label: nextProps.label || '',

      value: nextProps.value,
      disabled: nextProps.disabled,

      required: nextProps.required,
      onChange: nextProps.onChange
    })
  }

  // ----- EVENT HANDLERS

  handleChangeValue(event) {
    // Value update
    const updateValue = event.target.value

    this.setState({
       value: updateValue
    })

    // Custom change value event
    const currentOnChange = this.state.onChange
    if(currentOnChange)
      currentOnChange(updateValue, event)
  }

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleChangeValue = this.handleChangeValue.bind(this)

    // ----- STATE

    this.state = {
      id: this.props.id || uniqid(),
      label: this.props.label || '',
      value: this.props.value || '',

      disabled: false,

      required: this.props.required,
      onChange: this.props.onChange
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props

    const inputId = this.state.id
    const inputLabel = this.state.label
    const inputValue = this.state.value

    const isDisabled = this.state.disabled
    const isRequired = this.state.required

    return (
      <div>
        <FormControl className={classes.form} disabled={isDisabled} required={isRequired}>
          <InputLabel htmlFor={inputId}>
            {inputLabel}
          </InputLabel>

          <Select
            className={classes.input}
            classes={{
              select: classes.select
            }}
            value={inputValue}
            onChange={this.handleChangeValue}
          >
            <MenuItem value={5}>5</MenuItem>
            <MenuItem value={10}>10</MenuItem>
            <MenuItem value={20}>20</MenuItem>
            <MenuItem value={50}>50</MenuItem>
          </Select>
        </FormControl>

        {this.props.children}
      </div>
    )
  }
}

// ----- EXPORT

CustomSelect.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomSelect)
