// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Icons
import AddIcon from '@material-ui/icons/Add'
import VisibleIcon from '@material-ui/icons/Visibility'
import NotVisibleIcon from '@material-ui/icons/VisibilityOff'

import ClearIcon from '@material-ui/icons/Clear'
import SaveIcon from '@material-ui/icons/Save'

// Frontend components
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'

import Dialog from '@material-ui/core/Dialog'

import Grid from '@material-ui/core/Grid'
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'

import CircularProgress from '@material-ui/core/CircularProgress'

// Konva
import { Stage, Layer, Image, Rect, Circle } from 'react-konva'

// ----- CUSTOM

// Custom components
// ...

// Custom assets
// ...

// Custom utils
import { sleep, getRandomRange, randomColor } from '../utils'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  dialog: {
    backgroundColor: 'transparent'
  },
  container: {

  },
  close: {
    position: 'fixed',
    top: 8,
    right: 24,

    color: 'white'
  },
  add: {
    position: 'fixed',
    bottom: 24,
    left: 24,

    width: 56,
    height: 56,

    border: '1px solid white',
    borderRadius: '50%',

    backgroundColor: 'transparent',
    color: 'white',

    '&:hover': {
      backgroundColor: 'white',
      color: 'rgba(0, 0, 0, 0.54)'
    }
  },
  grid: {
    marginTop: 64,

    paddingTop: 24,
    paddingLeft: 96,
    paddingRight: 12
  },
  column: {
    paddingLeft: 12,
    paddingRight: 12
  },

  progressContainer: {
    display: 'flex',

    width: '100%',
    height: '100%',

    alignItems: 'center',
    justifyContent: 'center'
  },
  progressIcon: {
    color: 'white'
  },
  progressRemove: {
    color: '#FF1744'
  },
  progressSave: {
    color: '#2196F3'
  },

  titleContainer: {
    height: 72,

    marginBottom: 24,
    padding: 24,

    border: '1px solid white',
    borderLeftWidth: 5,
    borderRadius: 4
  },
  titleValue: {
    color: 'white'
  },


  data: {
    height: 'calc(100vh - 208px)',
    position: 'relative',

    backgroundColor: 'transparent',

    // border: '1px solid white',
    borderRadius: 4,
    boxShadow: 'none',

    overflowX: 'hidden',
    overflowY: 'auto'
  },
  list: {

  },
  listItem: {
    height: 72,

    marginBottom: 12,
    border: 'none',
    borderLeft: '10px solid white',

    cursor: 'pointer',
    transition: '0.1s',
    '&:hover': {
      backgroundColor: 'rgba(255, 255, 255, 0.1)',
      border: '1px solid white'
    }
  },
  listItemSelected: {
    backgroundColor: 'rgba(255, 255, 255, 0.2)'
  },
  saveItem: {
    height: 72,

    marginBottom: 12,
    border: '1px dashed #2196F3'
  },
  removeItem: {
    height: 72,

    marginBottom: 12,
    border: '1px dashed #FF1744'
  },
  itemColumn: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center'
  },
  itemColumnLeft: {
    justifyContent: 'left'
  },
  itemIcon: {
    marginLeft: 24
  },
  visibleIcon: {
    color: 'rgba(0, 0, 0, 0.54)',
    backgroundColor: 'white'
  },
  notVisibleIcon: {
    color: 'white',
    backgroundColor: 'none',
    border: '1px solid white'
  },
  itemSave: {
    color: 'white'
  },
  itemRemove: {
    color: 'white'
  },
  stage: {
    display: 'flex',
    width: '100%',
    height: '100%',

    alignItems: 'center',
    justifyContent: 'center'
  },
  stageContent: {

  },
  layer: {

  }
})

// ----- DEFINE GALLERY SELECT

class CustomGallerySelect extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Update reference
    this.updateReference(this)

    // update selected photo information
    await this.updateInformation()
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE
    this.setState({
      notification: nextProps.notification,
      service: nextProps.service,

      selectedPhoto: nextProps.selected
    },
    async () => {
      // Update selected photo information
      await this.updateInformation()
    })
  }

  componentDidUpdate(prevProps, prevState) {
    // Adapt stage to image size
    this.fitStageIntoParentContainer()
  }

  // ----- EVENT HANDLERS

  async handleAdd(event) {
    // Add new random bounding box
    await this.addAnnotation()
  }

  handleSelect(key, event) {
    const currentSelectedAnnotation = this.state.selectedAnnotation

    this.setState({
      selectedAnnotation: key
    })
  }

  handleUnselect(event) {
    this.setState({
      selectedAnnotation: ''
    })
  }

  handleToggleValue(key, event) {
    // Dont let event propagate
    event.stopPropagation()

    let currentAnnotations = this.state.annotations

    // Update visible value
    currentAnnotations[key].visible = !currentAnnotations[key].visible

    this.setState({
      annotations: currentAnnotations
    })
  }

  async handleSave(key, event) {
    await this.updateAnnotation(key)
  }

  async handleRemove(key, event) {
    const currentRemove = this.state.remove

    if(!currentRemove) {
      // Check if user wants to cancel changes or remove
      const currentOldAnnotations = this.state.oldAnnotations
      const currentAnnotations = this.state.annotations
      let currentAnnotation = currentAnnotations[key]
      let changed = currentAnnotation.changed

      if(changed) {
        // Revert changes
        currentAnnotations[key] = JSON.parse(JSON.stringify(currentOldAnnotations[key]))

        this.setState({
          annotations: currentAnnotations
        })
      } else {
        // Remove value
        await this.removeAnnotation(key)
      }
    }
    else {
      // Notification
      this.state.notification('Photo', `Server is still handling annotation '${currentRemove}' removal, please stand by for a few seconds`, 'warning')
    }
  }

  handleClose(event) {
    const currentCloseEvent = this.state.onClose

    // Custom close event
    if(currentCloseEvent)
      currentCloseEvent(event)
  }

  handleDragBound(position, event) {
    const stageReference = this.imageStage.current
    const stage = stageReference.getStage()
    const scaleX = stage.scaleX()
    const scaleY = stage.scaleY()
    const shiftX = stage.x()
    const shiftY = stage.y()

    const currentPhoto = this.state.photo
    const currentWidth = currentPhoto.width * scaleX
    const currentHeight = currentPhoto.height * scaleY

    const currentOffset = this.state.offSet
    const offsetWidth = shiftX + currentOffset.x * scaleX
    const offsetHeight = shiftY + currentOffset.y * scaleY

    const currentX = position.x
    const currentY = position.y

    let realX = 0
    let realY = 0

    // X position
    if(currentX > offsetWidth) {
      if(currentX < offsetWidth + currentWidth) {
        realX = currentX
      }
      else {
        realX = offsetWidth + currentWidth
      }
    }
    else {
      realX = offsetWidth
    }

    // Y position
    if(currentY > offsetHeight) {
      if(currentY < offsetHeight + currentHeight) {
        realY = currentY
      }
      else {
        realY = offsetHeight + currentHeight
      }
    }
    else {
      realY = offsetHeight
    }

    return {
      x: realX,
      y: realY
    }
  }

  handleDragMove(key, point, event) {
    const currentPhoto = this.state.photo
    const currentWidth = currentPhoto.width
    const currentHeight = currentPhoto.height

    const currentOffset = this.state.offSet
    const offsetWidth = currentOffset.x
    const offsetHeight = currentOffset.y

    const currentTarget = event.target
    const currentX = currentTarget.x()
    const currentY = currentTarget.y()

    const realX = currentX - offsetWidth
    const realY = currentY - offsetHeight

    const percentageX = realX != 0 ? realX / currentWidth : 0
    const percentageY = realY != 0 ? realY / currentHeight : 0

    // Update annotation value
    const pointX = `${point}X`
    const pointY = `${point}Y`

    let currentAnnotations = this.state.annotations
    currentAnnotations[key][pointX] = percentageX
    currentAnnotations[key][pointY] = percentageY
    currentAnnotations[key].changed = true

    this.setState({
      annotations: currentAnnotations
    })
  }

  // ----- METHODS

  updateReference(value) {
    const currentReferenceEvent = this.state.ref

    // Custom reference event
    if(currentReferenceEvent)
      currentReferenceEvent(value)
  }

  updateInformation() {
    const currentOldSelectedPhoto = this.state.oldSelectedPhoto
    const currentSelectedPhoto = this.state.selectedPhoto

    if(currentOldSelectedPhoto != currentSelectedPhoto) {
      this.setState({
        oldSelectedPhoto: currentSelectedPhoto
      })

      if(currentSelectedPhoto) {
        // Fetch selected photo value
        this.fetchPhoto()

        // Fetch selected photo annotations
        this.fetchPhotoAnnotations(false)
      }
    }
  }

  async showLoadingPhoto() {
    this.setState({
      loadingPhoto: true
    })

    // Sleep for 1 second
    await sleep(1000)
  }

  hideLoadingPhoto() {
    this.setState({
      loadingPhoto: false
    })
  }

  async showLoadingAnnotations() {
    this.setState({
      loadingAnnotations: true
    })

    // Sleep for 1 second
    await sleep(1000)
  }

  hideLoadingAnnotations() {
    this.setState({
      loadingAnnotations: false
    })
  }

  async showRemoveAnnotation(key) {
    this.setState({
      remove: key
    })

    // Sleep for 1 second
    await sleep(1000)
  }

  hideRemoveAnnotation() {
    this.setState({
      remove: ''
    })
  }

  async showSaveAnnotation(key) {
    this.setState({
      save: key
    })

    // Sleep for 1 second
    await sleep(1000)
  }

  hideSaveAnnotation() {
    this.setState({
      save: ''
    })
  }

  // ----- COMMUNICATIONS

  async fetchPhoto() {
    const currentSelectedPhoto = this.state.selectedPhoto

    // Show loading file
    await this.showLoadingPhoto()

    try {
      const availablePhoto = await this.state.service.getPhoto(currentSelectedPhoto)

      // Update image value
      let image = new window.Image()

      image.id = availablePhoto.id
      image.src = 'data:image/png;base64,' + availablePhoto.original
      image.onload = () => {
        // Calculate image ratio
        const imageWidth = image.width
        const imageHeight = image.height

        const ratio = imageWidth / imageHeight

        // Change image width and height
        if(imageWidth > imageHeight) {
          image.width = 800
          image.height = 800 / ratio
        }
        else {
          image.width = 450 * ratio
          image.height = 450
        }

        // Calculate offset
        const currentWidth = image.width
        const currentHeight = image.height

        const offsetWidth = currentWidth <= 800 ? (848 - currentWidth) * 0.5 : 0
        const offsetHeight = currentHeight <= 450 ? (498 - currentHeight) * 0.5 : 0

        const offSet = {
          x: offsetWidth,
          y: offsetHeight
        }

        // Update values
        this.setState({
          photo: image,
          offSet
        })
      }
    } catch(error) {
      this.setState({
        photo: ''
      })

      // Notification
      this.state.notification('Photo', `Could not retrieve photo '${currentSelectedPhoto}' original: ${error.message}`, 'error')
    }

    // Hide loading photo
    this.hideLoadingPhoto()
  }

  async fetchPhotoAnnotations(keepChanges) {
    const currentSelectedPhoto = this.state.selectedPhoto

    // Show loading annotations
    await this.showLoadingAnnotations()

    try {
      const filter = {
          limit: 100
      }

      const availableAnnotations = await this.state.service.getPhotoAnnotations(currentSelectedPhoto, filter)
      const formattedAnnotations = availableAnnotations.reduce((result, value) => {
          // Add a random color for each value
          value.color = randomColor()

          // Add changed control
          value.changed = false

          // Add visibility control
          value.visible = false

          result[value.id] = value
          return result
      }, {})

      let annotations = JSON.parse(JSON.stringify(formattedAnnotations))
      let oldAnnotations = JSON.parse(JSON.stringify(formattedAnnotations))

      // Check if user wants to keep changes
      if(keepChanges) {
        const currentAnnotations = this.state.annotations
        const currentOldAnnotations = this.state.oldAnnotations

        if(currentAnnotations) {
          for (var key in currentAnnotations) {
              if (currentAnnotations.hasOwnProperty(key) && annotations.hasOwnProperty(key) && oldAnnotations.hasOwnProperty(key)) {
                   annotations[key] = JSON.parse(JSON.stringify(currentAnnotations[key]))
                   oldAnnotations[key] = JSON.parse(JSON.stringify(currentOldAnnotations[key]))
              }
          }
        }
      }

      // Update values
      this.setState({
        annotations,
        oldAnnotations
      })

    } catch(error) {
      this.setState({
        annotations: '',
        oldAnnotations: ''
      })

      // Notification
      this.state.notification('Photo', `Could not retrieve photo '${currentSelectedPhoto}' annotations: ${error.message}`, 'error')
    }

    // Hide loading annotations
    this.hideLoadingAnnotations()
  }

  async addAnnotation() {
    const currentSelectedPhoto = this.state.selectedPhoto

    try {
      const tempValue = {
        'topLeftX': getRandomRange(1, 50) / 100,
        'topLeftY': getRandomRange(1, 50) / 100,
        'bottomRightX': getRandomRange(50, 100) / 100,
        'bottomRightY': getRandomRange(50, 100) / 100
      }

      // Validate bounding points
      let value = Object.assign({}, tempValue)
      const sumTopLeft = value.topLeftX + value.topLeftY
      const sumBottomRight = value.bottomRightX + value.bottomRightY

      if(sumTopLeft > sumBottomRight) {
        // TopLeft is the real BottomRight
        value.topLeftX = tempValue.bottomRightX
        value.topLeftY = tempValue.bottomRightY

        value.bottomRightX = tempValue.topLeftX
        value.bottomRightY = tempValue.topLeftY
      }

      // Add new random annotation
      const availableAnnotation = await this.state.service.addPhotoAnnotation(currentSelectedPhoto, value)

      // Select newly added annotation
      const key = availableAnnotation.id
      this.handleSelect(key)

      // Notification
      this.state.notification('Photo', `Added new annotation to photo '${currentSelectedPhoto}'`, 'success')
    } catch(error) {
      this.state.notification('Photo', `Could not add new annotation to photo '${currentSelectedPhoto}': ${error.message}`, 'error')
    }

    // Fetch updated annotations (Keep old values, since user might not saved old changes)
    await this.fetchPhotoAnnotations(true)
  }

  async removeAnnotation(key) {
    const currentSelectedPhoto = this.state.selectedPhoto

    // Show remove annotation
    await this.showRemoveAnnotation(key)

    try {
      let currentOldAnnotations = this.state.oldAnnotations
      let currentAnnotations = this.state.annotations

      // Remove annotation
      await this.state.service.removeAnnotation(key)

      delete currentOldAnnotations[key]
      delete currentAnnotations[key]

      this.setState({
        annotations: currentAnnotations,
        oldAnnotations: currentOldAnnotations
      })

      // Notification
      this.state.notification('Gallery', `Removed annotation '${key}' from photo '${currentSelectedPhoto}'`, 'success')
    } catch(error) {
      this.state.notification('Gallery', `Could not remove annotation '${key}' from photo '${currentSelectedPhoto}': ${error.message}`, 'error')
    }

    // Fetch updated annotations (Keep old values, since user might not saved old changes)
    // await this.fetchPhotoAnnotations(true)

    // Hide remove annotation
    this.hideRemoveAnnotation(key)
  }

  async updateAnnotation(key) {
    const currentSelectedPhoto = this.state.selectedPhoto

    // Show save annotation
    await this.showSaveAnnotation(key)

    try {
      let currentOldAnnotations = this.state.oldAnnotations
      let currentAnnotations = this.state.annotations
      let currentAnnotation = currentAnnotations[key]

      // Update annotation
      let updatedAnnotation = await this.state.service.updateAnnotation(key, currentAnnotation)

      // Add color
      updatedAnnotation.color = currentAnnotation.color

      // Add changed control
      updatedAnnotation.changed = false

      // Add visibility control
      updatedAnnotation.visible = currentAnnotation.visible

      currentOldAnnotations[key] = JSON.parse(JSON.stringify(updatedAnnotation))
      currentAnnotations[key] = JSON.parse(JSON.stringify(updatedAnnotation))

      this.setState({
        annotations: currentAnnotations,
        oldAnnotations: currentOldAnnotations
      })

      // Notification
      this.state.notification('Gallery', `Updated annotation '${key}' of photo '${currentSelectedPhoto}'`, 'success')
    } catch(error) {
      this.state.notification('Gallery', `Could not update annotation '${key}' of photo '${currentSelectedPhoto}': ${error.message}`, 'error')
    }

    // Hide save annotation
    this.hideSaveAnnotation(key)
  }

  // ----- DRAWING

  fitStageIntoParentContainer() {
    const photo = this.state.photo
    const offSet = this.state.offSet

    if(photo) {
      const photoWidth = photo.width
      const photoHeight = photo.height

      const offSetX = offSet.x
      const offSetY = offSet.y

      const desiredWidth = photoWidth + 2 * offSetX
      const desiredHeight = photoHeight + 2 * offSetY

      const container = this.imageContainer.current
      const stageReference = this.imageStage.current

      if(container && stageReference) {
        const stage = stageReference.getStage()

        const containerWidth = container.offsetWidth
        const containerHeight = container.offsetHeight

        const desiredWidthRatio = containerWidth / desiredWidth
        const desiredHeightRatio = containerHeight / desiredHeight
        const chosenRatio = Math.min(desiredWidthRatio, desiredHeightRatio)

        const shiftX = (containerWidth - desiredWidth * chosenRatio) * 0.5
        const shiftY = (containerHeight - desiredHeight * chosenRatio) * 0.5

        // Stage
        stage.width(desiredWidth * desiredWidthRatio)
        stage.height(desiredHeight * desiredHeightRatio)
        stage.x(shiftX)
        stage.y(shiftY)

        stage.scale({
          x: chosenRatio,
          y: chosenRatio
        })
        stage.draw()
      }
    }
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleClose = this.handleClose.bind(this)

    this.handleSelect = this.handleSelect.bind(this)
    this.handleUnselect = this.handleUnselect.bind(this)
    this.handleToggleValue = this.handleToggleValue.bind(this)

    this.handleAdd = this.handleAdd.bind(this)
    this.handleSave = this.handleSave.bind(this)
    this.handleRemove = this.handleRemove.bind(this)

    this.handleDragBound = this.handleDragBound.bind(this)
    this.handleDragMove = this.handleDragMove.bind(this)
    this.fitStageIntoParentContainer = this.fitStageIntoParentContainer.bind(this)

    // ----- STATE

    this.state = {
      ref: this.props.ref,
      notification: this.props.notification,
      service: this.props.service,
      onClose: this.props.onClose,

      oldSelectedPhoto: '',
      selectedPhoto: this.props.selected,
      photo: '',
      offSet: '',
      loadingPhoto: false,

      selectedAnnotation: '',
      annotations: '',
      oldAnnotations: '',
      changed: '',
      remove: '',
      save: '',
      loadingAnnotations: false
    }

    // ----- REFERENCES

    this.imageContainer = React.createRef()
    this.imageStage = React.createRef()

    // ---- FIXES

    // Adapt the stage on any window resize
    window.addEventListener('resize', this.fitStageIntoParentContainer)
  }

  render() {
    const { classes } = this.props

    const currentOpen = Boolean(this.state.selectedPhoto)

    // ----- Photo

    const currentSelectedPhoto = this.state.selectedPhoto
    const currentPhoto = this.state.photo
    const currentOffset = this.state.offSet

    // Format photo

    const photoWidth = currentPhoto ? currentPhoto.width : 0
    const photoHeight = currentPhoto ? currentPhoto.height : 0

    const offsetX = currentOffset ? currentOffset.x : 0
    const offsetY = currentOffset ? currentOffset.y : 0

    // Photo content
    const photoContent = <Image x={offsetX} y={offsetY} image={currentPhoto} width={photoWidth} height={photoHeight} />

    // ----- Annotations

    const currentSelectedAnnotation = this.state.selectedAnnotation
    const currentAnnotations = this.state.annotations
    const oldAnnotations = this.state.oldAnnotations
    const currentKeys = Object.keys(currentAnnotations)
    const currentSize = currentKeys.length

    const currentRemove = this.state.remove
    const currentSave = this.state.save

    // Annotations content
    let annotationsListContent = []
    let annotationsRectanglesContent = []
    let annotationsCirclesContent = []

    if(currentSize > 0) {
      currentKeys.forEach(key => {

        const oldValue = oldAnnotations[key]
        const currentValue = currentAnnotations[key]

        // Values
        const id = currentValue.id

        const color = currentValue.color
        const changed = currentValue.changed
        const visible = currentValue.visible

        const topLeftX = offsetX + currentValue.topLeftX * photoWidth
        const topLeftY = offsetY + currentValue.topLeftY * photoHeight
        const bottomRightX = offsetX + currentValue.bottomRightX * photoWidth
        const bottomRightY = offsetY + currentValue.bottomRightY * photoHeight
        const width = (bottomRightX - topLeftX)
        const height = (bottomRightY - topLeftY)

        const styleVisible = {
          backgroundColor: color
        }

        const styleNotVisible = {
          borderColor: color,
          color: color
        }

        const styleItem = {
          borderColor: color
        }

        // Remove
        const remove = currentRemove === id

        // Save
        const save = currentSave === id

        // Selected
        const selected = currentSelectedAnnotation === id

        // Visible
        const toggleFuction = !selected && !changed ? this.handleToggleValue.bind(this, id) : null
        const visibleIcon = selected || visible || changed ?
        <IconButton
          className={ClassNames(classes.itemIcon, classes.visibleIcon)}
          onClick={toggleFuction}
        >
          <VisibleIcon />
        </IconButton> :
        <IconButton
          className={ClassNames(classes.itemIcon, classes.notVisibleIcon)}
          onClick={toggleFuction}
        >
          <NotVisibleIcon />
        </IconButton>

        // Save
        const saveIcon = changed ?
        <IconButton
          className={classes.itemSave}
          onClick={this.handleSave.bind(this, id)}
        >
          <SaveIcon />
        </IconButton> : ''

        // Delete
        const deleteIcon = selected || changed ?
        <IconButton
          className={classes.itemRemove}
          onClick={this.handleRemove.bind(this, id)}
        >
          <ClearIcon />
        </IconButton> : ''

        // Final item content

        const removeProgress = <div className={classes.progressContainer}>
          <CircularProgress size={50} thickness={2} className={ClassNames(classes.progressIcon, classes.progressRemove)} />
        </div>
        const removeContent = <Grid key={id} container direction='row' className={classes.removeItem}>
          <Grid key={`${id}-removing`} item xs={12} className={classes.itemColumn}>
            {removeProgress}
          </Grid>
        </Grid>

        const saveProgress = <div className={classes.progressContainer}>
          <CircularProgress size={50} thickness={2} className={ClassNames(classes.progressIcon, classes.progressSave)} />
        </div>
        const saveContent = <Grid key={id} container direction='row' className={classes.saveItem}>
          <Grid key={`${id}-removing`} item xs={12} className={classes.itemColumn}>
            {saveProgress}
          </Grid>
        </Grid>

        const infoClasses = selected ? ClassNames(classes.listItem, classes.listItemSelected) : classes.listItem
        const infoContent = <Grid key={id} container direction='row' className={infoClasses} style={styleItem} onClick={this.handleSelect.bind(this, id)}>
          <Grid key={`${id}-color`} item xs={8} className={ClassNames(classes.itemColumn, classes.itemColumnLeft)}>
            {visibleIcon}
          </Grid>
          <Grid key={`${id}-save`} item xs={2} className={classes.itemColumn}>
            {saveIcon}
          </Grid>
          <Grid key={`${id}-remove`} item xs={2} className={classes.itemColumn}>
            {deleteIcon}
          </Grid>
        </Grid>

        let itemContent = infoContent

        // To remove ?
        itemContent = remove ? removeContent : itemContent

        // To save ?
        itemContent = save ? saveContent : itemContent

        // List
        annotationsListContent.push(itemContent)

        // Draw
        if(!remove && (selected || visible || changed)) {
          const rectangleFill = <Rect x={topLeftX} y={topLeftY} width={width} height={height} fill={color} opacity={0.2}/>
          const rectangleBorder = <Rect x={topLeftX} y={topLeftY} width={width} height={height} stroke={color} strokeWidth={3}/>
          annotationsRectanglesContent.push(rectangleFill)
          annotationsRectanglesContent.push(rectangleBorder)
        }

        if(!remove && (selected)) {
          const topLeftCircle = <Circle x={topLeftX} y={topLeftY} radius={15} fill={color} draggable={true} dragBoundFunc={pos => this.handleDragBound(pos)} onDragMove={this.handleDragMove.bind(this, key, 'topLeft')}/>
          const bottomRightCircle = <Circle x={bottomRightX} y={bottomRightY} radius={15} fill={color} draggable={true} dragBoundFunc={pos => this.handleDragBound(pos)} onDragMove={this.handleDragMove.bind(this, key, 'bottomRight')}/>
          annotationsCirclesContent.push(topLeftCircle)
          annotationsCirclesContent.push(bottomRightCircle)
        }
      })
    }

    // Final loading check content
    const currentPhotoLoading = this.state.loadingPhoto
    const currentAnnotationsLoading = this.state.loadingAnnotations

    const currentListContent = currentAnnotationsLoading ?
    <div className={classes.progressContainer}>
      <CircularProgress size={128} thickness={2} className={classes.progressIcon} />
    </div> :
    <Grid container direction='row' justify='center' alignItems='center' className={classes.list}>
      {annotationsListContent}
    </Grid>

    const currentPhotoContent = currentPhotoLoading ?
    <div className={classes.progressContainer}>
      <CircularProgress size={128} thickness={2} className={classes.progressIcon} />
    </div> :
    <Stage ref={this.imageStage} className={classes.stage}>
      {/* IMAGE */}
      <Layer>
        {photoContent}
      </Layer>

      {/* RECTANGLES */}
      <Layer>
        {annotationsRectanglesContent}
      </Layer>

      {/* CIRCLES */}
      <Layer>
        {annotationsCirclesContent}
      </Layer>
    </Stage>

    return(
      <div>
        <Dialog
          fullScreen
          open={currentOpen}
          onClose={this.handleClose}
          classes={{
            paper: classes.dialog
          }}
        >
          <div className={classes.container}>
            {/* SELECTED */}
            <Grid container direction='row' justify='center' alignItems='center' className={classes.grid}>
              {/* ANNOTATIONS */}
              <Grid key={'annotations'} item xs={3} className={classes.column}>
                {/* TITLE */}
                <div className={classes.titleContainer}>
                  <Typography variant='title' className={classes.titleValue}>
                    ANNOTATIONS
                  </Typography>
                </div>

                {/* VALUES */}
                <div className={classes.data}>
                  {currentListContent}
                </div>
              </Grid>

              {/* PHOTO */}
              <Grid key={'photo'} item xs={9} className={classes.column}>
                {/* TITLE */}
                <div className={classes.titleContainer}>
                  <Typography variant='title' className={classes.titleValue}>
                    PHOTO: {currentSelectedPhoto}
                  </Typography>
                </div>

                {/* VALUE */}
                <div ref={this.imageContainer} className={classes.data}>
                  {currentPhotoContent}
                </div>
              </Grid>
            </Grid>

            {/* CLOSE */}
            <IconButton
              className={classes.close}
              onClick={this.handleClose}
            >
              <ClearIcon />
            </IconButton>

            {/* ADD */}
            <IconButton
              className={classes.add}
              onClick={this.handleAdd}
            >
              <AddIcon />
            </IconButton>
          </div>
        </Dialog>
      </div>
    )
  }
}

// ----- EXPORT

CustomGallerySelect.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomGallerySelect)
