// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Icons
import IconButton from '@material-ui/core/IconButton'
import ArrowRight from '@material-ui/icons/ChevronRight'
import ArrowLeft from '@material-ui/icons/ChevronLeft'

// Frontend components
import Typography from '@material-ui/core/Typography'
import Paper from '@material-ui/core/Paper'
import Grid from '@material-ui/core/Grid'

// ----- CUSTOM

// Custom components
import CustomSearch from '../components/customSearch'
import CustomGrid from '../components/customGrid'
import CustomSelect from '../components/customSelect'
import CustomSlider from '../components/customSlider'

// Custom assets
// ...

// Custom utils
import { sleep } from '../utils'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  container: {
    marginTop: 24,
    marginRight: 24,
    marginLeft: 152,
    height: 'calc(100vh - 88px)'
  },

  titleContainer: {
    position: 'relative',
    height: 72,

    marginTop: 24,
    marginBottom: 24,

    paddingTop: 18,
    paddingBottom: 18,
    paddingLeft: 24,

    backgroundColor: 'white',

    border: '1px solid #2196F3',
    borderLeftWidth: 5,
    borderRadius: 4
  },
  titleGrid: {

  },
  titleColumn: {

  },
  titleValue: {
    color: '#2196F3'
  },

  gridContainer: {
    position: 'relative',
    height: 'calc(100vh - 312px)'
  },

  paginationContainer: {
    position: 'relative',
    height: 80,

    marginTop: 24,
    marginBottom: 24,

    paddingTop: 24,

    borderTop: '1px solid #2196F3'
  },

  paginationColumn: {

  },

  columnLeft: {
    textAlign: 'left'
  },
  columnCenter: {
    textAlign: 'center'
  },
  columnRight: {
    textAlign: 'right'
  },

  columMarginFix: {
    marginLeft: 12,
    marginTop: -10
  },
  columnWidthFix: {
    flexBasis: 'unset'
  },

  paginationControl: {
    color: '#2196F3'
  },

  paginationButton: {

  }
})

// ----- DEFINE GRID PAGE

class CustomGridPage extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Refresh objects page
    await this.refreshPage()
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ...
  }

  // ----- EVENT HANDLERS

  handleSearchValue(value, event) {
    this.setState({
      search: value
    })
  }

  async handleSearchClick(event) {
    const currentSearch = this.state.search

    if(currentSearch) {
      // Disable search
      this.disableSearch()
    }

    // Refresh page
    await this.refreshPage()
  }

  async handleSearchClear(event) {
    const currentItemsPerPage = this.state.itemsPerPage

    // Clear search value
    this.setState({
      search: ''
    })

    // Enable search
    this.enableSearch()

    // Fetch new objects
    await this.fetchObjects(1, currentItemsPerPage)
  }

  async handleSearchInput(event) {
    if(event.which === 13) {
      const currentSearch = this.state.search

      if(currentSearch) {
        // Disable search
        this.disableSearch()
      }

      // Refresh page
      await this.refreshPage()
    }
  }

  handleSelect(id, event) {
    this.setState({
      selected: id
    })
  }

  async handleUnselect(event) {
    this.setState({
      selected: ''
    })

    // Refresh page
    await this.refreshPage()
  }

  async handleBackPage(event) {
    const currentPage = this.state.page
    const currentItemsPerPage = this.state.itemsPerPage

    const isFirstPage = currentPage === 1

    if(!isFirstPage) {
      // Fetch new objects
      await this.fetchObjects(currentPage - 1, currentItemsPerPage)

      this.setState({
        page: currentPage - 1
      })
    }
  }

  async handleNextPage(event) {
    const currentValues = this.state.values
    const currentKeys = currentValues ? Object.keys(currentValues) : null
    const currentSize = currentKeys ? currentKeys.length : 0

    const currentCount = this.state.count
    const currentPage = this.state.page
    const currentItemsPerPage = this.state.itemsPerPage

    const currentOffsetStart = (currentPage - 1) * currentItemsPerPage + 1
    const currentOffsetEnd = currentOffsetStart + Math.min(currentSize, currentItemsPerPage) - 1

    const isLastPage = currentOffsetEnd >= currentCount

    if(!isLastPage) {
      // Fetch new objects
      await this.fetchObjects(currentPage + 1, currentItemsPerPage)

      this.setState({
        page: currentPage + 1
      })
    }
  }

  async handleChangeItemsPerPage(number, event) {
    // Fetch new objects
    await this.fetchObjects(1, number)

    this.setState({
      page: 1,
      itemsPerPage: number
    })
  }

  // ----- METHODS

  async refreshPage() {
    const currentPage = this.state.page
    const currentItemsPerPage = this.state.itemsPerPage
    const currentSearch = this.state.search

    // Fetch available objects
    await this.fetchObjects(currentPage, currentItemsPerPage, currentSearch)
  }

  disableSearch() {
    this.setState({
      searchDisabled: true
    })
  }

  enableSearch() {
    this.setState({
      searchDisabled: false
    })
  }

  async showLoading() {
    this.setState({
      loading: true
    })

    // Sleep for 1 second
    await sleep(1000)
  }

  hideLoading() {
    this.setState({
      loading: false
    })
  }

  // ----- COMMUNICATIONS

  async fetchObjects(page, itemsPerPage, label) {
    const where = label ? { label } : {}
    const offset = (page - 1) * itemsPerPage
    const limit = itemsPerPage

    const filter = {
      where,
      offset,
      limit
    }

    // Toggle loading
    await this.showLoading()

    try {
      // Fetch available objects
      const availableObjects = await this.state.service.getObjects(filter)
      const rows = availableObjects.rows
      const count = availableObjects.count

      const formattedObjects = rows.reduce((result, value) => {
          result[value.id] = value
          return result
      }, {})

      this.setState({
        values: formattedObjects,
        count
      })

      // Notification
      // this.state.notification('Models', `Fetched available platform objects`, 'success')
    } catch(error) {
      this.setState({
        values: ''
      })

      // Notification
      this.state.notification('Models', `Could not retrieve available platform objects: ${error.message}`, 'error')
    }

    // Toggle loading
    this.hideLoading()
  }

  // TODO async addObject(name, description) {}
  // TODO async removeObject(id) {}

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleSearchClick = this.handleSearchClick.bind(this)
    this.handleSearchClear = this.handleSearchClear.bind(this)
    this.handleSearchInput = this.handleSearchInput.bind(this)
    this.handleSearchValue = this.handleSearchValue.bind(this)

    this.handleSelect = this.handleSelect.bind(this)
    this.handleUnselect = this.handleUnselect.bind(this)

    this.handleBackPage = this.handleBackPage.bind(this)
    this.handleNextPage = this.handleNextPage.bind(this)
    this.handleChangeItemsPerPage = this.handleChangeItemsPerPage.bind(this)

    // ----- STATE

    this.state = {
      notification: this.props.notification,
      service: this.props.service,

      title: this.props.title,
      search: '',
      searchDisabled: false,

      values: '',
      loading: false,

      count: 0,
      page: 1,
      itemsPerPage: 10,

      selected: ''
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props

    // Format values
    const currentValues = this.state.values
    const currentKeys = currentValues ? Object.keys(currentValues) : null
    const currentSize = currentKeys ? currentKeys.length : 0

    const currentSearch = this.state.search
    const currentSearchDisabled = this.state.searchDisabled
    const currentSelected = this.state.selected

    const currentCount = this.state.count
    const currentPage = this.state.page
    const currentItemsPerPage = this.state.itemsPerPage

    const currentOffsetStart = (currentPage - 1) * currentItemsPerPage + 1
    const currentOffsetEnd = currentOffsetStart + Math.min(currentSize, currentItemsPerPage) - 1

    const currentTitle = this.state.title
    const currentStats = `${currentOffsetStart} - ${currentOffsetEnd} OF ${currentCount}`
    const waitingStats = `Loading page ...`

    // Validations
    const isFirstPage = currentPage === 1
    const isLastPage = currentOffsetEnd >= currentCount
    const isLoading = this.state.loading

    // Final values
    const gridValues = !isLoading ? currentValues : ''
    const statsValues = !isLoading ? currentStats : waitingStats

    return (
      <div className={classes.container}>
        {/* TITLE */}
        <div className={classes.titleContainer}>
          <Grid container direction='row' alignItems='center' className={classes.titleGrid}>
            <Grid item xs={4} className={ClassNames(classes.titleColumn, classes.columnLeft)}>
              <Typography variant='title' className={classes.titleValue}>
                {currentTitle}
              </Typography>
            </Grid>
            <Grid item xs={4} className={ClassNames(classes.titleColumn, classes.columnCenter)}>
            </Grid>
            <Grid item xs={4} className={ClassNames(classes.titleColumn, classes.columnRight)}>
              <CustomSearch
                label={'Search ...'}
                value={currentSearch}
                disabled={currentSearchDisabled}
                onChange={this.handleSearchValue}
                onKeyClick={this.handleSearchClick}
                onKeyClear={this.handleSearchClear}
                onKeyInput={this.handleSearchInput}
                className={classes.titleSearch}/>
            </Grid>
          </Grid>
        </div>

        {/* GRID */}
        <div className={classes.gridContainer}>
          <CustomGrid loading={isLoading} values={gridValues} onSelect={this.handleSelect}/>
        </div>

        {/* PAGINATION */}
        <Grid container direction='row' justify='center' alignItems='center' className={classes.paginationContainer}>
          <Grid item xs={4} className={ClassNames(classes.paginationColumn, classes.columnLeft)}>
            <Grid container direction='row' alignItems='center'>
              {/* CONTROL */}
              <Grid item xs={4} className={ClassNames(classes.paginationColumn, classes.columnLeft, classes.columnWidthFix)}>
                <Typography className={classes.paginationControl}>
                  Showing
                </Typography>
              </Grid>
              <Grid item xs={4} className={ClassNames(classes.paginationColumn, classes.columnLeft, classes.columnWidthFix, classes.columMarginFix)}>
                <CustomSelect label={''} value={currentItemsPerPage} onChange={this.handleChangeItemsPerPage} />
              </Grid>
              <Grid item xs={4} className={classes.paginationColumn}>

              </Grid>
            </Grid>
          </Grid>
          <Grid item xs={4} className={ClassNames(classes.paginationColumn, classes.columnCenter)}>
            {/* STATS */}
            <Typography className={classes.paginationControl}>
              {statsValues}
            </Typography>
          </Grid>
          <Grid item xs={4} className={ClassNames(classes.paginationColumn, classes.columnRight)}>
            {/* BUTTONS */}
            <IconButton className={classes.paginationButton} disabled={isFirstPage} onClick={this.handleBackPage}>
              <ArrowLeft />
            </IconButton>
            <IconButton className={classes.paginationButton} disabled={isLastPage} onClick={this.handleNextPage}>
              <ArrowRight />
            </IconButton>
          </Grid>
        </Grid>

        {/* SELECT */}
        <CustomSlider service={this.state.service} notification={this.state.notification} selected={currentSelected} onClose={this.handleUnselect} />

        {this.props.children}
      </div>
    )
  }
}

// ----- EXPORT

CustomGridPage.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomGridPage)
