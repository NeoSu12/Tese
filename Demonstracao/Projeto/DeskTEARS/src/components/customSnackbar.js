// ----- IMPORTS

// React
import React from 'react'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

import blue from '@material-ui/core/colors/blue'
import green from '@material-ui/core/colors/green'
import amber from '@material-ui/core/colors/amber'
import red from '@material-ui/core/colors/red'
import grey from '@material-ui/core/colors/grey'

// Icons
import SuccessIcon from '@material-ui/icons/CheckCircle'
import ErrorIcon from '@material-ui/icons/Error'
import WarningIcon from '@material-ui/icons/Warning'
import InfoIcon from '@material-ui/icons/Info'
import CloseIcon from '@material-ui/icons/Close'

// Components
import Snackbar from '@material-ui/core/Snackbar'
import SnackbarContent from '@material-ui/core/SnackbarContent'
import IconButton from '@material-ui/core/IconButton'

// Classes
import moment from 'moment'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  container: {
    width: 400,
    flexWrap: 'unset',

    backgroundColor: 'white',
    boxShadow: 'none',
    border: '1px solid',
    borderColor: grey[500]
  },

  successColor: {
    color: green[600]
  },
  errorColor: {
    color: red[600]
  },
  infoColor: {
    color: blue[500]
  },
  warningColor: {
    color: amber[800]
  },
  normalColor: {
    color: grey[500]
  },

  successBorder: {
    borderColor: green[600]
  },
  errorBorder: {
    borderColor: red[600]
  },
  infoBorder: {
    borderColor: blue[500]
  },
  warningBorder: {
    borderColor: amber[700]
  },

  snack: {
    position: 'unset',
    marginRight: 30,
    marginBottom: 10,

    '&:last-child': {
      marginBottom: 30
    }
  },

  close: {

  },

  icon: {
    fontSize: 20
  },

  header: {
    display: 'flex',
    flexDirection: 'row'
  },

  headerIcon: {
    opacity: 0.9,
    marginRight: theme.spacing.unit
  },

  title: {
    color: grey[800],
    marginTop: 8,
    textTransform: 'uppercase'
  },

  message: {
    color: grey[500]
  }
})

// ----- DEFINE SNACKBAR

class CustomSnackbar extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ...
  }

  // ----- EVENT HANDLERS

  handleClose(event, reason) {
    if (reason === 'clickaway')
      return

    // Update state
    this.setState({
      open: false
    })
  }

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleClose = this.handleClose.bind(this)

    // ----- STATE
    this.state = {
      title: this.props.title || 'Title',
      message: this.props.message || 'Message',
      variation: this.props.variation || 'info',
      hideDuration: this.props.hideDuration || 0,
      open: true
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props

    const icons = {
      'success': SuccessIcon,
      'warning': WarningIcon,
      'error': ErrorIcon,
      'info': InfoIcon
    }

    const currentTimestamp = moment().format('h:mm A')
    const currentTitle = this.state.title
    const currentMessage = this.state.message

    const currentLowerVariation = this.state.variation.toLowerCase()
    const currentVariation = icons.hasOwnProperty(currentLowerVariation) ? currentLowerVariation : 'info'

    const currentHideDuration = this.state.hideDuration > 0 ? this.state.hideDuration : 3000

    const currentColor = classes[`${currentVariation}Color`]
    const currentBorder = classes[`${currentVariation}Border`]
    const CurrentIcon = icons[currentVariation]
    const currentOpen = this.state.open

    return (
      <Snackbar
        anchorOrigin={{
          vertical: 'bottom',
          horizontal: 'right'
        }}
        open={currentOpen}
        autoHideDuration={currentHideDuration}
        onClose={this.handleClose}
        className={classes.snack}
      >
        <SnackbarContent
          className={ClassNames(classes.container, currentColor, currentBorder)}
          aria-describedby='client-snackbar'
          message={
            <div id='client-snackbar'>
              <div className={classes.header}>
                <CurrentIcon className={ClassNames(classes.icon, classes.headerIcon)} />
                <span className={currentColor}>
                  {currentVariation.toUpperCase()}
                </span>
                <span className={classes.normalColor}>
                  &nbsp;- {currentTimestamp}
                </span>
              </div>
              <div className={classes.title}>
                {currentTitle}
              </div>
              <div className={classes.normalColor}>
                {currentMessage}
              </div>
            </div>
          }
          action={[
            <IconButton
              key='close'
              aria-label='Close'
              color='inherit'
              className={classes.close}
              onClick={this.handleClose}
            >
              <CloseIcon className={classes.icon} />
            </IconButton>
          ]}
        />
      </Snackbar>
    )
  }
}

// ----- EXPORT

CustomSnackbar.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomSnackbar)
