// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Icons

// Frontend components
import Typography from '@material-ui/core/Typography'

import Dialog from '@material-ui/core/Dialog'
import DialogActions from '@material-ui/core/DialogActions'
import DialogContent from '@material-ui/core/DialogContent'
import DialogTitle from '@material-ui/core/DialogTitle'

import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import Divider from '@material-ui/core/Divider'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'

// ----- CUSTOM

// Custom components
// ...

// Custom assets
// ...

// Custom utils
// ...

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  container: {

  },
  title: {
    textTransform: 'uppercase'
  }
})

// ----- DEFINE DIALOG

class CustomDialog extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ...
  }

  // ----- EVENT HANDLERS

  handleCancel(event) {
    const currentCancelEvent = this.state.onCancel

    // Custom cancel event
    if(currentCancelEvent)
      currentCancelEvent(event)
  }

  handleAccept(event) {
    const currentAcceptEvent = this.state.onAccept

    // Custom accept event
    if(currentAcceptEvent)
      currentAcceptEvent(event)
  }

  // ----- METHODS
  // ...

  // ----- COMMUNICATIONS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleCancel = this.handleCancel.bind(this)
    this.handleAccept = this.handleAccept.bind(this)

    // ----- STATE

    this.state = {
      title: this.props.title,
      rejectTitle: this.props.rejectTitle || 'Cancel',
      acceptTitle: this.props.acceptTitle || 'Accept',
      onCancel: this.props.onCancel,
      onAccept: this.props.onAccept
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props

    const currentTitle = this.state.title
    const currentAcceptTitle = this.state.acceptTitle
    const currentRejectTitle = this.state.rejectTitle

    return (
      <div>
        <Dialog
          scroll={'body'}
          open={true}
          onClose={this.handleCancel}
          classes={{
            paper: classes.container
          }}
        >
          <DialogTitle id='form-dialog-title' className={classes.title}>{currentTitle}</DialogTitle>
          <DialogContent>
            {this.props.children}
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleAccept} color='primary'>
              {currentAcceptTitle}
            </Button>
            <Button onClick={this.handleCancel} color='secondary'>
              {currentRejectTitle}
            </Button>
          </DialogActions>
        </Dialog>
      </div>
    )
  }

}

// ----- EXPORT

CustomDialog.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomDialog)
