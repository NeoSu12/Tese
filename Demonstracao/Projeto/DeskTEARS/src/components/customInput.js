// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'

// Utils
import uniqid from 'uniqid'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Frontend components
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'

import Input from '@material-ui/core/Input'
import InputLabel from '@material-ui/core/InputLabel'
import InputAdornment from '@material-ui/core/InputAdornment'
import IconButton from '@material-ui/core/IconButton';

import Visibility from '@material-ui/icons/Visibility'
import VisibilityOff from '@material-ui/icons/VisibilityOff'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  form: {
    width: '100%',
    borderTopLeftRadius: 4,
    borderTopRightRadius: 4,
    textAlign: 'center'
  },

  input: {
    paddingBottom: 6,
    '&:hover:before': {
      borderColor: 'gray !important',
      borderWidth: '1px !important'
    }
  },

  adornment: {
    marginTop: -5
  },

  visible: {
    color: '#2196F3'
  }
})

// ----- DEFINE INPUT

class CustomInput extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({
      label: nextProps.label || '',
      type: nextProps.type || 'text',

      value: nextProps.value,
      valid: nextProps.valid,
      error: nextProps.error,
      disabled: nextProps.disabled,

      required: nextProps.required,
      onChange: nextProps.onChange,
      onKeyInput: nextProps.onKeyInput,
    })
  }

  // ----- EVENT HANDLERS

  handleChangeValue(event) {
    // Value update
    const updateValue = event.target.value
    const updateLength = updateValue.length

    const currentShowPassword = this.state.showPassword
    let updateShowPassword = currentShowPassword

    if(this.isPassword())
      updateShowPassword = updateLength === 0 ? false : currentShowPassword

    this.setState({
       value: updateValue,
       showPassword: updateShowPassword
    })

    // Custom change value event
    const currentOnChange = this.state.onChange
    if(currentOnChange)
      currentOnChange(updateValue)
  }

  handleKeyInput(event) {
    // Custom key input event
    const currentOnKeyInput = this.state.onKeyInput
    if(currentOnKeyInput)
      currentOnKeyInput(event)
  }

  handleTogglePassword() {
    // Value update
    const currentShowPassword = this.state.showPassword
    this.setState({
      showPassword: !currentShowPassword
    })
  }

  handleMouseUpPassword(event) {
    event.preventDefault()

    this.setState({
      showPassword: false
    })
  }

  handleMouseDownPassword(event) {
    event.preventDefault()

    this.setState({
      showPassword: true
    })
  }

  handleMouseLeavePassword(event) {
    event.preventDefault()

    this.setState({
      showPassword: false
    })
  }

  // ----- METHODS

  isPassword() {
    const currentType = this.state.type
    return currentType && currentType.toUpperCase() === 'PASSWORD'
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleKeyInput = this.handleKeyInput.bind(this)
    this.handleChangeValue = this.handleChangeValue.bind(this)

    this.handleMouseUpPassword = this.handleMouseUpPassword.bind(this)
    this.handleMouseDownPassword = this.handleMouseDownPassword.bind(this)
    this.handleMouseLeavePassword = this.handleMouseLeavePassword.bind(this)

    // ----- STATE

    this.state = {
      id: this.props.id || uniqid(),
      label: this.props.label || '',
      type: this.props.type || 'text',
      value: this.props.value || '',

      valid: true,
      error: '',
      disabled: false,

      required: this.props.required,
      onChange: this.props.onChange,
      onKeyInput: this.props.onKeyInput,

      showPassword: false
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const inputId = this.state.id
    const inputLabel = this.state.label
    const inputValue = this.state.value
    const inputError = this.state.error

    const isPassword = this.isPassword()
    const showPassword = this.state.showPassword
    const showIcon = showPassword ? <Visibility className={this.props.classes.visible}/> : <VisibilityOff />

    const isDisabled = this.state.disabled
    const isRequired = this.state.required
    const isValid = this.state.valid
    const inputType = !isPassword || (isPassword && showPassword) ? 'text' : 'password'

    const visibilityIcon = isPassword ?
      <InputAdornment position="end">
        <IconButton className={this.props.classes.adornment} disabled={isDisabled} onMouseUp={this.handleMouseUpPassword} onMouseDown={this.handleMouseDownPassword} onMouseLeave={this.handleMouseLeavePassword}>
          {showIcon}
        </IconButton>
      </InputAdornment> : ''

    return (
      <div>
        <FormControl className={this.props.classes.form} disabled={isDisabled} required={isRequired} error={!isValid}>
          <InputLabel htmlFor={inputId}>
            {inputLabel}
          </InputLabel>
          <Input
            fullWidth
            id={this.state.id}
            name={inputLabel}
            autoComplete={inputLabel}
            type={inputType}
            value={inputValue}
            className={this.props.classes.input}
            onChange={this.handleChangeValue}
            onKeyPress={this.handleKeyInput}
            endAdornment={visibilityIcon} />
          <FormHelperText>{inputError}</FormHelperText>
        </FormControl>

        {this.props.children}
      </div>
    )
  }
}

// ----- EXPORT

CustomInput.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomInput)
