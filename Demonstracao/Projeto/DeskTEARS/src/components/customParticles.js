// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Custom effects
import Particles from 'react-particles-js'
import ParticlesConfig from '../assets/particles/config'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  // Particles
  particles: {
    position: 'fixed',
    zIndex: -1,

    width: '100vw',
    height: '100vh',
    overflow: 'hidden'
  }
})

// ----- DEFINE PARTICLES

class CustomParticles extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      force: nextProps.force
    })
  }

  shouldComponentUpdate(nextProps, nextState) {
    const currentForce = this.state.force || nextProps.force
    return Boolean(currentForce)
  }

  // ----- EVENT HANDLERS
  // ...

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS
    // ...

    // ----- STATE

    this.state = {
      force: this.props.force
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props

    return (
      <Particles params={ParticlesConfig} className={classes.particles}/>
    )
  }
}

// ----- EXPORT

CustomParticles.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomParticles)
