// ----- IMPORTS
import React from 'react'

// Custom classes
import CustomDrugList from './CustomDrugList'
import CustomPhotoGallery from './CustomPhotoGallery'

// Frontend components
import { Row, Col, Alert, ListGroup, ListGroupItem, UncontrolledTooltip, Form, FormGroup, FormFeedback, Label, Input } from 'reactstrap'
import FontAwesomeIcon from 'react-fontawesome'

// ----- DEFINE CUSTOM MEDICATION SELECTION

export default class CustomMedicationSelect extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Update values
    await this.fetchPackage()
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- EVENTS
    // ...

    // ----- STATE
    this.setState({
      notification: nextProps.onNotification,
      unselect: nextProps.onUnselect
    })

    // ----- REFERENCES

    this.medicationService = nextProps.medicationService
  }

  // ----- EVENT HANDLERS

  // Package

  async handleRefresh(event) {
    // Refresh information
    await this.fetchPackage()

    // Refresh drugs
    // TODO ...

    // Refresh gallery thumbnails
    await this.gallery.current.handleRefresh()
  }

  async handleSave(event) {
    await this.updatePackage()
  }

  handleUnselect(key, event) {
    let currentUnselect = this.state.unselect

    // Custom unselect
    if(currentUnselect)
      currentUnselect(event)
  }

  // Information

  handleStringChange(key, event) {
    const input = event.target
    const inputValue = input.value
    const inputStart = input.selectionStart
    const inputEnd = input.selectionEnd

    let currentValue = this.state.value

    // Define new key value
    currentValue[key] = inputValue.toUpperCase()

    // Update input values
    this.setState({
      changed: true,
      value: currentValue
    },
    () => input.setSelectionRange(inputStart, inputEnd)
    )
  }

  handleFloatChange(key, event) {
    const input = event.target
    const inputValue = input.value

    let currentValue = this.state.value

    // Define new key value
    currentValue[key] = parseFloat(inputValue)

    // Update input values
    this.setState({
      changed: true,
      value: currentValue
    })
  }

  // ----- METHODS

  async fetchPackage() {
    let currentSelected = this.state.selected

    try {
      // Fetch medication
      let currentPackage = await this.medicationService.getPackage(currentSelected)

      // Update value
      this.setState({
        changed: false,
        value: currentPackage
      })

      // Validate value
      this.validatePackage()

      // Notification
      if(this.state.notification)
        this.state.notification('Packages', `SUCCESS - Fetched package '${currentSelected}' information`, 'success')
    } catch(error) {
      this.setState({
        changed: false,
        value: ''
      })

      if(this.state.notification)
        this.state.notification('Packages', `ERROR - Could not retrieve package '${currentSelected}' information: ${error.message}`, 'error')
    }
  }

  validatePackage() {
    let currentValue = this.state.value
    let invalidValues = ['name', 'laboratory', 'format', 'quantity', 'description']

    let removeIndex = -1
    if(currentValue) {
      // Validate name
      if(currentValue.name && currentValue.name.length > 0) {
        removeIndex = invalidValues.indexOf('name')

        if(removeIndex > -1)
          invalidValues.splice(removeIndex, 1)
      }

      // Validate laboratory
      if(currentValue.laboratory && currentValue.laboratory.length > 0) {
        removeIndex = invalidValues.indexOf('laboratory')

        if(removeIndex > -1)
          invalidValues.splice(removeIndex, 1)
      }

      // Validate format
      if(currentValue.form && currentValue.form.length > 0) {
        removeIndex = invalidValues.indexOf('format')

        if(removeIndex > -1)
          invalidValues.splice(removeIndex, 1)
      }

      // Validate quantity
      if(currentValue.quantityUnit && currentValue.quantity && currentValue.quantity > 0) {
        switch(currentValue.quantityUnit) {
          case 'UNIT':
            if(currentValue.quantity % 1 === 0) {
              removeIndex = invalidValues.indexOf('quantity')

              if(removeIndex > -1)
                invalidValues.splice(removeIndex, 1)
            }
            break

          case 'GRAM':
          case 'LITRE':
            removeIndex = invalidValues.indexOf('quantity')

            if(removeIndex > -1)
              invalidValues.splice(removeIndex, 1)
            break
        }
      }

      // Validate quantityUnit
      // ...

      // Validate description
      if(currentValue.description && currentValue.description.length > 0) {
        removeIndex = invalidValues.indexOf('description')

        if(removeIndex > -1)
          invalidValues.splice(removeIndex, 1)
      }
    }

    this.setState({
      invalid: invalidValues
    })

    return invalidValues.length === 0
  }

  async updatePackage() {
    let currentSelected = this.state.selected
    let currentValue = this.state.value

    let validation = this.validatePackage()
    if(validation) {
      try {
        // Update medication
        let updatedPackage = await this.medicationService.updatePackage(currentSelected, currentValue)

        // Update value
        this.setState({
          changed: false,
          value: updatedPackage
        })

        // Notification
        if(this.state.notification)
          this.state.notification('Packages', `SUCCESS - Updated package '${currentSelected}'`, 'success')
      } catch(error) {
        this.setState({
          changed: false,
          value: ''
        })

        if(this.state.notification)
          this.state.notification('Packages', `ERROR - Could not update package '${currentSelected}': ${error.message}`, 'error')
      }
    } else {
      if(this.state.notification)
        this.state.notification('Packages', `WARNING - Please check for any errors in your inputed values`, 'warning')
    }
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    // Package
    this.handleUnselect = this.handleUnselect.bind(this)
    this.handleRefresh = this.handleRefresh.bind(this)
    this.handleSave = this.handleSave.bind(this)

    // Information
    this.handleStringChange = this.handleStringChange.bind(this)
    this.handleFloatChange = this.handleFloatChange.bind(this)

    // ----- STATE

    this.state = {
      notification: this.props.onNotification,
      unselect: this.props.onUnselect,
      selected: this.props.selected,

      changed: false,
      value: '',

      invalid: []
    }

    // ----- REFERENCES

    this.medicationService = this.props.medicationService

    this.drugs = React.createRef()
    this.gallery = React.createRef()
  }

  renderWaiting() {
    return (
      <div className='medication-information'>
        {/* NAVIGATION */}
        <div className='medication-table'>
          <div className='table-nav'>
            <FontAwesomeIcon id='selectBackButton' className='nav-button nav-button-back' name='chevron-left' onClick={this.handleUnselect} />
            <UncontrolledTooltip placement='top' target='selectBackButton'>
             Back
            </UncontrolledTooltip>

            <h5 className='display-4 nav-link'>?</h5>

            <FontAwesomeIcon id='selectRefreshButton' className='nav-button nav-button-refresh' name='sync' onClick={this.handleRefresh} />
            <UncontrolledTooltip placement='top' target='selectRefreshButton'>Refresh</UncontrolledTooltip>
          </div>
        </div>
      </div>
    )
  }

  renderValue() {
    let currentValue = this.state.value
    let currentInvalid = this.state.invalid

    let validName = currentInvalid.indexOf('name') < 0
    let validLaboratory = currentInvalid.indexOf('laboratory') < 0
    let validFormat = currentInvalid.indexOf('format') < 0
    let validQuantity = currentInvalid.indexOf('quantity') < 0
    let validDescription = currentInvalid.indexOf('description') < 0

    // Format buttons
    let saveIcon = this.state.changed ? <FontAwesomeIcon id='selectSaveButton' className='nav-button nav-button-save' name='save' onClick={this.handleSave} /> : ''
    let saveIconTooltip = saveIcon ? <UncontrolledTooltip placement='top' target='selectSaveButton'>Save</UncontrolledTooltip> : ''

    let refreshButton = this.state.changed ? <FontAwesomeIcon id='selectRefreshButton' className='nav-button nav-button-refresh' name='times' onClick={this.handleRefresh} /> : <FontAwesomeIcon id='selectRefreshButton' className='nav-button nav-button-refresh' name='sync' onClick={this.handleRefresh} />
    let refreshButtonTooltip = this.state.changed ? <UncontrolledTooltip placement='top' target='selectRefreshButton'>Cancel</UncontrolledTooltip> : <UncontrolledTooltip placement='top' target='selectRefreshButton'>Refresh</UncontrolledTooltip>

    // Format information
    let id = currentValue.id
    let name = currentValue.name
    let laboratory = currentValue.laboratory
    let format = currentValue.form

    let quantity = currentValue.quantity
    let quantityUnit = currentValue.quantityUnit

    let description = currentValue.description

    return (
      <div>
        {/* NAVIGATION */}
        <div className='medication-table'>
          <div className='table-nav'>
            <FontAwesomeIcon id='selectBackButton' className='nav-button nav-button-back' name='chevron-left' onClick={this.handleUnselect} />
            <UncontrolledTooltip placement='top' target='selectBackButton'>
             Back
            </UncontrolledTooltip>

            <h5 className='display-4 nav-link'>{name}</h5>

            {saveIcon}
            {saveIconTooltip}

            {refreshButton}
            {refreshButtonTooltip}
          </div>
        </div>

        <hr className='my-4' />

        {/* INFORMATION */}
        <div className='medication-information'>
          {/* BASIC */}
          <h5 className='display-4 information-title'>INFORMATION</h5>

          <Form>
            <FormGroup row>
              <Label for='packageId' sm={2}>ID</Label>
              <Col sm={10}>
                <Input type='text' name='id' id='packageId' placeholder='Id' value={id} disabled/>
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for='packageName' sm={2}>NAME</Label>
              <Col sm={10}>
                <Input type='text' name='name' id='packageName' placeholder='Name' value={name} invalid={!validName} onChange={this.handleStringChange.bind(this, 'name')}/>
                <FormFeedback>{!validName ? 'Package name cannot be blank' : ''}</FormFeedback>
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for='packageLaboratory' sm={2}>LABORATORY</Label>
              <Col sm={10}>
                <Input type='text' name='laboratory' id='packageLaboratory' placeholder='Laboratory' value={laboratory} invalid={!validLaboratory} onChange={this.handleStringChange.bind(this, 'laboratory')}/>
                <FormFeedback>{!validLaboratory ? 'Package laboratory cannot be blank' : ''}</FormFeedback>
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for='packageFormat' sm={2}>FORMAT</Label>
              <Col sm={10}>
                <Input type='text' name='format' id='packageFormat' placeholder='Format' value={format} invalid={!validFormat} onChange={this.handleStringChange.bind(this, 'form')}/>
                <FormFeedback>{!validFormat ? 'Package format cannot be blank' : ''}</FormFeedback>
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for='packageQuantity' sm={2}>QUANTITY</Label>
              <Col sm={8}>
                <Input type='number' step='0.01' name='quantity' id='packageQuantity' placeholder='Quantity' value={quantity} invalid={!validQuantity} onChange={this.handleFloatChange.bind(this, 'quantity')}/>
                <FormFeedback>{!validQuantity ? 'Package quantity must be bigger than zero and follow unit rules' : ''}</FormFeedback>
              </Col>
              <Col sm={2}>
                <Input type='select' name='quantityUnit' id='packageQuantityUnit' value={quantityUnit} onChange={this.handleStringChange.bind(this, 'quantityUnit')}>
                  <option>UNIT</option>
                  <option>GRAM</option>
                  <option>LITRE</option>
                </Input>
              </Col>
            </FormGroup>

            <FormGroup row>
              <Label for='packageDescription' sm={2}>DESCRIPTION</Label>
              <Col sm={10}>
                <Input type='textarea' name='description' id='packageDescription' value={description} invalid={!validDescription} onChange={this.handleStringChange.bind(this, 'description')}/>
                <FormFeedback>{!validDescription ? 'Package description cannot be blank' : ''}</FormFeedback>
              </Col>
            </FormGroup>
          </Form>

          {/* DRUGS */}
          <h5 className='display-4 information-title'>DRUGS</h5>
          <CustomDrugList ref={this.drugs} medicationService={this.medicationService} onNotification={this.state.notification} selected={this.state.selected}/>

          {/* PHOTOS */}
          <h5 className='display-4 information-title'>PHOTOS</h5>
          <CustomPhotoGallery ref={this.gallery} medicationService={this.medicationService} onNotification={this.state.notification} selected={this.state.selected}/>
        </div>
      </div>
    )
  }

  render() {
    let currentValue = this.state.value

    if(currentValue)
      return this.renderValue()
    else
      return this.renderWaiting()
  }
}
