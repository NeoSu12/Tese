// ----- IMPORTS

import React from 'react'

// Custom classes
import CustomNav from './CustomNav'
import CustomMenu from './CustomMenu'
import CustomMedication from './CustomMedication'
import { sleep } from '../utils'

// Frontend components
import FontAwesomeIcon from 'react-fontawesome'
import { Row, Col, UncontrolledAlert, Card, CardTitle } from 'reactstrap'

import ReactJson from 'react-json-view'

// Frontend animations
import { Transition, animated } from 'react-spring'

// ----- GLOBAL VARIABLES
// ...

// ----- DEFINE TOOL

export default class CustomTool extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Make first call to check training status
    await this.checkTrainingStatus()

    // Recall check training status every 30 seconds
    setInterval(async () => {
      await this.checkTrainingStatus()
    }, 30000)
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- EVENTS
    // ...

    // ----- STATE

    this.setState({
      logout: nextProps.onLogout,
      notification: nextProps.onNotification,
      status: nextProps.status || 'home'
    })

    // ----- REFERENCES

    this.medicationService = nextProps.medicationService
  }

  // ----- EVENT HANDLERS

  // Navigation buttons
  async handleTrain(event) {
    try {
      // Send learn request
      await this.medicationService.learn()

      // Check request status
      await this.checkTrainingStatus()

      if(this.state.notification)
        this.state.notification('Learning', `SUCCESS - Successfully sent intelligent system learning request, please stand by`, 'success')
    } catch(error) {
      if(this.state.notification)
        this.state.notification('Learning', `ERROR - Could not check and send intelligent system learning request: ${error.message}`, 'error')
    }
  }

  handleLogout(event) {
    let currentLogout = this.state.logout

    // Request online logout
    this.medicationService.logout()

    // Custom logout
    if(currentLogout)
      currentLogout()
  }

  // Navigation status
  handleHome(event) {
    let currentStatus = this.state.status

    if(currentStatus.toUpperCase() !== 'HOME') {
      // Update status to home
      this.setState({
        status: 'home'
      })
    }
  }

  // Menu buttons
  handleMedication(event) {
    // Update status to medication
    this.setState({
      status: 'medication'
    })
  }

  handleElderly(event) {
    // Update status to elderly
    this.setState({
      status: 'elderly'
    })
  }

  // ----- METHODS

  async checkTrainingStatus() {
    try {
      // Fetch training status
      const currentStatus = await this.medicationService.getLearningStatus()

      // Update value
      this.setState({
        training: currentStatus
      })

      // Notification
      if(this.state.notification)
        this.state.notification('Learning', `WARNING - Checking intelligent system learning status: ${currentStatus}`, 'warning')
    } catch(error) {
      this.setState({
        training: false
      })

      if(this.state.notification)
        this.state.notification('Learning', `ERROR - Could not retrieve intelligent system learning status: ${error.message}`, 'error')
    }
  }

  showTraining() {
    // Update training status to true
    this.setState({
      training: true
    })
  }

  hideTraining() {
    // Update training status to false
    this.setState({
      training: false
    })
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    // Home
    this.handleHome = this.handleHome.bind(this)

    // Train
    this.handleTrain = this.handleTrain.bind(this)

    // logout
    this.handleLogout = this.handleLogout.bind(this)

    // Medication
    this.handleMedication = this.handleMedication.bind(this)

    // Elderly
    this.handleElderly = this.handleElderly.bind(this)

    // ----- STATE

    this.state = {
      logout: this.props.onLogout,
      notification: this.props.onNotification,
      status: this.props.status || 'home',
      training: false
    }

    // ----- REFERENCES

    this.medicationService = this.props.medicationService

    // ----- SCENES

    this.homeScene = style =>
      <animated.div style={style} className='work-child'>
        <CustomMenu onMedication={this.handleMedication} onElderly={this.handleElderly}/>
      </animated.div>

    this.medicationScene = style =>
      <animated.div style={style} className='work-child'>
        <CustomMedication medicationService={this.medicationService} onNotification={this.state.notification}/>
      </animated.div>

    this.scenes = {
      'home': this.homeScene,
      'medication': this.medicationScene,
      // 'elderly': ...,
    }
  }

  render() {
    let data = this.medicationService.getAuth()
    let text = <div>Welcome back <span className='font-weight-bold'>{data.username}</span>!</div>
    let current = this.state.status != 'home' ? this.state.status : ''
    let training = this.state.training

    return (
      <div>
        <Row>
          <Col md={12}>
            <CustomNav text={text} current={current} training={training} onHome={this.handleHome} onTrain={this.handleTrain} onLogout={this.handleLogout}/>
          </Col>
        </Row>

        <hr className='my-4' />

        <UncontrolledAlert className='medication-alert' color='info'>
          <h4 className='alert-heading'>ATTENTION!</h4>
          <p>
            Our service requires as much medication package's information and photos as possible so that our intelligent system may learn to better recognize YOUR elderly's medication and help you keep their prescriptions up to date and most importantly, have some control over their daily doses of medication! So please, keep filling up our database and send those annotated pictures of yours, we thank you&nbsp;
            <FontAwesomeIcon name='smile'></FontAwesomeIcon>
          </p>
        </UncontrolledAlert>

        <div className='medication-work'>
          <div className='work-container'>
            <Transition
              from={{ opacity: 0, transform: 'translate3d(-25%,0,0)' }}
              enter={{ opacity: 1, transform: 'translate3d(0,0,0)' }}
              leave={{ opacity: 0, transform: 'translate3d(25%,0,0)' }}>
              {this.scenes[this.state.status]}
            </Transition>
          </div>
        </div>

        <hr className='my-4' />

        <Row>
          <Col md={12}>
            <Card body>
              <CardTitle>Caregiver Information</CardTitle>
              <ReactJson src={data}/>
            </Card>
          </Col>
        </Row>

        {this.props.children}
      </div>
    )
  }
}
