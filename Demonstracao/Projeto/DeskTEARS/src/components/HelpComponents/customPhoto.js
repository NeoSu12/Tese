// ----- IMPORTS
import React from 'react'
import Webcam from 'react-webcam'

// Frontend components
import FontAwesomeIcon from 'react-fontawesome'
import { UncontrolledTooltip, Button } from 'reactstrap'

// ----- GLOBAL VARIABLES
const VIDEO_OPTIONS = {
  width: 1280,
  height: 720,
  facingMode: 'user'
}

// ----- DEFINE CUSTOM INPUT

export default class CustomPhoto extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize(props)
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({
      picture: nextProps.onPicture,

      enableRecognition: nextProps.enableRecognition,
      recognition: nextProps.onRecognition
    })
  }

  // ----- EVENT HANDLERS
  handlePicture(event) {
    let currentPicture = this.state.picture

    // Custom picture
    if(currentPicture) {
      const image = this.webcam.getScreenshot()

      // Remove headers
      const result = image.replace('data:image/png;base64,', '')

      currentPicture(event, result)
    }
  }

  handleRecognition(event) {
    let currentRecognition = this.state.recognition

    // Custom recognition
    if(currentRecognition) {
      const image = this.webcam.getScreenshot()

      // Remove headers
      const result = image.replace('data:image/png;base64,', '')

      currentRecognition(event, result)
    }
  }

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    // Picture
    this.handlePicture = this.handlePicture.bind(this)

    // Recognition
    this.handleRecognition = this.handleRecognition.bind(this)

    // ----- STATE

    this.state = {
      picture: this.props.onPicture,

      enableRecognition: this.props.enableRecognition,
      recognition: this.props.onRecognition
    }

    // ----- REFERENCES
    // this.webcam
  }

  render() {
    let showRecognition = this.state.enableRecognition

    return (
      <div className='navigation-frame'>
        <Webcam
          audio={false}
          ref={(webcam) => {
            this.webcam = webcam
          }}
          screenshotFormat='image/png'
          width={'100%'}
          height={'100%'}
          videoConstraints={VIDEO_OPTIONS}
        />

        <Button id='pictureButton' color='primary' className='frame-button-picture' onClick={this.handlePicture}>
          <FontAwesomeIcon name='camera' onClick={this.handleRefresh} />
        </Button>
        <UncontrolledTooltip placement='top' target='pictureButton'>
          Take a picture!
        </UncontrolledTooltip>

        <Button id='recognitionButton' color='danger' className='frame-button-recognition' disabled={!showRecognition} onClick={this.handleRecognition}>
          <FontAwesomeIcon name='eye' onClick={this.handleRefresh} />
        </Button>
        <UncontrolledTooltip disabled={!showRecognition} placement='top' target='recognitionButton'>
          Recognize me!
        </UncontrolledTooltip>
        {this.props.children}
      </div>
    )
  }
}
