// ----- IMPORTS
import React from 'react'

// Custom classes
import CustomPhotoSelect from './CustomPhotoSelect'

// Frontend components
import { Alert, Row, Col, ListGroup, ListGroupItem, UncontrolledTooltip } from 'reactstrap'
import FontAwesomeIcon from 'react-fontawesome'

import { sleep, fileToBase64 } from '../utils'

// ----- DEFINE CUSTOM LIST

export default class CustomPhotoGallery extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Fetch selected value current thumbnails
    await this.fetchThumbnails()
  }

  componentWillUnmount() {
    // ...
  }

  async componentWillReceiveProps(nextProps) {
    // ----- EVENTS
    // ...

    // ----- STATE
    this.setState({
      notification: nextProps.onNotification
    })

    // ----- REFERENCES

    this.medicationService = nextProps.medicationService
  }

  // ----- EVENT HANDLERS

  async handleRefresh(event) {
    await this.fetchThumbnails()
  }

  handleAdd(event) {
    // Open file explorer
    this.explorer.current.click()
  }

  async handleFileChange(event) {
    let files = this.explorer.current.files

    let index = 0
    let size = files ? files.length : 0

    // Pick file from input
		if(size > 0) {
      // Show loading file
      this.showLoadingPhoto()

      for(index = 0; index < size; index ++) {
        let file = files[index]

        if(file) {
  				// Load image from file
  				let base64 = await fileToBase64(file)

          // Upload photo
          await this.addPhoto(base64)
  			}
      }

      // Hide loading photo
      this.hideLoadingPhoto()

      // Update available photos
      await this.fetchThumbnails()
		}

    // Clear input files
    this.explorer.current.value = ''
  }

  handleSelect(key, event) {
    this.setState({
      selectedPhoto: key
    })
  }

  async handleUnselect(event) {
    this.setState({
      selectedPhoto: ''
    })

    // Update available photos
    await this.fetchThumbnails()
  }

  async handleRemove(key, event) {
    let currentRemove = this.state.remove

    if(!currentRemove) {
      await this.removePhoto(key)
    }
    else {
      // Notification
      if(this.state.notification)
        this.state.notification('Photos', `WARNING - Server is still handling photo '${currentRemove}' removal, please stand by for a few seconds`, 'warning')
    }
  }

  // ----- METHODS

  // TODO - ADD PAGE CONTROL!
  async fetchThumbnails() {
    let currentSelectedPackage = this.state.selectedPackage

    try {
      let availableThumbnails = await this.medicationService.getThumbnails(currentSelectedPackage)

      let formattedThumbnails = availableThumbnails.reduce((result, value) => {
          result[value.id] = value
          return result
      }, {})

      this.setState({
        values: formattedThumbnails
      })

      // Notification
      if(this.state.notification)
        this.state.notification('Photos', `SUCCESS - Fetched package '${currentSelectedPackage}' thumbnails`, 'success')
    } catch(error) {
      this.setState({
        values: ''
      })

      if(this.state.notification)
        this.state.notification('Photos', `ERROR - Could not retrieve package '${currentSelectedPackage}' thumbnails: ${error.message}`, 'error')
    }
  }

  async addPhoto(base64) {
    let currentSelectedPackage = this.state.selectedPackage

    try {
      // Add new photo
      await this.medicationService.addPhoto(currentSelectedPackage, base64)

      // Notification
      if(this.state.notification)
        this.state.notification('Photos', `SUCCESS - Added new photo to package '${currentSelectedPackage}'`, 'success')
    } catch(error) {
      if(this.state.notification)
        this.state.notification('Photos', `ERROR - Could not add new photo to package '${currentSelectedPackage}': ${error.message}`, 'error')
    }
  }

  async removePhoto(photoId) {
    let currentSelectedPackage = this.state.selectedPackage

    // Show remove file
    this.showRemovePhoto(photoId)

    try {
      // Remove photo
      await this.medicationService.removePhoto(photoId)

      // Notification
      if(this.state.notification)
        this.state.notification('Photos', `SUCCESS - Removed photo '${photoId}' from package '${currentSelectedPackage}'`, 'success')
    } catch(error) {
      if(this.state.notification)
        this.state.notification('Photos', `ERROR - Could not remove photo '${photoId}' from package '${currentSelectedPackage}': ${error.message}`, 'error')
    }

    // Wait a few seconds
    await sleep(1000)

    // Update available photos
    await this.fetchThumbnails()

    // Hide loading photo
    this.hideRemovePhoto(photoId)
  }

  showLoadingPhoto() {
    this.setState({
      loading: true
    })
  }

  hideLoadingPhoto() {
    this.setState({
      loading: false
    })
  }

  showRemovePhoto(key) {
    this.setState({
      remove: key
    })
  }

  hideRemovePhoto() {
    this.setState({
      remove: ''
    })
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleAdd = this.handleAdd.bind(this)
    this.handleFileChange = this.handleFileChange.bind(this)

    this.handleSelect = this.handleSelect.bind(this)
    this.handleUnselect = this.handleUnselect.bind(this)
    this.handleRemove = this.handleRemove.bind(this)

    // ----- STATE

    this.state = {
      notification: this.props.onNotification,
      selectedPackage: this.props.selected,
      selectedPhoto: '',
      loading: false,
      remove: '',

      values: []
    }

    // ----- REFERENCES

    this.medicationService = this.props.medicationService

    this.explorer = React.createRef()
  }

  render() {
    let currentValues = this.state.values
    let currentKeys = Object.keys(currentValues)
    let currentSize = currentKeys.length

    // Format list
    let contentAdd = <ListGroupItem key='addPhoto' className='values-item' onClick={this.handleAdd}>
      <FontAwesomeIcon id='addButton' className='item-button item-button-plus' name='plus'/>
      <UncontrolledTooltip placement='top' target='addButton'>
       Add
      </UncontrolledTooltip>
    </ListGroupItem>
    let contentLoad = <ListGroupItem key='loadingPhoto' className='values-item'>
                        <div className='item-icon item-icon-loading'>
                          <FontAwesomeIcon name='spinner' spin/>
                        </div>
                      </ListGroupItem>
    let contentHeader = this.state.loading ? contentLoad : contentAdd
    let content = ''

    if(currentSize > 0) {
      content = currentKeys.map(key => {
          let value = currentValues[key]

          let id = value.id
          let thumbnail = 'data:image/png;base64,' + value.thumbnail
          let isValid = value.thumbnail
          let toRemove = this.state.remove && this.state.remove === id

          let customClassAnnotation = value.annotated ? 'item-image item-image-annotated' : 'item-image item-image-not-annotated'

          let contentValid = <div className='item-container'>
            <div className='item-overlay'>
              <div className='overlay-item' onClick={this.handleSelect.bind(this, id)}>
                <FontAwesomeIcon name='eye'/>
              </div>
              <div className='overlay-item' onClick={this.handleRemove.bind(this, id)}>
                <FontAwesomeIcon name='times'/>
              </div>
            </div>

            <div className={customClassAnnotation}>
              <img id={id} src={thumbnail} className='image-value'/>
            </div>
          </div>
          let contentInvalid = <div className='item-container'>
            <div className='item-overlay'>
              <div className='overlay-item' onClick={this.handleRemove.bind(this, id)}>
                <FontAwesomeIcon name='times'/>
              </div>
            </div>

            <div className='item-icon item-icon-invalid'>
              <FontAwesomeIcon name='exclamation'/>
            </div>
          </div>
          let contentRemove = <div className='item-icon item-icon-error'>
            <FontAwesomeIcon name='spinner' spin/>
          </div>

          let content = isValid ? contentValid : contentInvalid

          // To remove ?
          content = toRemove ? contentRemove : content

          return <ListGroupItem key={key} className='values-item'>
            {content}
          </ListGroupItem>
        }
      )
    }

    // Format selected
    let currentSelectedPhoto = this.state.selectedPhoto
    let contentSelected = currentSelectedPhoto ? <CustomPhotoSelect medicationService={this.medicationService} onNotification={this.state.notification} selected={currentSelectedPhoto} onClose={this.handleUnselect}/> : ''

    return(
      <div className='medication-gallery'>
        {/* VALUES */}
        <Row>
          <Col md={12}>
            <ListGroup className='gallery-values'>
              {contentHeader}
              {content}
            </ListGroup>
          </Col>
        </Row>

        {/* SELECTED */}
        {contentSelected}

        <input ref={this.explorer} className='gallery-explorer' type='file' accept='image/*' multiple='multiple' onChange={this.handleFileChange}></input>
      </div>
    )
  }
}
