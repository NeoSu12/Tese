// ----- IMPORTS
import React from 'react'

// Custom classes
import CustomMedicationList from './CustomMedicationList'
import CustomMedicationSelect from './CustomMedicationSelect'

// Frontend components
import { Row, Col, Alert, ListGroup, ListGroupItem, UncontrolledTooltip } from 'reactstrap'
import FontAwesomeIcon from 'react-fontawesome'

// Frontend animations
import { Transition, animated } from 'react-spring'

// ----- DEFINE CUSTOM LIST

export default class CustomMedication extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- EVENTS
    // ...

    // ----- STATE
    this.setState({
      notification: nextProps.onNotification
    })

    // ----- REFERENCES

    this.medicationService = nextProps.medicationService
  }

  // ----- EVENT HANDLERS

  handleSelect(key, event) {
    this.setState({
      selected: key
    })
  }

  handleUnselect(event) {
    this.setState({
      selected: ''
    })
  }

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleSelect = this.handleSelect.bind(this)
    this.handleUnselect = this.handleUnselect.bind(this)

    // ----- STATE

    this.state = {
      notification: this.props.onNotification,
      selected: ''
    }

    // ----- REFERENCES

    this.medicationService = this.props.medicationService

    // ----- SCENES

    this.listScene = style =>
      <animated.div style={style} className='work-child'>
        <CustomMedicationList medicationService={this.medicationService} onNotification={this.state.notification} onSelect={this.handleSelect}/>
      </animated.div>

    this.selectScene = style =>
      <animated.div style={style} className='work-child'>
        <CustomMedicationSelect medicationService={this.medicationService} onNotification={this.state.notification} onUnselect={this.handleUnselect} selected={this.state.selected}/>
      </animated.div>

    this.scenes = {
      'list': this.listScene,
      'select': this.selectScene
    }
  }

  render() {
    let currentSelected = this.state.selected
    let currentScene = currentSelected ? this.scenes['select'] : this.scenes['list']

    return(
      <div className='medication-work'>
        <div className='work-container'>
          <Transition
            from={{ opacity: 0 }}
            enter={{ opacity: 1 }}
            leave={{ opacity: 0 }}>
            {currentScene}
          </Transition>
        </div>
      </div>
    )
  }
}
