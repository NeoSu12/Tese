// ----- IMPORTS
import React from 'react'

// Frontend components
import { Alert } from 'reactstrap'
import FontAwesomeIcon from 'react-fontawesome'

// ----- DEFINE CUSTOM LIST

export default class CustomDrugList extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- EVENTS
    // ...

    // ----- STATE
    this.setState({
      notification: nextProps.onNotification
    })

    // ----- REFERENCES

    this.medicationService = nextProps.medicationService
  }

  // ----- EVENT HANDLERS
  // ...

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS
    // ...

    // ----- STATE

    this.state = {
      notification: this.props.onNotification,
      selected: this.props.selected
    }

    // ----- REFERENCES

    this.medicationService = this.props.medicationService
  }

  render() {
    return(
      <div className='medication-drugs'>
        <Alert className='medication-alert' color='warning'>
          <h4 className='alert-heading'><FontAwesomeIcon name='ban'></FontAwesomeIcon>&nbsp;IT'S TEMPORARY, WE PROMISE!</h4>
          <p align='justify'>
            Here users may edit and list which chemical compounds are part of this package. It is important to point out that this functionality will be added in the future!
          </p>
        </Alert>

        <br/>
      </div>
    )
  }
}
