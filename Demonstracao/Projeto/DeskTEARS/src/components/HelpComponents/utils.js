// ...

// ----- SYSTEM

async function sleep(time) {
	const realTime = time || 1000

	return new Promise((resolve, reject) => {
		setTimeout(resolve, realTime)
	})
}

// ----- FILES

async function fileToBase64(file) {
	return new Promise((resolve, reject) => {
		if(file) {
			let reader = new FileReader()

			reader.onload = () => {
	      let base64Header = /data:(.*?),/g
	      let result = reader.result
	      let filter = result ? result.replace(base64Header, '') : ''

				resolve(filter)
			}

			reader.readAsDataURL(file)
		}
		else
			reject()
	})
}

export { sleep, fileToBase64 }
