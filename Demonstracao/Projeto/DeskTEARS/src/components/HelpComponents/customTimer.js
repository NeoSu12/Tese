// ----- IMPORTS
import React from 'react'

// ----- DEFINE TIMER

export default class CustomTimer extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    this.interval = setInterval(() => {
      this.tick()
    }, 1000)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  componentWillReceiveProps(nextProps) {
    // ...
  }

  // ----- METHODS

  tick() {
    this.setState((prevState, props) => ({
      date: new Date()
    }))
  }

  // ----- BASIC

  initialize() {
    // ----- STATE

    this.state = {
      date: new Date()
    }
  }

  render() {
    return (
      <div>
        Current Time: {this.state.date.toLocaleTimeString()}
        {this.props.children}
      </div>
    )
  }
}
