// ----- IMPORTS
import React from 'react'

// Custom classes
import CustomSelector from './CustomSelector'

// Frontend components
import { Row, Col } from 'reactstrap'

// ----- DEFINE CUSTOM SELECTOR

export default class CustomMenu extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize(props)
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({
      medication: nextProps.onMedication,
      elderly: nextProps.onElderly
    })
  }

  // ----- EVENT HANDLERS

  handleMedication(event) {
    let currentMedication = this.state.medication

    // Custom medication event
    if(currentMedication)
      currentMedication(event)
  }

  handleElderly(event) {
    let currentElderly = this.state.elderly

    // Custom elderly event
    if(currentElderly)
      currentElderly(event)
  }

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleMedication = this.handleMedication.bind(this)
    this.handleElderly = this.handleElderly.bind(this)

    // ----- STATE

    this.state = {
      medication: this.props.onMedication,
      elderly: this.props.onElderly
    }
  }

  render() {
    let currentButtonStyle = {
        backgroundColor: this.state.color
    }

    let currentTitleStyle = {
        color: this.state.color
    }

    return (
      <div className='medication-menu'>
        <div className='menu-box'>
          <Row>
            <Col md={6}>
              <CustomSelector icon='medkit' title='MEDICATION' color='#FF7F7F' onClick={this.handleMedication} disabled={false}/>
            </Col>
            <Col md={6}>
              <CustomSelector icon='hands-helping' title='ELDERLY' color='#CCE5FF' onClick={this.handleElderly} disabled={true} />
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}
