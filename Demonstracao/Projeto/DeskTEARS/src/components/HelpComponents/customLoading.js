// ----- IMPORTS
import React from 'react'

// Frontend components
import ReactLoading from 'react-loading'

// ----- DEFINE CUSTOM LOADING

export default class CustomLoading extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize(props)
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({
      type: nextProps.type || 'bars',
      color: nextProps.color || '#FFFFFF',
      image: nextProps.image || '',
      message: nextProps.message || ''
    })
  }

  // ----- EVENT HANDLERS
  // ...

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS
    // ...

    // ----- STATE

    this.state = {
      type: this.props.type || 'bars',
      color: this.props.color || '#FFFFFF',
      image: this.props.image || '',
      message: this.props.message || ''
    }
  }

  render() {
    let realImage = this.state.image ? <img className='loading-value-image' src={'data:image/png;base64,' + this.state.image} /> : ''
    return (
      <div className='medication-loading'>
        <div className='loading-value'>
          {realImage}

          <ReactLoading className='loading-value-icon' type={this.state.type} color={this.state.color} />

          <div className='loading-value-message'>
            {this.state.message}
          </div>
        </div>
        {this.props.children}
      </div>
    )
  }
}
