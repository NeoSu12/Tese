// ----- IMPORTS
import React from 'react'

// Frontend components
import { UncontrolledTooltip } from 'reactstrap'
import { Stage, Layer, Image, Rect, Circle } from 'react-konva'
import Konva from 'konva'

import FontAwesomeIcon from 'react-fontawesome'

import { sleep } from '../utils'

// ----- DEFINE CUSTOM LIST

export default class CustomPhotoSelect extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Fetch selected value current photo
    await this.fetchPhoto()
  }

  componentWillUnmount() {
    // ...
  }

  async componentWillReceiveProps(nextProps) {
    // ----- EVENTS
    // ...

    // ----- STATE
    this.setState({
      notification: nextProps.onNotification,
      unselect: nextProps.onClose
    })

    // ----- REFERENCES

    this.medicationService = nextProps.medicationService
  }

  // ----- EVENT HANDLERS

  async handleClose(event) {
    let currentClose = this.state.close

    // Custom close
    if(currentClose)
      currentClose(event)
  }

  async handleSave(event) {
    // Show saving icon
    this.handleShowSaving()

    await this.updateAnnotation()

    // Hide save icon
    this.handleHideSave()

    // Hide saving icon
    this.handleHideSaving()
  }

  handleDragBound(position, key) {
    let currentImage = this.state.image
    let currentWidth = currentImage.width
    let currentHeight = currentImage.height

    let offsetWidth = this.state.offSet.x
    let offsetHeight = this.state.offSet.y

    let currentX = position.x
    let currentY = position.y

    let realX = 0
    let realY = 0

    // X position
    if(currentX > offsetWidth) {
      if(currentX < offsetWidth + currentWidth) {
        realX = currentX
      }
      else {
        realX = offsetWidth + currentWidth
      }
    }
    else {
      realX = offsetWidth
    }

    // Y position
    if(currentY > offsetHeight) {
      if(currentY < offsetHeight + currentHeight) {
        realY = currentY
      }
      else {
        realY = offsetHeight + currentHeight
      }
    }
    else {
      realY = offsetHeight
    }

    return {
      x: realX,
      y: realY
    }
  }

  handleDragMove(key, event) {
    let currentImage = this.state.image
    let currentWidth = currentImage.width
    let currentHeight = currentImage.height

    let offsetWidth = this.state.offSet.x
    let offsetHeight = this.state.offSet.y

    let currentX = event.target.x()
    let currentY = event.target.y()

    let realX = currentX - offsetWidth
    let realY = currentY - offsetHeight

    let percentageX = realX != 0 ? realX / currentWidth : 0
    let percentageY = realY != 0 ? realY / currentHeight : 0

    // Update rectangle value
    let currentRectangle = this.state.rectangle

    currentRectangle[key] = {
      x: percentageX,
      y: percentageY
    }

    this.setState({
      rectangle: currentRectangle
    })

    // Show save icon
    this.handleShowSave()
  }

  handleShowSave() {
    this.setState({
      changed: true
    })
  }

  handleHideSave() {
    this.setState({
      changed: false
    })
  }

  handleShowSaving() {
    this.setState({
      saving: true
    })
  }

  handleHideSaving() {
    this.setState({
      saving: false
    })
  }

  // ----- METHODS

  async fetchPhoto() {
    let currentSelected = this.state.selected

    try {
      // Get photo
      const photo = await this.medicationService.getPhoto(currentSelected)

      // Update image value
      let image = new window.Image()

      // Update rectangle value
      let rectangle = {
        topLeft: {
          x: photo.topLeftX || 0.0,
          y: photo.topLeftY || 0.0
        },
        bottomRight: {
          x: photo.bottomRightX || 1.0,
          y: photo.bottomRightY || 1.0
        }
      }

      image.id = photo.id
      image.src = 'data:image/png;base64,' + photo.image
      image.onload = () => {
        // Calculate image ratio
        let imageWidth = image.width
        let imageHeight = image.height

        let ratio = imageWidth / imageHeight

        // Change image width and height
        if(imageWidth > imageHeight) {
          image.width = 800
          image.height = 800 / ratio
        }
        else {
          image.width = 600 * ratio
          image.height = 600
        }

        // Calculate offset
        let currentWidth = image.width
        let currentHeight = image.height

        let offsetWidth = currentWidth <= 800 ? (850 - currentWidth) * 0.5 : 0
        let offsetHeight = currentHeight <= 600 ? (650 - currentHeight) * 0.5 : 0

        let offSet = {
          x: offsetWidth,
          y: offsetHeight
        }

        this.setState({
          image,
          oldRectangle: rectangle,
          rectangle,
          offSet
        })
      }

      // Notification
      if(this.state.notification)
        this.state.notification('Photos', `SUCCESS - Fetched photo '${currentSelected}'`, 'success')
    } catch(error) {
      if(this.state.notification)
        this.state.notification('Photos', `ERROR - Could not fetch photo '${currentSelected}': ${error.message}`, 'error')
    }
  }

  async updateAnnotation() {
    let currentSelected = this.state.selected
    let currentRectangle = this.state.rectangle

    // Validate rectangle points
    let currentTopLeft = currentRectangle.topLeft
    let currentBottomRight = currentRectangle.bottomRight

    let sumTopLeft = currentTopLeft.x + currentTopLeft.y
    let sumBottomRight = currentBottomRight.x + currentBottomRight.y

    if(sumTopLeft > sumBottomRight) {
      // TopLeft is the real BottomRight
      currentRectangle['topLeft'] = currentBottomRight
      currentRectangle['bottomRight'] = currentTopLeft
    }

    try {
      // Update annotation
      let updatedAnnotation = await this.medicationService.updateAnnotation(currentSelected, currentRectangle)
      let updatedRectangle = {
        topLeft: {
          x: updatedAnnotation.topLeftX || 0.0,
          y: updatedAnnotation.topLeftY || 0.0
        },
        bottomRight: {
          x: updatedAnnotation.bottomRightX || 1.0,
          y: updatedAnnotation.bottomRightY || 1.0
        }
      }

      // Update value
      this.setState({
        oldRectangle: updatedRectangle,
        rectangle: updatedRectangle
      })

      // Notification
      if(this.state.notification)
        this.state.notification('Packages', `SUCCESS - Updated photo '${currentSelected}' rectangle annotation`, 'success')
    } catch(error) {
      this.setState({
        rectangle: this.state.oldRectangle
      })

      if(this.state.notification)
        this.state.notification('Packages', `ERROR - Could not update photo '${currentSelected}' rectangle annotation: ${error.message}`, 'error')
    }
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleClose = this.handleClose.bind(this)
    this.handleSave = this.handleSave.bind(this)

    this.handleDragBound = this.handleDragBound.bind(this)
    this.handleDragMove = this.handleDragMove.bind(this)

    // ----- STATE

    this.state = {
      notification: this.props.onNotification,
      selected: this.props.selected,
      close: this.props.onClose,
      image: '',
      oldRectangle: '',
      rectangle: '',
      offSet: '',
      changed: false,
      saving: false
    }

    // ----- REFERENCES

    this.medicationService = this.props.medicationService
  }

  render() {
    let isLoading = !this.state.image

    // Format image
    let imageWidth = !isLoading ? this.state.image.width : 0
    let imageHeight = !isLoading ? this.state.image.height : 0

    let rectangleTopLeft = this.state.rectangle ? this.state.rectangle.topLeft : ''
    let rectangleBottomRight = this.state.rectangle ? this.state.rectangle.bottomRight : ''

    let rectangleWidth = (rectangleBottomRight.x - rectangleTopLeft.x) * imageWidth
    let rectangleHeight = (rectangleBottomRight.y - rectangleTopLeft.y) * imageHeight

    let offsetX = !isLoading ? this.state.offSet.x : 0
    let offsetY = !isLoading ? this.state.offSet.y : 0

    let content = isLoading ? <div className='block-icon'>
      <FontAwesomeIcon name='spinner' spin/>
    </div> : <Stage className='block-stage' width={850} height={650}>
      <Layer>
        <Image x={offsetX} y={offsetY} image={this.state.image} width={imageWidth} height={imageHeight} />
      </Layer>
      <Layer>
        {/* RECTANGLE */}
        <Rect x={offsetX + rectangleTopLeft.x * imageWidth} y={offsetY + rectangleTopLeft.y * imageHeight} width={rectangleWidth} height={rectangleHeight} fill='#FF7F7F' opacity={0.2}/>
        {/* BORDER */}
        <Rect x={offsetX + rectangleTopLeft.x * imageWidth} y={offsetY + rectangleTopLeft.y * imageHeight} width={rectangleWidth} height={rectangleHeight} stroke='#FF7F7F' strokeWidth={3}/>
      </Layer>
      <Layer>
        {/* CIRCLES */}
        <Circle x={offsetX + rectangleTopLeft.x * imageWidth} y={offsetY + rectangleTopLeft.y * imageHeight} radius={15} fill='#FF7F7F' draggable={true} dragBoundFunc={(pos) => this.handleDragBound(pos, 'topLeft')} onDragMove={this.handleDragMove.bind(this, 'topLeft')}/>
        <Circle x={offsetX + rectangleBottomRight.x * imageWidth} y={offsetY + rectangleBottomRight.y * imageHeight} radius={15} fill='#FF7F7F' draggable={true} dragBoundFunc={(pos) => this.handleDragBound(pos, 'bottomRight')} onDragMove={this.handleDragMove.bind(this, 'bottomRight')}/>
      </Layer>
    </Stage>

    // Format buttons
    let saveIcon = ''
    let saveIconTooltip = ''

    let changed = this.state.changed
    let saving = this.state.saving

    if(changed) {
        if(saving) {
          saveIcon = <FontAwesomeIcon className='container-button container-save' name='spinner' spin/>
        }
        else {
          saveIcon = <FontAwesomeIcon id='photoSaveButton' className='container-button container-save' name='save' onClick={this.handleSave} />
          saveIconTooltip = <UncontrolledTooltip placement='top' target='photoSaveButton'>Save</UncontrolledTooltip>
        }
    }

    let closeIcon = <FontAwesomeIcon id='photoCloseButton' className='container-button container-close' name='times' onClick={this.handleClose}/>
    let closeIconTooltip = <UncontrolledTooltip placement='top' target='photoCloseButton'>Close</UncontrolledTooltip>

    return(
      <div className='medication-annotation'>
        <div className='annotation-container'>
          {/* SAVE ICON */}
          {saveIcon}
          {saveIconTooltip}

            {/* CLOSE ICON */}
          {closeIcon}
          {closeIconTooltip}

          {/* PHOTO */}
          <div className='container-block'>
            {content}
          </div>
        </div>
      </div>
    )
  }
}
