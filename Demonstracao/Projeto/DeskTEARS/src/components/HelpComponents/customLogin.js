// ----- IMPORTS
import React from 'react'
import ReactJson from 'react-json-view'

// Frontend components
import FontAwesomeIcon from 'react-fontawesome'
import { Row, Col, Form, FormGroup, FormFeedback, Label, Input, Button, Card, CardTitle, UncontrolledAlert } from 'reactstrap'

// ----- DEFINE TOOL

export default class CustomLogin extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Fetch platform available users
    await this.fetchUsers()
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({
      username: {
        value: '',
        valid: true,
        error: ''
      },
      password: {
        value: '',
        valid: true,
        error: ''
      },
      caregivers: [],
      login: nextProps.onLogin,
      notification: nextProps.onNotification
    })

    // ----- REFERENCES

    this.medicationService = nextProps.medicationService
  }

  // ----- EVENTS

  // Inputs
  handleUsernameInput(event) {
    let value = event.target.value

    // Local update
    this.setState(
      {
        username: {
          value: value,
          valid: true,
          error: ''
        }
      }
    )
  }

  handlePasswordInput(event) {
    let value = event.target.value

    // Local update
    this.setState(
      {
        password: {
          value: value,
          valid: true,
          error: ''
        }
      }
    )
  }

  handleKeyInput(event) {
    if(event.which === 13)
      this.handleAuthentication(event)
  }

  // Buttons
  handleAuthentication(event) {
    let currentLogin = this.state.login

    // Verify inputs
    let user = this.state.username.value
    let pass = this.state.password.value

    let validUser = user && user.length > 0
    let validPass = pass && pass.length > 0

    if(validUser && validPass) {
      // Authentication call
      this.medicationService.authenticate(user, pass)
      .then(() => {
        // Custom login
        if(currentLogin)
          currentLogin()
      })
      .catch(error => {
        if(this.state.notification)
          this.state.notification('Authentication', `ERROR - Could not authenticate and retrieve client information: ${error.message}`, 'error')
      })
    }

    // Update input validation
    this.validateInputValues(validUser, validPass)
  }

  // ----- METHODS

  validateInputValues(validUsername, validPassword) {
    let errorUsername = !validUsername ? 'Invalid username!' : ''
    let errorPassword = !validPassword ? 'Invalid password!' : ''

    this.setState((prevState) => (
      {
        username: {
          value: prevState.username.value,
          valid: validUsername,
          error: errorUsername
        },
        password: {
          value: '',
          valid: validPassword,
          error: errorPassword
        }
      }
    ))
  }

  async fetchUsers() {
    try {
      let availableCaregivers = await this.medicationService.getCaregivers()

      this.setState({
        caregivers: availableCaregivers
      })

      if(this.state.notification)
        this.state.notification('Users', `SUCCESS - Fetched available platform caregivers`, 'success')
    } catch(error) {
      this.setState({
        caregivers: []
      })

      if(this.state.notification)
        this.state.notification('Users', `ERROR - Could not retrieve available platform caregivers: ${error.message}`, 'error')
    }
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleUsernameInput = this.handleUsernameInput.bind(this)
    this.handlePasswordInput = this.handlePasswordInput.bind(this)
    this.handleKeyInput = this.handleKeyInput.bind(this)

    this.handleAuthentication = this.handleAuthentication.bind(this)

    // ----- STATE

    this.state = {
      username: {
        value: '',
        valid: true,
        error: ''
      },
      password: {
        value: '',
        valid: true,
        error: ''
      },
      caregivers: [],
      login: this.props.onLogin,
      notification: this.props.onNotification
    }

    // ----- REFERENCES

    this.medicationService = this.props.medicationService
  }

  render() {
    let info = <UncontrolledAlert color='info'>
      <h4 className='alert-heading'>IMPORTANT!</h4>
      <p>
        To access our caregivers service platform, please use any of the available users listed bellow. If you can't find your name there, just submit a new username in the following input box!&nbsp;
        <FontAwesomeIcon name='smile'></FontAwesomeIcon>
      </p>
    </UncontrolledAlert>

    return (
      <div className='medication-login'>

        {info}

        <Row className='justify-content-center login-row'>
          <Col xs={12} md={8} lg={6} className='login-col'>
            <Form>
              <FormGroup>
                <Label for='username' hidden>Username</Label>
                <Input value={this.state.username.value} invalid={!this.state.username.valid} onChange={this.handleUsernameInput} onKeyPress={this.handleKeyInput} type='username' name='username' id='username' placeholder='Username' />
                <FormFeedback>{this.state.username.error}</FormFeedback>
              </FormGroup>

              <FormGroup>
                <Label for="password" hidden>Password</Label>
                <Input value={this.state.password.value} invalid={!this.state.password.valid} onChange={this.handlePasswordInput} onKeyPress={this.handleKeyInput} type="password" name="password" id="password" placeholder="Password" />
                <FormFeedback>{this.state.password.error}</FormFeedback>
              </FormGroup>

              <Button color='info' size='lg' onClick={this.handleAuthentication} block>Submit</Button>
            </Form>
          </Col>
        </Row>

        <hr className='my-4' />

        <Row>
          <Col md={12}>
            <Card body>
              <CardTitle>Available caregivers:</CardTitle>
              <ReactJson name={'Caregivers'} src={this.state.caregivers}/>
            </Card>
          </Col>
        </Row>
        {this.props.children}
      </div>
    )
  }
}
