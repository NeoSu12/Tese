// ----- IMPORTS
import React from 'react'

// Frontend components
import { Alert, ListGroup, ListGroupItem, Badge, UncontrolledTooltip } from 'reactstrap'
import FontAwesomeIcon from 'react-fontawesome'

// ----- DEFINE CUSTOM LIST

export default class CustomList extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- EVENTS

    this.select = nextProps.onSelect
    this.unselect = nextProps.onUnselect
    this.remove = nextProps.onRemove

    // ----- STATE

    this.setState({
      selected: nextProps.selected,
      values: nextProps.values || {}
    })
  }

  // ----- EVENT HANDLERS
  // ...

  // ----- METHODS

  handleSelect(key, event) {
    let currentSelect = this.select
    let currentUnselect = this.unselect

    // Select current item
    if(this.state.selected !== key) {
      this.setState({
        selected: key
      },
      () => {
        // Custom select
        if(currentSelect)
          currentSelect(key)
      })
    } else {
      this.setState({
        selected: ''
      },
      () => {
        // Custom unselect
        if(currentUnselect)
          currentUnselect()
      })
    }
  }

  handleRemove(key, event) {
    let currentRemove = this.remove

    // Custom remove
    if(currentRemove)
      currentRemove(key)
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    // List
    this.handleSelect = this.handleSelect.bind(this)
    this.handleRemove = this.handleRemove.bind(this)

    // Props
    this.select = this.props.onSelect
    this.unselect = this.props.onUnselect
    this.remove = this.props.onRemove

    // ----- STATE

    this.state = {
      selected: this.props.selected,
      values: this.props.values || {}
    }

    // ----- REFERENCES

    this.list = React.createRef()
  }

  render() {
    // Format content
    let currentValues = this.state.values
    let currentKeys = Object.keys(currentValues)
    let size = currentKeys.length

    let content = <Alert color='warning' className='guidance-alert'>
        WARNING - Currently there are no available values to list!
      </Alert>

    if(size > 0) {
      content = currentKeys.map(key => {
          let value = currentValues[key]
          let description = value.description

          let selected = key === this.state.selected ? 'item-selected' : ''
          let placed = value.placed ? 'item-placed' : ''
          let remove = selected ? <FontAwesomeIcon className='item-icon icon-clear' name='times' onClick={this.handleRemove.bind(this, key)}></FontAwesomeIcon> : ''

          let fullClassName = `list-item ${placed} ${selected}`

          let roles = value.roles
          let customBadges = ''

          if(roles) {
            customBadges = roles.map((role, index) => {
              let caption = role.caption
              let char = role.char
              let color = role.color
              let style = {
                'backgroundColor': color
              }
              return <Badge key={index} id={`${key}_BADGE${index}`} className='list-icon' style={style}>{char}<UncontrolledTooltip placement='top' target={`${key}_BADGE${index}`}>{caption}</UncontrolledTooltip></Badge>
            })
          }

          return <ListGroupItem key={key} className={fullClassName}>
            <div className='item-value' onClick={this.handleSelect.bind(this, key)}>
              <h4><Badge color='secondary'>{key}</Badge> {customBadges} - {description}</h4>
            </div>
            {remove}
          </ListGroupItem>
        }
      )
    }

    return (
      <div>
        <ListGroup className='guidance-list'>
          {content}
        </ListGroup>
        {this.props.children}
      </div>
    )
  }
}
