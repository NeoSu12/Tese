// ----- IMPORTS
import React from 'react'

// Frontend components
import FontAwesomeIcon from 'react-fontawesome'
import { UncontrolledTooltip } from 'reactstrap'

// ----- DEFINE CUSTOM medication

export default class CustomNav extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize(props)
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({
      text: nextProps.text || '',
      current: nextProps.current || '',
      home: nextProps.onHome,
      train: nextProps.onTrain,
      training: nextProps.training,
      logout: nextProps.onLogout
    })
  }

  // ----- EVENT HANDLERS

  handleHome(event) {
    let currentHome = this.state.home

    // Custom home
    if(currentHome)
      currentHome(event)
  }

  handleTrain(event) {
    let currentTrain = this.state.train

    // Custom train
    if(currentTrain)
      currentTrain(event)
  }

  handleLogout(event) {
    let currentLogout = this.state.logout

    // Custom logout
    if(currentLogout)
      currentLogout(event)
  }

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    // Input
    this.handleTrain = this.handleTrain.bind(this)
    this.handleLogout = this.handleLogout.bind(this)
    this.handleHome = this.handleHome.bind(this)

    // ----- STATE

    this.state = {
      text: this.props.text || '',
      current: this.props.current || '',
      home: this.props.onHome,
      train: this.props.onTrain,
      training: this.props.training,
      logout: this.props.onLogout
    }
  }

  render() {
    let arrow = this.state.current ? <h5 className='display-5 nav-text'>></h5> : ''
    let value = this.state.current ? <h5 className='display-5 nav-link'>{this.state.current.toUpperCase()}</h5> : ''

    let isTraining = this.state.training
    let trainIcon = isTraining ? <FontAwesomeIcon id='trainButton' className='nav-button nav-button-train' name='spinner' spin/> : <FontAwesomeIcon id='trainButton' className='nav-button nav-button-train' name='graduation-cap' onClick={this.handleTrain} />
    let trainIconTooltipText = isTraining ? 'Intelligent system is still learning ...' : 'Train intelligent system'

    return (
      <div className='medication-nav'>
        <div className='nav-group'>
          <h5 className='display-4 nav-text'>{this.state.text}</h5>

          {trainIcon}
          <UncontrolledTooltip placement='top' target='trainButton'>
            {trainIconTooltipText}
          </UncontrolledTooltip>

          <FontAwesomeIcon id='logoutButton' className='nav-button nav-button-logout' name='power-off' onClick={this.handleLogout} />
          <UncontrolledTooltip placement='top' target='logoutButton'>
           Logout
          </UncontrolledTooltip>
          {this.props.children}
        </div>

        <div className='nav-group nav-margin-top nav-keepwidth'>
          <h5 className='display-5 nav-link' onClick={this.handleHome}>HOME</h5>
          {arrow}
          {value}
        </div>
      </div>
    )
  }
}
