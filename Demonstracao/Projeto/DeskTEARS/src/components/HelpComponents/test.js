// ----- IMPORTS

import React from 'react'
import ReactDOM from 'react-dom'

// Custom fonts
import 'typeface-roboto'

// Custom classes
// ...

// Stylesheets
import '../sass/main.sass'

// Frontend components
import { CssBaseline, AppBar, Toolbar, IconButton, Typography, Button } from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu'

// ----- GLOBAL VARIABLES
// ...

// ----- DEFINE APP

export default class Test extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ...
  }

  // ----- EVENT HANDLERS
  // ...

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS
    // ...

    // ----- STATE

    this.state = {
      // ...
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    return (
      <React.Fragment>
        <CssBaseline />
        <div className="container">
          <AppBar position="static">
            <Toolbar variant="dense" className="header">
              <IconButton className="header-menu" color="inherit" aria-label="Menu">
                <MenuIcon />
              </IconButton>

              <Typography variant="title" color="inherit" className="header-title">
                Manager
              </Typography>

              <Button color="inherit">Login</Button>
            </Toolbar>
          </AppBar>

          <Typography variant="display4" gutterBottom>
            Display 4
          </Typography>
          <Typography variant="display3" gutterBottom>
            Display 3
          </Typography>
          <Typography variant="display2" gutterBottom>
            Display 2
          </Typography>
          <Typography variant="display1" gutterBottom>
            Display 1
          </Typography>
          <Typography variant="headline" gutterBottom>
            Headline
          </Typography>
          <Typography variant="title" gutterBottom>
            Title
          </Typography>
          <Typography variant="subheading" gutterBottom>
            Subheading
          </Typography>
          <Typography variant="body2" gutterBottom>
            Body 2
          </Typography>
          <Typography variant="body1" gutterBottom align="right">
            Body 1
          </Typography>
          <Typography variant="caption" gutterBottom align="center">
            Caption
          </Typography>
          <Typography gutterBottom noWrap>
            {`
              Lorem ipsum dolor sit amet, consectetur adipisicing elit,
              sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.
            `}
          </Typography>
          <Typography variant="button" gutterBottom>
            Button
          </Typography>

          {this.props.children}
        </div>
      </React.Fragment>
    )
  }
}
