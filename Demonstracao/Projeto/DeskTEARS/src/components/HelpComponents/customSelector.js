// ----- IMPORTS
import React from 'react'

// Frontend components
import FontAwesomeIcon from 'react-fontawesome'
import { Button } from 'reactstrap'

// ----- DEFINE CUSTOM SELECTOR

export default class CustomSelector extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize(props)
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({
      icon: nextProps.icon || 'exclamation',
      title: nextProps.title || '',
      color: nextProps.color || '#FFFFFF',
      click: nextProps.onClick,
      disabled : nextProps.disabled
    })
  }

  // ----- EVENT HANDLERS

  handleClick(event) {
    let currentDisabled = this.state.disabled
    let currentClick = this.state.click

    // Custom click event
    if(!currentDisabled && currentClick)
      currentClick()
  }

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleClick = this.handleClick.bind(this)

    // ----- STATE

    this.state = {
      icon: this.props.icon || 'exclamation',
      title: this.props.title || '',
      color: this.props.color || '#FFFFFF',
      click: this.props.onClick,
      disabled: this.props.disabled
    }
  }

  render() {
    let currentButtonClass = this.state.disabled ? 'selector-button-disabled' : 'selector-button'
    let currentButtonStyle = {
        backgroundColor: this.state.disabled ? '#E9ECEF' : this.state.color
    }

    let currentTitleStyle = {
        color: this.state.disabled ? '#E9ECEF' : this.state.color
    }

    return (
      <div className='medication-selector'>
        <Button style={currentButtonStyle} className={currentButtonClass} onClick={this.handleClick}>
          <FontAwesomeIcon className='selector-icon' name={this.state.icon}/>
        </Button>

        <div style={currentTitleStyle} className='selector-title'>
          {this.state.title}
        </div>

        {this.props.children}
      </div>
    )
  }
}
