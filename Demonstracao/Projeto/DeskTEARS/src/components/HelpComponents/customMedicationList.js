// ----- IMPORTS
import React from 'react'

// Frontend components
import { Row, Col, Alert, ListGroup, ListGroupItem, UncontrolledTooltip, Button, Modal, ModalHeader, ModalBody, ModalFooter, Card, CardBody, CardSubtitle, CardText, Label, Input, Form, FormGroup, FormFeedback } from 'reactstrap'
import FontAwesomeIcon from 'react-fontawesome'

// ----- GLOBAL VARIABLES
const DEFAULT_ADD_PACKAGE = {
  'name': '',
  'laboratory': '',
  'format': '',
  'quantity': '',
  'quantityUnit': 'UNIT',
  'description': ''
}

// ----- DEFINE CUSTOM MEDICATION LIST

export default class CustomMedicationList extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Update values
    await this.fetchPackages()
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- EVENTS
    // ...

    // ----- STATE
    this.setState({
      notification: nextProps.onNotification
    })

    // ----- REFERENCES

    this.medicationService = nextProps.medicationService
  }

  // ----- EVENT HANDLERS

  // Refresh
  async handleRefresh(event) {
    await this.fetchPackages()
  }

  // Select
  handleSelect(key, event) {
    let currentSelect = this.state.select

    // Custom select
    if(currentSelect)
      currentSelect(key, event)
  }

  // Add
  async handleAdd(event) {
    // Show modal to add package
    this.handleShowAdd()
  }

  handleAcceptAdd() {
    this.addPackage()
  }

  handleShowAdd() {
    // Show modal to remove package
    this.setState({
      add: true,
      addValue: Object.assign({}, DEFAULT_ADD_PACKAGE),
      addInvalid: []
    })
  }

  handleHideAdd() {
    // Hide modal to remove package
    this.setState({
      add: false,
      addValue: '',
      addInvalid: []
    })
  }

  handleAddStringChange(key, event) {
    const input = event.target
    const inputValue = input.value
    const inputStart = input.selectionStart
    const inputEnd = input.selectionEnd

    let currentAddValue = this.state.addValue

    // Define new key value
    currentAddValue[key] = inputValue.toUpperCase()

    // Update input values
    this.setState({
      changed: true,
      addValue: currentAddValue
    },
    () => input.setSelectionRange(inputStart, inputEnd)
    )
  }

  handleAddFloatChange(key, event) {
    const input = event.target
    const inputValue = input.value

    let currentAddValue = this.state.addValue

    // Define new key value
    currentAddValue[key] = parseFloat(inputValue)

    // Update input values
    this.setState({
      changed: true,
      addValue: currentAddValue
    })
  }

  validateAddPackage() {
    let currentAddValue = this.state.addValue
    let invalidAddValues = ['name', 'laboratory', 'format', 'quantity', 'description']

    let removeIndex = -1
    if(currentAddValue) {
      // Validate name
      if(currentAddValue.name && currentAddValue.name.length > 0) {
        removeIndex = invalidAddValues.indexOf('name')

        if(removeIndex > -1)
          invalidAddValues.splice(removeIndex, 1)
      }

      // Validate laboratory
      if(currentAddValue.laboratory && currentAddValue.laboratory.length > 0) {
        removeIndex = invalidAddValues.indexOf('laboratory')

        if(removeIndex > -1)
          invalidAddValues.splice(removeIndex, 1)
      }

      // Validate format
      if(currentAddValue.form && currentAddValue.form.length > 0) {
        removeIndex = invalidAddValues.indexOf('format')

        if(removeIndex > -1)
          invalidAddValues.splice(removeIndex, 1)
      }

      // Validate quantity
      if(currentAddValue.quantityUnit && currentAddValue.quantity && currentAddValue.quantity > 0) {
        switch(currentAddValue.quantityUnit) {
          case 'UNIT':
            if(Number.isInteger(currentAddValue.quantity)) {
              removeIndex = invalidAddValues.indexOf('quantity')

              if(removeIndex > -1)
                invalidAddValues.splice(removeIndex, 1)
            }
            break

          case 'GRAM':
          case 'LITRE':
            removeIndex = invalidAddValues.indexOf('quantity')

            if(removeIndex > -1)
              invalidAddValues.splice(removeIndex, 1)
            break
        }
      }

      // Validate quantityUnit
      // ...

      // Validate description
      if(currentAddValue.description && currentAddValue.description.length > 0) {
        removeIndex = invalidAddValues.indexOf('description')

        if(removeIndex > -1)
          invalidAddValues.splice(removeIndex, 1)
      }
    }

    this.setState({
      addInvalid: invalidAddValues
    })

    return invalidAddValues.length === 0
  }

  // Remove
  async handleRemove(key, event) {
    // Prevent default
    event.preventDefault()

    // Prevent propagation up the DOM element
    event.stopPropagation()

    // Show modal to remove package
    this.handleShowRemove(key)
  }

  handleAcceptRemove() {
    let currentRemove = this.state.remove

    if(currentRemove)
      // Remove package
      this.removePackage(currentRemove)

    // Hide modal to remove package
    this.handleHideRemove()
  }

  handleShowRemove(key) {
    // Show modal to remove package
    this.setState({
      remove: key
    })
  }

  handleHideRemove() {
    // Hide modal to remove package
    this.setState({
      remove: ''
    })
  }

  // ----- METHODS

  // TODO - ADD PAGE CONTROL!
  async fetchPackages() {
    try {
      let availablePackages = await this.medicationService.getPackages()

      let listPackages = availablePackages.rows
      let formattedPackages = listPackages.reduce((result, value) => {
          result[value.id] = value
          return result
      }, {})

      this.setState({
        values: formattedPackages
      })

      // Notification
      if(this.state.notification)
        this.state.notification('Packages', `SUCCESS - Fetched available platform packages`, 'success')
    } catch(error) {
      this.setState({
        values: ''
      })

      if(this.state.notification)
        this.state.notification('Packages', `ERROR - Could not retrieve available platform packages: ${error.message}`, 'error')
    }
  }

  async addPackage() {
    let currentValue = this.state.addValue
    let validation = this.validateAddPackage()

    if(validation) {
      try {
        // Add medication
        await this.medicationService.addPackage(currentValue)

        // Update current values
        await this.fetchPackages()

        // Notification
        if(this.state.notification)
          this.state.notification('Packages', `SUCCESS - Added new package`, 'success')
      } catch(error) {
        if(this.state.notification)
          this.state.notification('Packages', `ERROR - Could not add new package: ${error.message}`, 'error')
      }

      // Hide modal to add package
      this.handleHideAdd()
    } else {
      if(this.state.notification)
        this.state.notification('Packages', `WARNING - Please check for any errors in your inputed values`, 'warning')
    }
  }

  async removePackage(id) {
    try {
      // Remove package
      await this.medicationService.removePackage(id)

      // Update current values
      await this.fetchPackages()

      // Notification
      if(this.state.notification)
        this.state.notification('Packages', `SUCCESS - Removed package '${id}'`, 'success')
    } catch(error) {
      if(this.state.notification)
        this.state.notification('Packages', `ERROR - Could not remove package '${id}': ${error.message}`, 'error')
    }
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleSelect = this.handleSelect.bind(this)
    this.handleAdd = this.handleAdd.bind(this)

    this.handleAdd = this.handleAdd.bind(this)
    this.handleAcceptAdd = this.handleAcceptAdd.bind(this)
    this.handleShowAdd = this.handleShowAdd.bind(this)
    this.handleHideAdd = this.handleHideAdd.bind(this)

    // Information
    this.handleAddStringChange = this.handleAddStringChange.bind(this)
    this.handleAddFloatChange = this.handleAddFloatChange.bind(this)

    this.handleRemove = this.handleRemove.bind(this)
    this.handleAcceptRemove = this.handleAcceptRemove.bind(this)
    this.handleShowRemove = this.handleShowRemove.bind(this)
    this.handleHideRemove = this.handleHideRemove.bind(this)

    this.handleRefresh = this.handleRefresh.bind(this)

    // ----- STATE

    this.state = {
      notification: this.props.onNotification,
      select: this.props.onSelect,

      add: false,
      addValue: Object.assign({}, DEFAULT_ADD_PACKAGE),
      addInvalid: [],

      remove: '',

      values: '',
      page: 1,
      itemsPerPage: 10
    }

    // ----- REFERENCES

    this.medicationService = this.props.medicationService
  }

  render() {
    let currentValues = this.state.values
    let currentKeys = Object.keys(currentValues)
    let currentSize = currentKeys.length

    // Format list
    let content = <Alert color='warning' className='medication-alert'>
        WARNING - Currently there are no available values to list!
      </Alert>

    if(currentSize > 0) {
      content = currentKeys.map(key => {
          let value = currentValues[key]

          let id = value.id
          let name = value.name
          let laboratory = value.laboratory

          return <ListGroupItem key={key} className='values-item'>
            <Row onClick={this.handleSelect.bind(this, key)}>
              <Col md={3} className='item-value item-value-left'>
                <div className='value-text'>{id}</div>
              </Col>
              <Col md={3} className='item-value item-value-left'>
                <div className='value-text'>{name}</div>
              </Col>
              <Col md={3} className='item-value item-value-left'>
                <div className='value-text'>{laboratory}</div>
              </Col>

              <Col md={3} className='item-value item-value-right'>
                <FontAwesomeIcon className='value-icon value-icon-delete' name='times' onClick={this.handleRemove.bind(this, key)} />
              </Col>
            </Row>
          </ListGroupItem>
        }
      )
    }

    // Format remove modal
    let currentRemoveKey = this.state.remove
    let currentRemoveValue = currentValues && currentRemoveKey ? currentValues[currentRemoveKey] : null
    let currentRemoveId = currentRemoveValue ? currentRemoveValue.id.toUpperCase() : ''
    let currentRemoveName = currentRemoveValue ? currentRemoveValue.name : ''
    let currentRemoveLaboratory = currentRemoveValue ? currentRemoveValue.laboratory : ''

    let toggleRemove = currentRemoveKey && currentRemoveValue ? true : false

    // Format add modal
    let currentAddValue = this.state.addValue

    let currentAddName = currentAddValue && currentAddValue.name ? currentAddValue.name.toUpperCase() : ''
    let currentAddLaboratory = currentAddValue && currentAddValue.laboratory ? currentAddValue.laboratory.toUpperCase() : ''
    let currentAddFormat = currentAddValue && currentAddValue.form ? currentAddValue.form.toUpperCase() : ''
    let currentAddQuantity = currentAddValue && currentAddValue.quantity ? currentAddValue.quantity : ''
    let currentAddQuantityUnit = currentAddValue && currentAddValue.quantityUnit ? currentAddValue.quantityUnit.toUpperCase() : 'UNIT'
    let currentAddDescription = currentAddValue && currentAddValue.description ? currentAddValue.description.toUpperCase() : ''

    let currentAddInvalid = this.state.addInvalid

    let validAddName = currentAddInvalid.indexOf('name') < 0
    let validAddLaboratory = currentAddInvalid.indexOf('laboratory') < 0
    let validAddFormat = currentAddInvalid.indexOf('format') < 0
    let validAddQuantity = currentAddInvalid.indexOf('quantity') < 0
    let validAddDescription = currentAddInvalid.indexOf('description') < 0

    let toggleAdd = this.state.add

    return (
      <div className='medication-table'>
        {/* NAVIGATION */}
        <div className='table-nav'>
          <h5 className='display-4 nav-link'>MEDICATION</h5>

          <FontAwesomeIcon id='listRefreshButton' className='nav-button nav-button-refresh' name='sync' onClick={this.handleRefresh} />
          <UncontrolledTooltip placement='top' target='listRefreshButton'>
           Refresh
          </UncontrolledTooltip>

          <FontAwesomeIcon id='listAddButton' className='nav-button nav-button-plus' name='plus' onClick={this.handleAdd} />
          <UncontrolledTooltip placement='top' target='listAddButton'>
           Add
          </UncontrolledTooltip>
        </div>

        {/* HEADER */}
        <Row className='table-header'>
          <Col md={3}>
            <div className='header-column'>
              <h5 className='display-5'>ID</h5>
            </div>
          </Col>

          <Col md={3}>
            <div className='header-column'>
              <h5 className='display-5'>NAME</h5>
            </div>
          </Col>

          <Col md={3}>
            <div className='header-column'>
              <h5 className='display-5'>LABORATORY</h5>
            </div>
          </Col>

          <Col md={3}></Col>
        </Row>

        {/* VALUES */}
        <Row>
          <Col md={12}>
            <ListGroup className='table-values'>
              {content}
            </ListGroup>
          </Col>
        </Row>

        {/* REMOVE MODAL */}
        <Modal isOpen={toggleRemove}>
          <ModalHeader>Remove Package</ModalHeader>
          <ModalBody>
            <Row>
              <Col md={12}>
                <p align='justify'>
                  Are you sure you want to remove the following package from our medication platform database?
                </p>
              </Col>
            </Row>
            <Row>
              <Col md={12}>
                <Card>
                  <CardBody>
                    <CardSubtitle>ID</CardSubtitle>
                    <CardText>{currentRemoveId}</CardText>
                    <CardSubtitle>NAME</CardSubtitle>
                    <CardText>{currentRemoveName}</CardText>
                    <CardSubtitle>LABORATORY</CardSubtitle>
                    <CardText>{currentRemoveLaboratory}</CardText>
                  </CardBody>
                </Card>
              </Col>
            </Row>
            <br/>
            <Row>
              <Col md={12}>
                <Alert className='medication-alert' color='danger'>
                  <h4 className='alert-heading'><FontAwesomeIcon name='exclamation-triangle'></FontAwesomeIcon>&nbsp;HOLD ON!</h4>
                  <p align='justify'>
                    Before accepting any changes, please remember that <b>all information related to this package will become unavailable</b> to all other users that also use this platform, and it might affect their experience.
                  </p>
                  <hr className='my-2' />
                  <p align='justify'>
                    Additionally, by continuing with this process you are confirming and acknowledging that <b>all related photos and other sensible information referring to this medication package will remain saved in our database</b> and that we have the power to use and manipulate this data for other possible future purposes.
                  </p>
                </Alert>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button color='primary' onClick={this.handleAcceptRemove}>Accept</Button>{' '}
            <Button color='secondary' onClick={this.handleHideRemove}>Cancel</Button>
          </ModalFooter>
        </Modal>

        {/* ADD MODAL */}
        <Modal isOpen={toggleAdd}>
          <ModalHeader>Add Package</ModalHeader>
          <ModalBody>
            <Row>
              <Col md={12}>
                <p align='justify'>
                  Please, fill out the form bellow correctly to add a new package to our medication platform database.
                </p>
              </Col>
            </Row>
            <Row>
              <Col md={12}>
                <Card>
                  <CardBody>
                    <Form>
                      <FormGroup>
                        <Label for='packageName'>
                          <CardSubtitle>NAME</CardSubtitle>
                        </Label>
                        <Input type='text' name='name' id='packageName' placeholder='Name' value={currentAddName} invalid={!validAddName} onChange={this.handleAddStringChange.bind(this, 'name')}/>
                        <FormFeedback>{!validAddName ? 'Package name cannot be blank' : ''}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label for='packageLaboratory'>
                          <CardSubtitle>LABORATORY</CardSubtitle>
                        </Label>
                        <Input type='text' name='laboratory' id='packageLaboratory' placeholder='Laboratory' value={currentAddLaboratory} invalid={!validAddLaboratory} onChange={this.handleAddStringChange.bind(this, 'laboratory')}/>
                        <FormFeedback>{!validAddLaboratory ? 'Package laboratory cannot be blank' : ''}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label for='packageFormat'>
                          <CardSubtitle>FORMAT</CardSubtitle>
                        </Label>
                        <Input type='text' name='format' id='packageFormat' placeholder='Format' value={currentAddFormat} invalid={!validAddFormat} onChange={this.handleAddStringChange.bind(this, 'form')}/>
                        <FormFeedback>{!validAddFormat ? 'Package format cannot be blank' : ''}</FormFeedback>
                      </FormGroup>

                      <FormGroup>
                        <Label for='packageQuantity'>
                          <CardSubtitle>QUANTITY</CardSubtitle>
                        </Label>
                        <Row>
                          <Col sm={8}>
                            <Input type='number' step='0.01' name='quantity' id='packageQuantity' placeholder='Quantity' value={currentAddQuantity} invalid={!validAddQuantity} onChange={this.handleAddFloatChange.bind(this, 'quantity')}/>
                            <FormFeedback>{!validAddQuantity ? 'Package quantity must be bigger than zero and follow unit rules' : ''}</FormFeedback>
                          </Col>
                          <Col sm={4}>
                            <Input type='select' name='quantityUnit' id='packageQuantityUnit' value={currentAddQuantityUnit} onChange={this.handleAddStringChange.bind(this, 'quantityUnit')}>
                              <option>UNIT</option>
                              <option>GRAM</option>
                              <option>LITRE</option>
                            </Input>
                          </Col>
                        </Row>
                      </FormGroup>

                      <FormGroup>
                        <Label for='packageDescription'>
                          <CardSubtitle>DESCRIPTION</CardSubtitle>
                        </Label>
                        <Input type='textarea' name='description' id='packageDescription' value={currentAddDescription} invalid={!validAddDescription} onChange={this.handleAddStringChange.bind(this, 'description')}/>
                        <FormFeedback>{!validAddDescription ? 'Package description cannot be blank' : ''}</FormFeedback>
                      </FormGroup>
                    </Form>
                  </CardBody>
                </Card>
              </Col>
            </Row>
            <br/>
            <Row>
              <Col md={12}>
                <Alert className='medication-alert' color='info'>
                  <h4 className='alert-heading'><FontAwesomeIcon name='lightbulb'></FontAwesomeIcon>&nbsp;ANOTHER THING!</h4>
                  <p align='justify'>
                    After this step you will be able to edit other additional information related to this package chemical compounds and add new photos to our repository to assist our image recognition system.
                  </p>
                </Alert>
              </Col>
            </Row>
          </ModalBody>
          <ModalFooter>
            <Button color='primary' onClick={this.handleAcceptAdd}>Accept</Button>{' '}
            <Button color='secondary' onClick={this.handleHideAdd}>Cancel</Button>
          </ModalFooter>
        </Modal>
        {this.props.children}
      </div>
    )
  }
}
