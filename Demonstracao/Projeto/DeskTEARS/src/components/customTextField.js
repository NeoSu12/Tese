// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Icons
// ...

// Frontend components
import FormControl from '@material-ui/core/FormControl'
import FormHelperText from '@material-ui/core/FormHelperText'
import Input from '@material-ui/core/Input'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  form: {
    width: '100%'
  },

  inputRoot: {
    width: '100%',
    color: 'inherit',

    padding: 12,
    borderRadius: 4,
    backgroundColor: 'rgba(33, 150, 243, 0.1)',
    '&:hover': {
      backgroundColor: 'rgba(33, 150, 243, 0.25)'
    }
  },
  inputRootDisabled: {
    backgroundColor: '#EDEDED',
    '&:hover': {
      backgroundColor: '#EDEDED'
    }
  },

  inputMultiline: {
    width: '100%',
    color: 'inherit',

    backgroundColor: 'white',

    paddingLeft: 12,
    paddingTop: 18,
    paddingBottom: 18,

    border: '1px solid rgba(33, 150, 243, 0.1)',
    borderRadius: 4
  },
  inputMultilineDisabled: {
    border: '1px solid #BABABA'
  },

  inputInput: {
    color: '#2196F3'
  },

  inputInputDisabled: {
    color: '#BABABA'
  }
})

// ----- DEFINE TEXTFIELD

class CustomTextField extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({
      placeholder: nextProps.placeholder || '',

      value: nextProps.value || '',
      disabled: nextProps.disabled,
      valid: nextProps.valid,
      error: nextProps.error,
      multiline: nextProps.multiline,

      onChange: nextProps.onChange,
      onKeyClick: nextProps.onKeyClick,
      onKeyInput: nextProps.onKeyInput
    })
  }

  // ----- EVENT HANDLERS

  handleChangeValue(event) {
    const value = event.target.value
    const currentChangeEvent = this.state.onChange

    // Custom change value event
    if(currentChangeEvent)
      currentChangeEvent(value, event)
  }

  handleKeyClick(event) {
    // Custom key click event
    const currentKeyClickEvent = this.state.onKeyClick

    // Custom key click event
    if(currentKeyClickEvent)
      currentKeyClickEvent(event)
  }

  handleKeyInput(event) {
    // Custom key input event
    const currentKeyInputEvent = this.state.onKeyInput

    // Custom key input event
    if(currentKeyInputEvent)
      currentKeyInputEvent(event)
  }

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleKeyClick = this.handleKeyClick.bind(this)
    this.handleKeyInput = this.handleKeyInput.bind(this)
    this.handleChangeValue = this.handleChangeValue.bind(this)

    // ----- STATE

    this.state = {
      placeholder: this.props.placeholder || '',

      value: this.props.value || '',
      disabled: this.props.disabled,
      valid: this.props.valid,
      error: this.props.error,
      multiline: this.props.multiline,

      onChange: this.props.onChange,
      onKeyClick: this.props.onKeyClick,
      onKeyInput: this.props.onKeyInput
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props

    const isDisabled = this.state.disabled
    const isMultiline = this.state.multiline
    const isValid = this.state.valid

    const inputPlaceholder = this.state.placeholder
    const inputValue = this.state.value
    const inputError = !isValid ? this.state.error : ''

    const inputRootClasses = isDisabled ? ClassNames(classes.inputRoot, classes.inputRootDisabled) : classes.inputRoot
    const inputMultilineClasses = isDisabled ? ClassNames(classes.inputMultiline, classes.inputMultilineDisabled) : classes.inputMultiline
    const chosenRootClasses = isMultiline ? inputMultilineClasses : inputRootClasses

    const inputInputClasses = isDisabled ? ClassNames(classes.inputInput, classes.inputInputDisabled) : classes.inputInput

    return (
      <div>
        <FormControl className={classes.form} disabled={isDisabled} error={!isValid}>
          <Input
            classes={{
              root: chosenRootClasses,
              input: inputInputClasses,
            }}
            value={inputValue}
            placeholder={inputPlaceholder}
            onChange={this.handleChangeValue}
            onKeyPress={this.handleKeyInput}
            multiline={isMultiline}
            disableUnderline
          />
          <FormHelperText>{inputError}</FormHelperText>
        </FormControl>

        {this.props.children}
      </div>
    )
  }
}

// ----- EXPORT

CustomTextField.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomTextField)
