// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Icons
import SearchIcon from '@material-ui/icons/Search'
import CloseButton from '@material-ui/icons/Close'

// Frontend components
import FormControl from '@material-ui/core/FormControl'
import Grid from '@material-ui/core/Grid'
import Input from '@material-ui/core/Input'
import IconButton from '@material-ui/core/IconButton'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  form: {
    display: 'flex',
    height: 36,

    marginRight: 16
  },

  search: {
    width: '100%',
    position: 'relative',
    marginLeft: 0,

    borderRadius: 4,
    backgroundColor: 'rgba(33, 150, 243, 0.1)',
    '&:hover': {
      backgroundColor: 'rgba(33, 150, 243, 0.25)'
    }
  },
  searchDisabled: {
    backgroundColor: '#EDEDED',
    '&:hover': {
      backgroundColor: '#EDEDED'
    }
  },

  searchIcon: {
    color: '#2196F3'
  },
  searchIconDisabled: {
    color: '#BABABA'
  },

  searchColumn: {

  },
  columnLeft: {
    textAlign: 'left'
  },
  columnCenter: {
    textAlign: 'center'
  },
  columnRight: {
    textAlign: 'right'
  },

  inputRoot: {
    width: '100%',
    color: 'inherit'
  },
  inputInput: {
    width: '100%',
    padding: 12,

    color: '#2196F3'
  },

  inputInputDisabled: {
    color: '#BABABA'
  }
})

// ----- DEFINE SEARCH

class CustomSearch extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.setState({
      label: nextProps.label || '',

      value: nextProps.value,
      disabled: nextProps.disabled,

      onChange: nextProps.onChange,
      onKeyClick: nextProps.onKeyClick,
      onKeyInput: nextProps.onKeyInput,
    })
  }

  // ----- EVENT HANDLERS

  handleChangeValue(event) {
    const input = event.target
    const inputValue = input.value
    const inputStart = input.selectionStart
    const inputEnd = input.selectionEnd

    const upperValue = inputValue.toUpperCase()
    const currentChangeEvent = this.state.onChange

    // Custom change value event
    if(currentChangeEvent)
      currentChangeEvent(upperValue)

    // Keep carrier index
    input.setSelectionRange(inputStart, inputEnd)
  }

  handleKeyClick(event) {
    // Custom key click event
    const currentKeyClickEvent = this.state.onKeyClick

    // Custom key click event
    if(currentKeyClickEvent)
      currentKeyClickEvent(event)
  }

  handleKeyClear(event) {
    // Custom key clear event
    const currentKeyClearEvent = this.state.onKeyClear

    // Custom key clear event
    if(currentKeyClearEvent)
      currentKeyClearEvent(event)
  }

  handleKeyInput(event) {
    // Custom key input event
    const currentKeyInputEvent = this.state.onKeyInput

    // Custom key input event
    if(currentKeyInputEvent)
      currentKeyInputEvent(event)
  }

  // ----- METHODS
  // ...

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleKeyClick = this.handleKeyClick.bind(this)
    this.handleKeyClear = this.handleKeyClear.bind(this)
    this.handleKeyInput = this.handleKeyInput.bind(this)
    this.handleChangeValue = this.handleChangeValue.bind(this)

    // ----- STATE

    this.state = {
      label: this.props.label || '',

      value: this.props.value || '',
      disabled: false,

      onChange: this.props.onChange,
      onKeyClick: this.props.onKeyClick,
      onKeyClear: this.props.onKeyClear,
      onKeyInput: this.props.onKeyInput
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props

    const inputLabel = this.state.label
    const inputValue = this.state.value

    const isDisabled = this.state.disabled

    const searchIcon = isDisabled ? <CloseButton /> : <SearchIcon />
    const searhFunction = isDisabled ? this.handleKeyClear : this.handleKeyClick
    const searchClasses = isDisabled ? ClassNames(classes.search, classes.searchDisabled) : classes.search
    const searchIconClasses = isDisabled ? ClassNames(classes.searchIcon, classes.searchIconDisabled) : classes.searchIcon
    const inputClasses = isDisabled ? ClassNames(classes.inputInput, classes.inputInputDisabled) : classes.inputInput

    return (
      <div>
        <FormControl className={classes.form} disabled={isDisabled}>
          <Grid container direction='row' alignItems='center' className={searchClasses}>
            <Grid item xs={8} className={ClassNames(classes.searchColumn, classes.columnLeft)}>
              <Input
                classes={{
                  root: classes.inputRoot,
                  input: inputClasses,
                }}
                value={inputValue}
                placeholder={inputLabel}
                onChange={this.handleChangeValue}
                onKeyPress={this.handleKeyInput}
                disableUnderline
              />
            </Grid>
            <Grid item xs={4} className={ClassNames(classes.searchColumn, classes.columnRight)}>
              <IconButton
                className={searchIconClasses}
                onClick={searhFunction}
              >
                {searchIcon}
              </IconButton>
            </Grid>
          </Grid>
        </FormControl>

        {this.props.children}
      </div>
    )
  }
}

// ----- EXPORT

CustomSearch.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomSearch)
