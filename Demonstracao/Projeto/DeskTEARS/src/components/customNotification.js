// ----- IMPORTS

// React
import React from 'react'
import uniqid from 'uniqid'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Custom components
import CustomSnackbar from './customSnackbar'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  container: {
    display: 'flex',
    position: 'fixed',
    right: 0,
    bottom: 0,

    flexDirection: 'column',
    overflow: 'hidden',
    zIndex: 1301
  }
})

// ----- DEFINE NOTIFICATION

class CustomNotification extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // Reference self object
    this.props.onRef(this)
  }

  componentWillUnmount() {
    // Unreference self object
    this.props.onRef(undefined)
  }

  componentWillReceiveProps(nextProps) {
    // ...
  }

  // ----- EVENT HANDLERS
  // ...

  // ----- METHODS

  addNotification(title, message, variation, hideDuration) {
    let currentSnacks = this.state.snacks

    // Add value
    const id = uniqid()
    const value = <CustomSnackbar
        key={id}
        id={id}
        title={title}
        message={message}
        variation={variation}
        hideDuration={hideDuration}
      />

    currentSnacks.push(value)

    // Update state
    this.setState({
      snacks: currentSnacks
    })
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS
    // ...

    // ----- STATE
    this.state = {
      snacks: []
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props
    const currentSnackbars = this.state.snacks

    return (
      <div className={classes.container}>
        {currentSnackbars}
      </div>
    )
  }
}

// ----- EXPORT

CustomNotification.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomNotification)
