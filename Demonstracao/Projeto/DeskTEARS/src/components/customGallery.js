// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Icons
import AddIcon from '@material-ui/icons/Add'

import EyeIcon from '@material-ui/icons/Visibility'
import ClearIcon from '@material-ui/icons/Clear'

import CheckIcon from '@material-ui/icons/CheckCircle'
import ErrorIcon from '@material-ui/icons/Error'
import WarningIcon from '@material-ui/icons/Warning'

// Frontend components
import GridList from '@material-ui/core/GridList'
import GridListTile from '@material-ui/core/GridListTile'

import IconButton from '@material-ui/core/IconButton'
import CircularProgress from '@material-ui/core/CircularProgress'
import Badge from '@material-ui/core/Badge'

// ----- CUSTOM

// Custom components
import CustomGallerySelect from './customGallerySelect'

// Custom assets
// ...

// Custom utils
import { sleep, fileToBase64 } from '../utils'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  container: {

  },

  grid: {
    overflowY: 'hidden',
    justifyContent: 'space-around',
    alignItems: 'center'
  },

  cell: {
    margin: 12,

    border: '1px solid #4CAF50',
    borderRadius: 4,

    transition: '0.1s',
    '&:hover': {
      transform: 'scale(1.05)'
    },
    '&:active': {
      transform: 'scale(1)'
    }
  },
  cellTile: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center'
  },
  cellImage: {
    left: '50%',
    height: '100%',
    position: 'relative',
    transform: 'translateX(-50%)',
    opacity: 0.25
  },
  cellIcon: {

  },
  iconInvalid: {
    color: '#FF1744',
    width: '75%',
    height: '75%',
    opacity: 0.25
  },
  cellAdd: {
    borderColor: '#4CAF50',
    borderStyle: 'dashed'
  },
  cellLoad: {
    borderColor: '#2196F3',
    borderStyle: 'dashed'
  },
  cellWarning: {
    borderColor: '#FF9800',
    borderStyle: 'dashed'
  },
  cellInvalid: {
    borderColor: '#FF1744',
    borderStyle: 'dashed'
  },
  cellRemove: {
    borderColor: '#FF1744',
    borderStyle: 'dashed'
  },

  cellItem: {
    display: 'flex',

    width: '100%',
    height: '100%',

    alignItems: 'center',
    justifyContent: 'center',

    cursor: 'Pointer'
  },
  itemAdd: {
    width: 56,
    height: 56,

    border: '1px solid #4CAF50',
    borderRadius: '50%',

    backgroundColor: 'transparent',
    color: '#4CAF50',

    '&:hover': {
      backgroundColor: '#4CAF50',
      color: 'white'
    }
  },
  itemLoad: {
    width: 56,
    height: 56,

    cursor: 'default'
  },
  itemInvalid: {

  },
  itemRemove: {
    width: 56,
    height: 56,

    cursor: 'default'
  },

  cellProgress: {

  },
  progressLoad: {
    color: '#2196F3'
  },
  progressRemove: {
    color: '#FF1744'
  },

  overlay: {
    opacity: 0,
    display: 'flex',
    position: 'absolute',
    width: '100%',
    height: '100%',
    zIndex: 2,

    backgroundColor: 'rgba(0, 0, 0, 0.3)',

    justifyContent: 'center',
    alignItems: 'center',

    transition: '0.2s',
    '&:hover': {
      opacity: 1
    }
  },
  overlayItem: {
    display: 'flex',
    width: '100%',
    height: '100%',

    alignItems: 'center',
    justifyContent: 'center',

    '&:hover': {
      backgroundColor: 'rgba(0, 0, 0, 0.15)'
    }
  },
  overlayButton: {

  },
  halfItem: {
    width: '50%'
  },
  fullItem: {
    width: '100%'
  },
  overlayIcon: {
    color: 'white'
  },

  warningIcon: {
    position: 'absolute',
    right: 'calc(50% - 32px)',
    top: 'calc(50% - 32px)',
    zIndex: 1,
    opacity: 0.5,

    fontSize: 64
  },

  warningValid: {
    color: '#4CAF50'
  },
  warningInvalid: {
    color: '#FF9800'
  },

  badge: {
    position: 'absolute',
    zIndex: 1
  },

  badgeIcon: {
    fontSize: 64
  },

  badgeCount: {
    position: 'absolute',
    width: 30,
    height: 30,
    top: 37,
    right: -5,

    border: '1px solid #4CAF50',
    backgroundColor: 'white',

    color: '#4CAF50',
    fontSize: 16
  },

  explorer: {
    display: 'none'
  }
})

// ----- DEFINE GALLERY

class CustomGallery extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Update reference
    this.updateReference(this)

    // Update selected object thumbnails
    await this.updateInformation()
  }

  componentWillUnmount() {
    // Update reference
    this.updateReference(undefined)
  }

  async componentWillReceiveProps(nextProps) {
    // ----- STATE
    this.setState({
      notification: nextProps.notification,
      service: nextProps.service,

      selectedModel: nextProps.selected
    },
    async () => {
      // Update selected object thumbnails
      await this.updateInformation()
    })
  }

  // ----- EVENT HANDLERS

  async handleRefresh(event) {
    await this.fetchThumbnails()
  }

  handleAdd(event) {
    // Open file explorer
    this.explorer.current.click()
  }

  async handleFileChange(event) {
    let files = this.explorer.current.files

    let index = 0
    let size = files ? files.length : 0

    // Pick file from input
		if(size > 0) {
      // Show loading file
      await this.showLoadingPhoto()

      for(index = 0; index < size; index ++) {
        let file = files[index]

        if(file) {
  				// Load image from file
  				let base64 = await fileToBase64(file)

          // Upload photo
          await this.addPhoto(base64)
  			}
      }

      // Hide loading photo
      this.hideLoadingPhoto()

      // Update available photos
      await this.fetchThumbnails()
		}

    // Clear input files
    this.explorer.current.value = ''
  }

  handleSelect(key, event) {
    this.setState({
      selectedPhoto: key
    })
  }

  async handleUnselect(event) {
    this.setState({
      selectedPhoto: ''
    })

    // Update available photos
    await this.fetchThumbnails()
  }

  async handleRemove(key, event) {
    let currentRemove = this.state.remove

    if(!currentRemove) {
      await this.removePhoto(key)
    }
    else {
      // Notification
      this.state.notification('Gallery', `Server is still handling photo '${currentRemove}' removal, please stand by for a few seconds`, 'warning')
    }
  }

  // ----- METHODS

  updateReference(value) {
    const currentReferenceEvent = this.state.ref

    // Custom reference event
    if(currentReferenceEvent)
      currentReferenceEvent(value)
  }

  async updateInformation() {
    const currentOldSelectedModel = this.state.oldSelectedModel
    const currentSelectedModel = this.state.selectedModel

    if(currentOldSelectedModel != currentSelectedModel) {
      this.setState({
        oldSelectedModel: currentSelectedModel
      })

      if(currentSelectedModel) {
        // Fetch selected object thumbnails
        await this.fetchThumbnails()
      }
    }
  }

  async showLoadingPhoto() {
    this.setState({
      loading: true
    })

    // Sleep for 1 second
    await sleep(1000)
  }

  hideLoadingPhoto() {
    this.setState({
      loading: false
    })
  }

  async showRemovePhoto(key) {
    this.setState({
      remove: key
    })

    // Sleep for 1 second
    await sleep(1000)
  }

  hideRemovePhoto() {
    this.setState({
      remove: ''
    })
  }

  // ---- COMMUNICATIONS

  // TODO - ADD PAGE CONTROL!
  async fetchThumbnails() {
    const currentSelectedModel = this.state.selectedModel

    // Show loading file
    await this.showLoadingPhoto()

    try {
      const filter = {
        limit: 100
      }

      const availableThumbnails = await this.state.service.getObjectThumbnails(currentSelectedModel, filter)
      const formattedThumbnails = availableThumbnails.reduce((result, value) => {
          result[value.id] = value
          return result
      }, {})

      this.setState({
        values: formattedThumbnails
      })

      // Notification
      // this.state.notification('Gallery', `Fetched model '${currentSelectedModel}' thumbnails`, 'success')
    } catch(error) {
      this.setState({
        values: ''
      })

      // Notification
      this.state.notification('Gallery', `Could not retrieve model '${currentSelectedModel}' thumbnails: ${error.message}`, 'error')
    }

    // Hide loading file
    this.hideLoadingPhoto()
  }

  async addPhoto(base64) {
    const currentSelectedModel = this.state.selectedModel

    try {
      // Add new photo
      await this.state.service.addObjectPhoto(currentSelectedModel, base64)

      // Notification
      this.state.notification('Gallery', `Added new photo to model '${currentSelectedModel}'`, 'success')
    } catch(error) {
      this.state.notification('Gallery', `Could not add new photo to model '${currentSelectedModel}': ${error.message}`, 'error')
    }
  }

  async removePhoto(photoId) {
    const currentSelectedModel = this.state.selectedModel

    // Show remove file
    await this.showRemovePhoto(photoId)

    try {
      // Remove photo
      await this.state.service.removePhoto(photoId)

      // Notification
      this.state.notification('Gallery', `Removed photo '${photoId}' from model '${currentSelectedModel}'`, 'success')
    } catch(error) {
      this.state.notification('Gallery', `Could not remove photo '${photoId}' from model '${currentSelectedModel}': ${error.message}`, 'error')
    }

    // Update available photos
    await this.fetchThumbnails()

    // Hide loading photo
    this.hideRemovePhoto(photoId)
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleAdd = this.handleAdd.bind(this)
    this.handleFileChange = this.handleFileChange.bind(this)

    this.handleSelect = this.handleSelect.bind(this)
    this.handleUnselect = this.handleUnselect.bind(this)
    this.handleRemove = this.handleRemove.bind(this)

    // ----- STATE

    this.state = {
      notification: this.props.notification,
      service: this.props.service,

      oldSelectedModel: '',
      selectedModel: this.props.selected,
      selectedPhoto: '',

      loading: false,
      remove: '',
      values: ''
    }

    // ----- REFERENCES

    this.explorer = React.createRef()
    this.gallerySelect = React.createRef()
  }

  render() {
    const { classes } = this.props

    const currentValues = this.state.values
    const currentKeys = Object.keys(currentValues)
    const currentSize = currentKeys.length
    const currentLoading = this.state.loading
    const currentRemove = this.state.remove

    const fixWidth = {
      width: 200
    }

    // Format list
    const contentAdd = <GridListTile
        key='addPhoto'
        style={fixWidth}
        className={ClassNames(classes.cell, classes.cellAdd)}
        classes={{
          tile: classes.cellTile
        }}
      >
      <IconButton
        className={ClassNames(classes.cellItem, classes.itemAdd)}
        onClick={this.handleAdd}
      >
        <AddIcon fontSize={'inherit'}/>
      </IconButton>
    </GridListTile>

    const contentLoad = <GridListTile
        key='loadingPhoto'
        style={fixWidth}
        className={ClassNames(classes.cell, classes.cellLoad)}
        classes={{
          tile: classes.cellTile
        }}
      >
      <div className={ClassNames(classes.cellItem, classes.itemLoad)}>
        <CircularProgress size={56} thickness={2} className={ClassNames(classes.cellProgress, classes.progressLoad)} />
      </div>
    </GridListTile>
    const contentHeader = this.state.loading ? contentLoad : contentAdd

    let content = ''
    if(currentSize > 0) {
      content = currentKeys.map(key => {
        const value = currentValues[key]

        const id = value.id
        const thumbnail = 'data:image/png;base64,' + value.thumbnail
        const annotations = value.annotations
        const annotationsSize = annotations ? annotations.length : 0

        const isValid = Boolean(value.thumbnail)
        const isAnnotated = annotations && annotations.length > 0
        const toRemove = currentRemove && currentRemove === id

        const warningIcon = isAnnotated ? <Badge badgeContent={annotationsSize} className={classes.badge} classes={{ badge: classes.badgeCount }}>
          <CheckIcon fontSize={'inherit'} className={ClassNames(classes.badgeIcon, classes.warningValid)}/>
        </Badge> : <ErrorIcon fontSize={'inherit'} className={ClassNames(classes.warningIcon, classes.warningInvalid)}/>

        const validCellClass = isValid ? '': classes.cellInvalid
        const warningCellClass = isAnnotated ? '' : classes.cellWarning

        const contentValid = <div className={classes.cellItem}>
          {/* WARNING ICON */}
          {warningIcon}

          {/* OVERLAY */}
          <div className={classes.overlay}>
            <div className={ClassNames(classes.overlayItem, classes.halfItem)}>
              <IconButton
                className={classes.overlayButton}
                onClick={this.handleSelect.bind(this, id)}
              >
                <EyeIcon fontSize={'inherit'} className={classes.overlayIcon}/>
              </IconButton>
            </div>

            <div className={ClassNames(classes.overlayItem, classes.halfItem)}>
              <IconButton
                className={classes.overlayButton}
                onClick={this.handleRemove.bind(this, id)}
              >
                <ClearIcon fontSize={'inherit'} className={classes.overlayIcon}/>
              </IconButton>
            </div>
          </div>

          {/* IMAGE */}
          <img id={id} src={thumbnail} className={classes.cellImage}/>
        </div>

        const contentInvalid = <div className={ClassNames(classes.cellItem, classes.itemInvalid)}>
          {/* OVERLAY */}
          <div className={classes.overlay}>
            <div className={ClassNames(classes.overlayItem, classes.halfItem)}>
              <IconButton
                className={classes.overlayButton}
                onClick={this.handleRemove.bind(this, id)}
              >
                <ClearIcon fontSize={'inherit'} className={classes.overlayIcon}/>
              </IconButton>
            </div>
          </div>

          <WarningIcon fontSize={'inherit'} className={ClassNames(classes.cellIcon, classes.iconInvalid)}/>
        </div>

        const contentRemove = <div className={ClassNames(classes.cellItem, classes.itemRemove)}>
          <CircularProgress size={56} thickness={2} className={ClassNames(classes.cellProgress, classes.progressRemove)} />
        </div>

        // Check what content to use
        let content = isValid ? contentValid : contentInvalid
        let contentClass = isValid ? '' : classes.cellInvalid

        // To remove ?
        content = toRemove ? contentRemove : content
        contentClass = toRemove ? classes.cellRemove : contentClass

        return <GridListTile
            key={id}
            style={fixWidth}
            className={ClassNames(classes.cell, contentClass, warningCellClass, validCellClass)}
            classes={{
              tile: classes.cellTile
            }}
          >
            {content}
          </GridListTile>
      })
    }

    // Format selected
    const currentSelectedPhoto = this.state.selectedPhoto

    return(
      <div className={classes.container}>
        {/* VALUES */}
        <GridList cellHeight={200} cols={5} spacing={0} className={classes.grid}>
          {contentHeader}
          {content}
        </GridList>

        {/* SELECTED */}
        <CustomGallerySelect ref={ref => this.gallerySelect = ref} service={this.state.service} notification={this.state.notification} selected={currentSelectedPhoto} onClose={this.handleUnselect}/>

        <input ref={this.explorer} className={classes.explorer} type='file' accept='image/*' multiple='multiple' onChange={this.handleFileChange}></input>
      </div>
    )
  }
}

// ----- EXPORT

CustomGallery.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomGallery)
