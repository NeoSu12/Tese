// ----- IMPORTS

// React
import React from 'react'
import ReactDOM from 'react-dom'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

// Icons
import DeleteIcon from '@material-ui/icons/Delete'
import SaveIcon from '@material-ui/icons/Save'
import CloseIcon from '@material-ui/icons/Close'
import WarningIcon from '@material-ui/icons/Warning'

// Frontend components
import Grid from '@material-ui/core/Grid'
import Dialog from '@material-ui/core/Dialog'
import DialogContentText from '@material-ui/core/DialogContentText'
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import Button from '@material-ui/core/Button'
import IconButton from '@material-ui/core/IconButton'
import Slide from '@material-ui/core/Slide'
import Divider from '@material-ui/core/Divider'

import Card from '@material-ui/core/Card'
import CardContent from '@material-ui/core/CardContent'
import CardMedia from '@material-ui/core/CardMedia'

import CircularProgress from '@material-ui/core/CircularProgress'

// ----- CUSTOM

// Custom components
import CustomDialog from './customDialog'
import CustomTextField from './customTextField'
import CustomGallery from './customGallery'

// Custom assets
// ...

// Custom utils
import { sleep } from '../utils'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  // ----- BASE
  container: {
    backgroundColor: '#FAFAFA'
  },

  // ----- BAR
  delete: {
    position: 'fixed',
    left: 24,

    color: 'white',
    backgroundColor: '#FF1744',

    '&:hover': {
      backgroundColor: '#B2102F'
    }
  },
  deleteIcon: {
    color: 'white',
    marginRight: 12
  },

  save: {
    position: 'fixed',
    right: 72,

    color: 'white'
  },
  close: {
    position: 'fixed',
    right: 24,

    color: 'white'
  },

  // ----- CONTENT

  // Progress
  progressContainer: {
    display: 'flex',

    width: '100%',
    height: '100%',

    alignItems: 'center',
    justifyContent: 'center'
  },
  progressIcon: {

  },

  // Data
  dataContainer: {
    padding: 24,

    overflowY: 'auto',
    overflowX: 'hidden'
  },
  dataRow: {
    width: '100%',
    maxWidth: '100%',
    marginBottom: 24
  },

  informationContainer: {

  },
  galleryContainer: {

  },

  titleContainer: {
    height: 72,

    marginBottom: 24,
    padding: 24,

    backgroundColor: 'white',

    border: '1px solid #2196F3',
    borderLeftWidth: 5,
    borderRadius: 4
  },
  titleValue: {
    color: '#2196F3'
  },

  data: {
    marginTop: 24,
    padding: 24,
    backgroundColor: 'white',

    border: '1px solid rgba(0, 0, 0, 0.54)',
    borderRadius: 4
  },
  dataTitle: {
    display: 'flex',
    alignItems: 'center',
    color: 'rgba(0, 0, 0, 0.54)',
    marginTop: 12,
    marginBottom: 12,
    fontWeight: 'bold'
  },
  dataIcon: {
    marginRight: 12,
    fontSize: 32
  },
  dataText: {
    color: 'rgba(0, 0, 0, 0.54)',
    textAlign: 'justify'
  },
  dataTextField: {

  },
  dataDivider: {
    backgroundColor: 'rgba(0, 0, 0, 0.54)',
    marginTop: 12,
    marginBottom: 12
  },

  info: {
    marginTop: 24,
    padding: 24,
    backgroundColor: 'white',

    border: '1px solid rgba(0, 0, 0, 0.54)',
    borderRadius: 4
  },
  infoTitle: {
    display: 'flex',
    alignItems: 'center',
    color: 'rgba(0, 0, 0, 0.54)',
    marginBottom: 12,
    fontWeight: 'bold'
  },
  infoIcon: {
    marginRight: 12,
    fontSize: 32
  },
  infoText: {
    color: 'rgba(0, 0, 0, 0.54)',
    textAlign: 'justify'
  },
  infoTextMarginBottom: {
    marginBottom: 12
  },
  infoDivider: {
    backgroundColor: 'rgba(0, 0, 0, 0.54)',
    marginTop: 12,
    marginBottom: 12
  },

  alert: {
    marginTop: 24,
    padding: 24,
    backgroundColor: 'rgba(183, 0, 0, 0.1)',
    border: '1px solid rgba(183, 0, 0, 0.5)'
  },
  alertTitle: {
    display: 'flex',
    alignItems: 'center',
    color: '#B70000',
    marginBottom: 12,
    fontWeight: 'bold'
  },
  alertIcon: {
    marginRight: 12,
    fontSize: 32
  },
  alertText: {
    color: '#B70000',
    textAlign: 'justify'
  },
  alertDivider: {
    backgroundColor: 'rgba(183, 0, 0, 0.5)',
    marginTop: 12,
    marginBottom: 12
  }
})

// ----- DEFINE SLIDER

class CustomSlider extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  async componentDidMount() {
    // Update reference
    this.updateReference(this)

    // Update selected object information
    await this.updateInformation()
  }

  componentWillUnmount() {
    // Update reference
    this.updateReference(undefined)
  }

  async componentWillReceiveProps(nextProps) {
    this.setState({
      ref: nextProps.ref,
      notification: nextProps.notification,
      service: nextProps.service,

      selected: nextProps.selected,
      onClose: nextProps.onClose
    },
    async () => {
      // Update selected object information
      await this.updateInformation()
    })
  }

  // ----- EVENT HANDLERS

  handleClose(event) {
    const currentChanged = this.state.changed

    if(currentChanged) {
      // Ask user if he wants to save
      this.setState({
        save: true
      })
    }
    else {
      const currentCloseEvent = this.state.onClose

      // Customn close event
      if(currentCloseEvent)
        currentCloseEvent(event)
    }
  }

  async handleSave(close, event) {
    // Update object
    await this.updateObject(close)
  }

  async handleAcceptSave(event) {
    // Save object
    await this.handleSave(true, event)
  }

  handleCancelSave(event) {
    const currentCloseEvent = this.state.onClose

    // Customn close event
    if(currentCloseEvent)
      currentCloseEvent(event)
  }

  handleRemove(event) {
    this.setState({
      remove: true
    })
  }

  async handleAcceptRemove(event) {
    // Remove object
    await this.removeObject()

    // Close
    this.handleClose()
  }

  handleCancelRemove(event) {
    this.setState({
      remove: false
    })
  }

  handleStringChange(key, value, event) {
    const input = event.target
    const inputValue = value
    const inputStart = input.selectionStart
    const inputEnd = input.selectionEnd

    let currentValue = this.state.value

    // Define new key value
    currentValue[key] = inputValue.toUpperCase()

    // Update input values
    this.setState({
      changed: true,
      value: currentValue
    },
    () => input.setSelectionRange(inputStart, inputEnd)
    )
  }

  // ----- METHODS

  transition(props) {
    return <Slide direction='up' {...props} />
  }

  async showLoading() {
    this.setState({
      loading: true
    })

    // Sleep for 1 second
    await sleep(1000)
  }

  hideLoading() {
    this.setState({
      loading: false
    })
  }

  updateReference(value) {
    const currentReferenceEvent = this.state.ref

    // Custom reference event
    if(currentReferenceEvent)
      currentReferenceEvent(value)
  }

  async updateInformation() {
    const currentOldSelected = this.state.oldSelected
    const currentSelected = this.state.selected

    if(currentOldSelected != currentSelected) {
      this.setState({
        oldSelected: currentSelected
      })

      if(currentSelected) {
        // Fetch selected object
        await this.fetchObject()
      }
    }
  }

  validateObject() {
    const currentValue = this.state.value
    let invalidValues = ['label', 'description']
    let removeIndex = -1

    if(currentValue) {
      // Validate label
      if(currentValue.label && currentValue.label.length > 0) {
        removeIndex = invalidValues.indexOf('label')

        if(removeIndex > -1)
          invalidValues.splice(removeIndex, 1)
      }

      // Validate description
      if(currentValue.description && currentValue.description.length > 0) {
        removeIndex = invalidValues.indexOf('description')

        if(removeIndex > -1)
          invalidValues.splice(removeIndex, 1)
      }
    }

    this.setState({
      invalid: invalidValues
    })

    return invalidValues.length === 0
  }

  // ----- COMMUNICATIONS

  async fetchObject() {
    const currentSelected = this.state.selected

    if(currentSelected) {
      // this.state.notification('Model', `User selected the ${currentSelected} object`, 'information')

      // Toggle loading
      await this.showLoading()

      try {
        // Fetch object
        const availableObject = await this.state.service.getObject(currentSelected)

        this.setState({
          oldValue: Object.assign({}, availableObject),
          value: Object.assign({}, availableObject),
          changed: false,
          save: false
        })
      } catch(error) {
        this.setState({
          oldValue: '',
          value: '',
          changed: false,
          save: false
        })

        // Notification
        this.state.notification('Models', `Could not retrieve available platform object: ${error.message}`, 'error')
      }

      // Toggle loading
      this.hideLoading()
    }
  }

  async removeObject() {
    const currentSelected = this.state.selected

    if(currentSelected) {
      this.state.notification('Models', `User is trying to remove ${currentSelected} object`, 'information')

      // Toggle loading
      await this.showLoading()

      try {
        // Remove object
        await this.state.service.removeObject(currentSelected)

        this.setState({
          selected: '',
          oldValue: '',
          value: ''
        })
      } catch(error) {
        // Notification
        this.state.notification('Models', `Could not remove platform object: ${error.message}`, 'error')
      }

      // Toggle loading
      // this.hideLoading()
    }
  }

  async updateObject(close) {
    const currentSelected = this.state.selected
    const currentValue = this.state.value

    const validation = this.validateObject()

    if(currentSelected && validation) {
      this.state.notification('Information', `User is trying to update ${currentSelected} object`, 'information')

      // Toggle loading
      await this.showLoading()

      try {
        // Update object
        const updatedValue = await this.state.service.updateObject(currentSelected, currentValue)

        this.setState({
          oldValue: Object.assign({}, updatedValue),
          value: Object.assign({}, updatedValue),
          changed: false,
          save: false
        })

        // Close window
        if(close) {
          // Close window
          const currentCloseEvent = this.state.onClose

          // Customn close event
          if(currentCloseEvent)
            currentCloseEvent(event)
        }
      } catch(error) {
        this.setState({
          oldValue: '',
          value: '',
          changed: false,
          save: false
        })

        // Notification
        this.state.notification('Models', `Could not update platform object: ${error.message}`, 'error')
      }

      // Toggle loading
      this.hideLoading()
    }
    else {
      this.state.notification('Models', `Please check for any errors in your inputed values`, 'warning')
      this.setState({
        save: false
      })
    }
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleClose = this.handleClose.bind(this)

    this.handleRemove = this.handleRemove.bind(this)
    this.handleAcceptRemove = this.handleAcceptRemove.bind(this)
    this.handleCancelRemove = this.handleCancelRemove.bind(this)

    this.handleSave = this.handleSave.bind(this)
    this.handleAcceptSave = this.handleAcceptSave.bind(this)
    this.handleCancelSave = this.handleCancelSave.bind(this)

    this.handleStringChange = this.handleStringChange.bind(this)

    // ----- STATE

    this.state = {
      ref: this.props.ref,
      notification: this.props.notification,
      service: this.props.service,

      oldSelected: '',
      selected: this.props.selected,

      oldValue: '',
      value: '',

      loading: false,
      remove: false,
      changed: false,

      save: false,
      invalid: [],

      onClose: this.props.onClose
    }

    // ----- REFERENCES
    this.gallery = React.createRef()
  }

  renderWaiting() {
    const { classes } = this.props

    const currentOpen = Boolean(this.state.selected)

    return (
      <div>
        <Dialog
          fullScreen
          open={currentOpen}
          onClose={this.handleClose}
          TransitionComponent={this.transition}
          classes={{
            paper: classes.container
          }}
        >
          {/* APP BAR */}
          <AppBar position='static'>
            <Toolbar>
              {/* CLOSE */}
              <IconButton
                className={classes.close}
                onClick={this.handleClose}
              >
                <CloseIcon />
              </IconButton>
            </Toolbar>
          </AppBar>

          {/* CONTAINER */}
          <div className={classes.progressContainer}>
            <CircularProgress size={128} thickness={2} className={classes.progressIcon} />
          </div>
        </Dialog>
      </div>
    )
  }

  renderValue() {
    const { classes } = this.props

    const currentOpen = Boolean(this.state.selected)

    const currentSelected = this.state.selected
    const currentOldValue = this.state.oldValue
    const currentValue = this.state.value
    const currentChanged = this.state.changed
    const currentRemove = this.state.remove
    const currentSave = this.state.save

    const currentInvalid = this.state.invalid
    const validLabel = currentInvalid.indexOf('label') < 0
    const validDescription = currentInvalid.indexOf('description') < 0

    const saveIcon = currentChanged ?
    <IconButton
      className={classes.save}
      onClick={this.handleSave.bind(this, false)}
    >
      <SaveIcon />
    </IconButton> : ''

    // Format object information ...
    const id = currentValue.id

    // Current
    const label = currentValue.label
    const description = currentValue.description

    // Old
    const labelOld = currentOldValue.label
    const descriptionOld = currentOldValue.description

    const informationContent = <div className={classes.informationContainer}>
      {/* TITLE */}
      <div className={classes.titleContainer}>
        <Typography variant='title' className={classes.titleValue}>
          INFORMATION
        </Typography>
      </div>

      {/* TEXT FIELDS */}
      <div className={classes.data}>
        <Typography variant='headline' component='h4' className={classes.dataTitle}>
          ID
        </Typography>
        <CustomTextField
          placeholder={'Identity'}
          value={id}
          disabled={true}
          className={classes.dataTextField}/>
        <Typography variant='headline' component='h4' className={classes.dataTitle}>
          LABEL
        </Typography>
        <CustomTextField
          placeholder={'Label'}
          value={label}
          valid={validLabel}
          error={"Label value can't be left blank"}
          onChange={this.handleStringChange.bind(this, 'label')}
          className={classes.dataTextField}/>
        <Typography variant='headline' component='h4' className={classes.dataTitle}>
          DESCRIPTION
        </Typography>
        <CustomTextField
          placeholder={'Description'}
          value={description}
          multiline={true}
          valid={validDescription}
          error={"Description value can't be left blank"}
          onChange={this.handleStringChange.bind(this, 'description')}
          className={classes.dataTextField}/>
      </div>
    </div>

    // Format object photos ...
    const galleryContent = <div className={classes.galleryContainer}>
      {/* TITLE */}
      <div className={classes.titleContainer}>
        <Typography variant='title' className={classes.titleValue}>
          GALLERY
        </Typography>
      </div>

      {/* IMAGES */}
      <CustomGallery ref={ref => this.gallery = ref} service={this.state.service} notification={this.state.notification} selected={currentSelected}/>
    </div>

    // Format remove content
    const removeContent = currentRemove ? <CustomDialog title={'Remove Dialog'} acceptTitle={'Remove'} rejectTitle={'Cancel'} onAccept={this.handleAcceptRemove} onCancel={this.handleCancelRemove}>
      <DialogContentText>
        Are you sure you want to remove the following object model from our object recognition platform database?
        <div className={classes.info}>
          <Typography variant='headline' component='h4' className={classes.infoTitle}>
            ID
          </Typography>
          <Typography variant='subheading' className={classes.infoText}>
            {id}
          </Typography>
          <Divider className={classes.infoDivider}/>
          <Typography variant='headline' component='h4' className={classes.infoTitle}>
            LABEL
          </Typography>
          <Typography variant='subheading' className={classes.infoText}>
            {label}
          </Typography>
        </div>
        <div className={classes.alert}>
          <Typography variant='headline' component='h4' className={classes.alertTitle}>
            <WarningIcon className={classes.alertIcon}/>HOLD ON!
          </Typography>
          <Typography variant='subheading' className={classes.alertText}>
            Before accepting any changes, please remember that <b>all information related to this object will become unavailable</b> from now on, and it might affect your experience when you try to use our platform.
          </Typography>
          <Divider className={classes.alertDivider}/>
          <Typography variant='subheading' className={classes.alertText}>
            Additionally, by continuing with this process you are confirming and acknowledging that <b>all related photos and other sensible information referring to this object model will remain saved in our database</b> and that we have the power to use and manipulate this data for other possible future purposes.
          </Typography>
        </div>
      </DialogContentText>
    </CustomDialog> : ''

    // Format save content
    const equalLabels = label == labelOld
    const equalDescriptions = description == descriptionOld

    const saveContentLabel = !equalLabels ? <div>
      <Divider className={classes.infoDivider}/>
      <Typography variant='headline' component='h4' className={classes.infoTitle}>
        LABEL
      </Typography>
      <Typography variant='subheading' className={classes.infoText}>
        <b>BEFORE:</b>
      </Typography>
      <Typography variant='p' className={ClassNames(classes.infoText, classes.infoTextMarginBottom)}>
        {labelOld}
      </Typography>
      <Typography variant='subheading' className={classes.infoText}>
        <b>AFTER:</b>
      </Typography>
      <Typography variant='p' className={classes.infoText}>
        {label}
      </Typography>
    </div> : ''

    const saveContentDescription = !equalDescriptions ? <div>
      <Divider className={classes.infoDivider}/>
      <Typography variant='headline' component='h4' className={classes.infoTitle}>
        DESCRIPTION
      </Typography>
      <Typography variant='subheading' className={classes.infoText}>
        <b>BEFORE:</b>
      </Typography>
      <Typography variant='p' className={ClassNames(classes.infoText, classes.infoTextMarginBottom)}>
        {descriptionOld}
      </Typography>
      <Typography variant='subheading' className={classes.infoText}>
        <b>AFTER:</b>
      </Typography>
      <Typography variant='p' className={classes.infoText}>
        {description}
      </Typography>
    </div> : ''

    const saveContent = currentSave ? <CustomDialog title={'Save Dialog'} acceptTitle={'Yes'} rejectTitle={'No'} onAccept={this.handleAcceptSave} onCancel={this.handleCancelSave}>
      <DialogContentText>
        We noticed that you made some changes to the following object while visiting this page, do you wich to save them before closing the dialog?
        <div className={classes.info}>
          <Typography variant='headline' component='h4' className={classes.infoTitle}>
            ID
          </Typography>
          <Typography variant='subheading' className={classes.infoText}>
            {id}
          </Typography>
          {saveContentLabel}
          {saveContentDescription}
        </div>
      </DialogContentText>
    </CustomDialog> : ''

    return (
      <div>
        <Dialog
          fullScreen
          open={currentOpen}
          onClose={this.handleClose}
          TransitionComponent={this.transition}
          classes={{
            paper: classes.container
          }}
        >
          {/* APP BAR */}
          <AppBar position='static'>
            <Toolbar>
              {/* DELETE */}
              <Button variant='contained' color='inherit' className={classes.delete} onClick={this.handleRemove}>
                <DeleteIcon className={classes.deleteIcon}/>
                Delete
              </Button>

              {/* SAVE */}
              {saveIcon}

              {/* LOGOUT */}
              <IconButton
                className={classes.close}
                onClick={this.handleClose}
              >
                <CloseIcon />
              </IconButton>
            </Toolbar>
          </AppBar>

          {/* DATA */}
          <div className={classes.dataContainer}>
            <div className={classes.dataRow}>
              {/* INFORMATION */}
              {informationContent}
            </div>
            <div className={classes.dataRow}>
              {/* GALLERY */}
              {galleryContent}
            </div>
          </div>

          {/* REMOVE */}
          {removeContent}

          {/* SAVE */}
          {saveContent}
        </Dialog>
      </div>
    )
  }

  render() {
    const currentLoading = this.state.loading

    if(currentLoading)
      return this.renderWaiting()
    else
      return this.renderValue()
  }
}

// ----- EXPORT

CustomSlider.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomSlider)
