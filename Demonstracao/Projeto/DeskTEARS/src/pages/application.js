// ----- IMPORTS

import React from 'react'
import ReactDOM from 'react-dom'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

import blue from '@material-ui/core/colors/blue'

// Icons
import PowerSettingsNew from '@material-ui/icons/PowerSettingsNew'

import AddIcon from '@material-ui/icons/Add'
import LearnIcon from '@material-ui/icons/FlashOn'
import PredictIcon from '@material-ui/icons/Camera'
import InfoIcon from '@material-ui/icons/Info'

// Components
import AppBar from '@material-ui/core/AppBar'
import Toolbar from '@material-ui/core/Toolbar'
import Typography from '@material-ui/core/Typography'
import IconButton from '@material-ui/core/IconButton'

import Paper from '@material-ui/core/Paper'
import Divider from '@material-ui/core/Divider'
import DialogContentText from '@material-ui/core/DialogContentText'

// ----- CUSTOM

// Custom components
import CustomDial from '../components/customDial'
import CustomGridPage from '../components/customGridPage'
import CustomDialog from '../components/customDialog'
import CustomTextField from '../components/customTextField'

// Custom assets
// ...

// Custom utils
// ...

// ----- GLOBAL VARIABLES
const DEFAULT_ADD_OBJECT = {
  'label': '',
  'description': ''
}

// ----- STYLES

const styles = theme => ({
  container: {

  },
  title: {
    color: 'white'
  },
  logout: {
    position: 'fixed',
    right: 24,

    color: 'white'
  },

  actionAdd: {
    border: '1px solid #4CAF50',

    backgroundColor: 'white',
    color: '#4CAF50',

    '&:hover': {
      backgroundColor: '#4CAF50',
      color: 'white',
    }
  },

  actionLearn: {
    border: '1px solid #FF9800',

    backgroundColor: 'white',
    color: '#FF9800',

    '&:hover': {
      backgroundColor: '#FF9800',
      color: 'white',
    }
  },

  actionPredict: {
    border: '1px solid #673AB7',

    backgroundColor: 'white',
    color: '#673AB7',

    '&:hover': {
      backgroundColor: '#673AB7',
      color: 'white',
    }
  },

  info: {
    marginTop: 24,
    padding: 24,
    backgroundColor: 'white',

    boxShadow: 'none',
    border: '1px solid rgba(0, 0, 0, 0.54)'
  },
  infoTitle: {
    display: 'flex',
    alignItems: 'center',
    color: 'rgba(0, 0, 0, 0.54)',
    marginTop: 12,
    marginBottom: 12,
    fontWeight: 'bold'
  },
  infoIcon: {
    marginRight: 12,
    fontSize: 32
  },
  infoText: {
    color: 'rgba(0, 0, 0, 0.54)',
    textAlign: 'justify'
  },
  infoTextField: {

  },
  infoDivider: {
    backgroundColor: 'rgba(0, 0, 0, 0.54)',
    marginTop: 12,
    marginBottom: 12
  },

  warning: {
    marginTop: 24,
    padding: 24,
    backgroundColor: 'rgba(33, 150, 243, 0.1)',

    boxShadow: 'none',
    border: '1px solid rgba(33, 150, 243, 0.5)'
  },
  warningTitle: {
    display: 'flex',
    alignItems: 'center',
    color: '#0A6EBD',
    marginBottom: 12,
    fontWeight: 'bold'
  },
  warningIcon: {
    marginRight: 12,
    fontSize: 32
  },
  warningText: {
    color: '#0A6EBD',
    textAlign: 'justify'
  },
  warningDivider: {
    backgroundColor: 'rgba(33, 150, 243, 0.5)',
    marginTop: 12,
    marginBottom: 12
  }
})

// ----- DEFINE APPLICATION

class CustomApplication extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.state = {
      notification: nextProps.notification
    }
  }

  // ----- EVENT HANDLERS

  async handleLogout(event) {
    const currentLogout = this.state.logout

    try {
      // Logout call
      await this.state.service.logout()

      // Custom logout
      if(currentLogout)
        currentLogout()
    } catch(error) {
      this.state.notification('Authentication', `Could not log out user: ${error.message}`, 'error')
    }
  }

  async handleAction(action, event) {
    const upperAction = action.toUpperCase()

    // Notification
    // this.state.notification('Information', `User clicked the ${upperAction} button`, 'information')

    switch (upperAction) {
      case 'ADD':
        this.handleAdd()
        break
    }
  }

  handleAdd(event) {
    this.setState({
      add: true,
      addValue: Object.assign({}, DEFAULT_ADD_OBJECT),
      addInvalid: []
    })
  }

  async handleAcceptAdd(event) {
    // Add object
    await this.addObject()
  }

  handleCancelAdd(event) {
    this.setState({
      add: false,
      addValue: '',
      addInvalid: []
    })
  }

  handleAddStringChange(key, value, event) {
    const input = event.target
    const inputValue = value
    const inputStart = input.selectionStart
    const inputEnd = input.selectionEnd

    let currentAddValue = this.state.addValue

    // Define new key value
    currentAddValue[key] = inputValue.toUpperCase()

    // Update input values
    this.setState({
      addValue: currentAddValue
    },
    () => input.setSelectionRange(inputStart, inputEnd)
    )
  }

  // ----- METHODS

  validateObject() {
    const currentAddValue = this.state.addValue
    let invalidValues = ['label', 'description']
    let removeIndex = -1

    if(currentAddValue) {
      // Validate label
      if(currentAddValue.label && currentAddValue.label.length > 0) {
        removeIndex = invalidValues.indexOf('label')

        if(removeIndex > -1)
          invalidValues.splice(removeIndex, 1)
      }

      // Validate description
      if(currentAddValue.description && currentAddValue.description.length > 0) {
        removeIndex = invalidValues.indexOf('description')

        if(removeIndex > -1)
          invalidValues.splice(removeIndex, 1)
      }
    }

    this.setState({
      addInvalid: invalidValues
    })

    return invalidValues.length === 0
  }

  // ----- COMMUNICATIONS

  async logout() {
    try {
      // Validate tears service
      const logged = await this.tearsService.validation()

      // User already logged in
      this.setApplication()
    } catch(error) {
      // User needs to login
      this.setLogin()
    }
  }

  async addObject(close) {
    const currentAddValue = this.state.addValue
    const validation = this.validateObject()

    if(validation) {
      this.state.notification('Models', `User is trying to add a new object`, 'information')

      try {
        await this.state.service.addObject(currentAddValue)
      } catch(error) {
        // Notification
        this.state.notification('Models', `Could not add new object to the platform: ${error.message}`, 'error')
      }

      // Clear values
      this.setState({
        add: false,
        addValue: '',
        addInvalid: []
      })
    }
    else {
        this.state.notification('Models', `Please check for any errors in your inputed values`, 'warning')
    }
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleLogout = this.handleLogout.bind(this)
    this.handleAction = this.handleAction.bind(this)

    this.handleAdd = this.handleAdd.bind(this)
    this.handleAcceptAdd = this.handleAcceptAdd.bind(this)
    this.handleCancelAdd = this.handleCancelAdd.bind(this)

    this.handleAddStringChange = this.handleAddStringChange.bind(this)

    // ----- STATE

    this.state = {
      service: this.props.service,
      notification: this.props.notification,
      logout: this.props.onLogout,

      add: false,
      addValue: '',
      addInvalid: []
    }

    // ----- REFERENCES
    // ...
  }

  render() {
    const { classes } = this.props
    const actions = [
      { icon: <PredictIcon />, name: 'Predict', classe: classes.actionPredict },
      { icon: <LearnIcon />, name: 'Learn', classe: classes.actionLearn },
      { icon: <AddIcon />, name: 'Add', classe: classes.actionAdd }
    ]

    // Add dialog
    const currentAdd = this.state.add
    const currentAddValue = this.state.addValue
    const currentAddInvalid = this.state.addInvalid
    const validAddLabel = currentAddInvalid.indexOf('label') < 0
    const validAddDescription = currentAddInvalid.indexOf('description') < 0

    const currentAddLabel = currentAddValue ? currentAddValue.label : ''
    const currentAddDescription = currentAddValue ? currentAddValue.description : ''

    const addContent = currentAdd ? <CustomDialog title={'Add Dialog'} acceptTitle={'Ok'} rejectTitle={'Cancel'} onAccept={this.handleAcceptAdd} onCancel={this.handleCancelAdd}>
      <DialogContentText>
        Please, fill out the form bellow correctly to add a new model to our object recognition platform database:
        {/* TEXT FIELDS */}
        <Paper className={classes.info}>
          <Typography variant='headline' component='h4' className={classes.infoTitle}>
            LABEL
          </Typography>
          <CustomTextField
            placeholder={'Label'}
            value={currentAddLabel}
            valid={validAddLabel}
            error={"Label value can't be left blank"}
            onChange={this.handleAddStringChange.bind(this, 'label')}
            className={classes.infoTextField}/>
          <Divider className={classes.infoDivider}/>
          <Typography variant='headline' component='h4' className={classes.infoTitle}>
            DESCRIPTION
          </Typography>
          <CustomTextField
            placeholder={'Description'}
            value={currentAddDescription}
            valid={validAddDescription}
            multiline={true}
            error={"Description value can't be left blank"}
            onChange={this.handleAddStringChange.bind(this, 'description')}
            className={classes.infoTextField}/>
        </Paper>
        <Paper className={classes.warning} elevation={1}>
          <Typography variant='headline' component='h4' className={classes.warningTitle}>
            <InfoIcon className={classes.warningIcon}/>ANOTHER THING!
          </Typography>
          <Typography variant='subheading' className={classes.warningText}>
            After this step you will be able to edit other additional information and add new photos to our repository to assist our image recognition system.
          </Typography>
        </Paper>
      </DialogContentText>
    </CustomDialog> : ''

    // Search grid
    const objectsContent = !currentAddValue ? <CustomGridPage title={'OBJECTS'} service={this.state.service} notification={this.state.notification}/> : ''

    return (
      <div className={classes.container}>
        {/* APP BAR */}
        <AppBar position='static'>
          <Toolbar>
            {/* TITLE */}
            <Typography variant='title' className={classes.title}>
              TEARS - Tangible Environments in Augmented Reality Systems
            </Typography>

            {/* LOGOUT */}
            <IconButton
              className={classes.logout}
              onClick={this.handleLogout}
            >
              <PowerSettingsNew />
            </IconButton>
          </Toolbar>
        </AppBar>

        {/* SPEED DIAL */}
        <CustomDial actions={actions} onAction={this.handleAction} />

        {/* GRID */}
        {objectsContent}

        {/* ADD */}
        {addContent}

        {/* CHILDREN */}
        {this.props.children}
      </div>
    )
  }
}

// ----- EXPORT

CustomApplication.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomApplication)
