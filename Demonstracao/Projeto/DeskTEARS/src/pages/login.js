// ----- IMPORTS

import React from 'react'
import ReactDOM from 'react-dom'
import ClassNames from 'classnames'

// Stylesheets
import PropTypes from 'prop-types'
import { withStyles } from '@material-ui/core/styles'

import blue from '@material-ui/core/colors/blue'

// Components
import Grid from '@material-ui/core/Grid'

import Card from '@material-ui/core/Card'
import CardHeader from '@material-ui/core/CardHeader'
import CardContent from '@material-ui/core/CardContent'

import Typography from '@material-ui/core/Typography'

import Button from '@material-ui/core/Button'
import CircularProgress from '@material-ui/core/CircularProgress'

// ----- CUSTOM

// Custom components
import CustomInput from '../components/customInput'

// Custom assets
import BackgroundImage from '../assets/images/TEARS.svg'

// Custom utils
import { sleep } from '../utils'

// ----- GLOBAL VARIABLES
// ...

// ----- STYLES

const styles = theme => ({
  container: {
    position: 'fixed',
    zIndex: 0,

    width: '100vw',
    height: '100vh'
  },

  row: {
    height: '100%'
  },
  column: {

  },

  logo: {
    maxHeight: '100vh'
  },

  card: {
    margin: '0 auto',
    width: 350,

    boxShadow: 'none',
    border: '1px solid #2196F3',
    backgroundColor: 'white'
  },

  cardHeader: {
    marginTop: 29,
    paddingLeft: 24,
    borderLeft: `5px solid #2196F3`
  },
  headline: {
    color: '#2196F3'
  },

  cardBody: {

  },

  cardInput: {
    paddingTop: 16,
    paddingBottom: 16,
    paddingLeft: 5,
    paddingRight: 5
  },
  input: {
    '&:not(:first-child)': {
      marginTop: 24
    }
  },

  cardButton: {
    position: 'relative',
    marginTop: 48,
    textAlign: 'center'
  },
  button: {
    width: 292,
    height: 48,
    color: 'white'
  },
  progress: {
    color: blue[500],
    position: 'absolute',
    top: '50%',
    left: '50%',
    marginTop: -16,
    marginLeft: -16
  },

  unselectable: {
    MozUserSelect: 'none',
    WebkitUserSelect:' none',
    msUserSelect: 'none',
    userSelect: 'none',

    WebkitUserDrag: 'none',
    userDrag: 'none'
  }
})

// ----- DEFINE LOGIN

class CustomLogin extends React.Component {
  // ----- CONSTRUCTOR

  constructor(props) {
    super(props)

    // Initialize
    this.initialize()
  }

  componentDidMount() {
    // ...
  }

  componentWillUnmount() {
    // ...
  }

  componentWillReceiveProps(nextProps) {
    // ----- STATE

    this.state = {
      notification: nextProps.notification
    }
  }

  // ----- EVENT HANDLERS

  // Inputs
  handleUsernameInput(value) {
    // Local update
    this.setState(
      {
        username: {
          value: value,
          valid: true,
          error: ''
        }
      }
    )
  }

  handlePasswordInput(value) {
    // Local update
    this.setState(
      {
        password: {
          value: value,
          valid: true,
          error: ''
        }
      }
    )
  }

  handleKeyInput(event) {
    if(event.which === 13) {
      this.handleAuthentication(event)
    }
  }

  // Buttons
  async handleAuthentication(event) {
    event.preventDefault()

    // Toggle loading
    await this.showLoading()

    // Authentication call
    await this.authenticate()

    // Toggle loading
    this.hideLoading()
  }

  // ----- METHODS

  validateInputValues(validUsername, validPassword) {
    const errorUsername = !validUsername ? 'Invalid username' : ''
    const errorPassword = !validPassword ? 'Invalid password' : ''

    this.setState(prevState => (
      {
        username: {
          ...prevState.username,
          valid: validUsername,
          error: errorUsername
        },
        password: {
          value: '',
          valid: validPassword,
          error: errorPassword
        }
      }
    ))
  }

  async showLoading() {
    this.setState(prevState => (
      {
        username: {
          ...prevState.username,
          disabled: true
        },
        password: {
          ...prevState.password,
          disabled: true
        },
        loading: true
      }
    ))

    // Await for some seconds
    await sleep(2000)
  }

  hideLoading() {
    this.setState(prevState => (
      {
        username: {
          ...prevState.username,
          disabled: false
        },
        password: {
          ...prevState.password,
          disabled: false
        },
        loading: false
      }
    ))
  }

  // ----- COMMUNICATIONS

  async authenticate(username, password) {
    // Verify inputs
    const currentLogin = this.state.login
    const user = this.state.username.value
    const pass = this.state.password.value

    const validUser = user && user.length > 0
    const validPass = pass && pass.length > 0

    if(validUser && validPass) {
      try {
        // Authentication call
        await this.state.service.authenticate(user, pass)

        // Custom login
        if(currentLogin)
          currentLogin()
      } catch(error) {
        this.state.notification('Authentication', `Could not authenticate and retrieve client information: ${error.message}`, 'error')
      }
    }

    // Update input validation
    this.validateInputValues(validUser, validPass)
  }

  // ----- BASIC

  initialize() {
    // ----- EVENTS

    this.handleUsernameInput = this.handleUsernameInput.bind(this)
    this.handlePasswordInput = this.handlePasswordInput.bind(this)
    this.handleKeyInput = this.handleKeyInput.bind(this)

    this.handleAuthentication = this.handleAuthentication.bind(this)

    // ----- STATE

    this.state = {
      username: {
        value: '',
        valid: true,
        error: '',
        disabled: false
      },
      password: {
        value: '',
        valid: true,
        error: '',
        disabled: false
      },
      loading: false,

      service: this.props.service,
      notification: this.props.notification,

      login: this.props.onLogin,
    }

    // ----- REFERENCES
    this.submit = React.createRef()
  }

  render() {
    const { classes } = this.props
    const loading = this.state.loading
    const text = !loading ? 'Ok' : ''

    return (
      <div>
        {/* CONTAINER */}
        <div className={classes.container}>

          {/* ROW */}
          <Grid container direction='row' justify='center' alignItems='center' className={classes.row}>

            {/* COLUMN 1 */}
            <Grid item xs={6} className={classes.column}>
              {/* LOGO */}
              <img src={BackgroundImage} className={ClassNames(classes.logo, classes.unselectable)} />
            </Grid>

            {/* COLUMN 2 */}
            <Grid item xs={6} className={classes.column}>
              {/* CARD */}
              <Card className={ClassNames(classes.card, classes.unselectable)}>
                <div className={classes.cardHeader}>
                  <Typography className={classes.headline} variant="headline">
                    Login
                  </Typography>
                  <Typography variant="subheading" color="textSecondary">
                    We're glad to see you again!
                  </Typography>
                </div>

                <CardContent className={classes.cardBody}>
                  <form className={classes.cardInput} onSubmit={this.handleAuthentication}>
                    <CustomInput
                      className={classes.input}
                      label='Username'
                      type='text'
                      required={true}
                      disabled={this.state.username.disabled}
                      value={this.state.username.value}
                      valid={this.state.username.valid}
                      error={this.state.username.error}
                      onChange={this.handleUsernameInput}
                      onKeyInput={this.handleKeyInput}/>
                    <CustomInput
                      className={classes.input}
                      label='Password'
                      type='password'
                      required={true}
                      disabled={this.state.password.disabled}
                      value={this.state.password.value}
                      valid={this.state.password.valid}
                      error={this.state.password.error}
                      onChange={this.handlePasswordInput}
                      onKeyInput={this.handleKeyInput}/>
                    <div className={classes.cardButton}>
                      <Button ref={this.submit} type="submit" size="large" variant="contained" color="primary" className={classes.button} disabled={loading}>
                        {text}
                      </Button>
                      {loading && <CircularProgress size={32} thickness={5} className={classes.progress} />}
                    </div>
                  </form>
                </CardContent>
              </Card>
            </Grid>

          </Grid>
        </div>

        {/* CHILDREN */}
        {this.props.children}
      </div>
    )
  }
}

// ----- EXPORT

CustomLogin.propTypes = {
  classes: PropTypes.object.isRequired
}

export default withStyles(styles)(CustomLogin)
